//*************************************************************************
//
//Description:	Include-file for PIC18F4550 LoCon
//				Contains all declarations for LoCon_Commands.h
//
//Filename	: LOCON_Commands.h
//Version	: 0.1
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: Ing. G. Bronkhorst
//
// 04-12-2018 Modification: Functions added for DBC-1200 LITE-Amplifier controller
//*************************************************************************

void ClearLoConTransmitBuffers(void);
void ClearLoConReceiveBuffers(void);

void HandleCmdReadSetting_EQ_Hinge_DS_1(void);
void HandleCmdWriteSetting_EQ_Hinge_DS_1(int New_EQ_Hinge_Downstream_1, int OnlyValueCheck);

void HandleCmdReadTypeIDAmplifier(void);
void HandleCmdWriteTypeIDAmplifier(int New_Amplifier_ID_BLL, int OnlyValueCheck);
void HandleCmdReadFactoryDefaultSetting(void);
void HandleCmdWriteFactoryDefaultSetting(int Factory_New_Upstream_Att, int Factory_New_Pre_Att_Mod_1, int Factory_New_Interstage_Att_Mod_1, int Factory_New_Upstream_Equ, int Factory_New_Pre_Equ_Mod_1, int Factory_New_Interstage_Equ_Mod_1, int Factory_New_Downstream_Power_Sw_Mod_1, int Factory_New_Amplifier_ID_BLL, int Factory_New_Setting_0_5dB_Steps,int Factory_New_Setting_0_5dB_Steps_Time_Delay_Ms, int Factory_New_EQ_Hinge_Downstream_1, int ReceivedBytes);
void HandleCmdReadSetting_0_5_Steps_Enabled(void);
void HandleCmdWriteSetting_0_5_Steps_Enabled(int New_Setting_0_5dB_Steps, int OnlyValueCheck);

void HandleCmdReadSetting_0_5_Steps_Time_Delay_Ms(void);
void HandleCmdWriteSetting_0_5_Steps_Time_Delay_Ms(int New_Setting_0_5dB_Steps_Time_Delay, int OnlyValueCheck);

void HandleCmdWriteAddress(int LoConAddr);
void HandleCmdReadAddress(void);
void HandleCmdRequestID(void);
void HandleCmdReadErrors(void);
void HandleCmdReadStatus(void);
void HandleCmdResetModifiedBit(void);
void HandleCmdSetModifiedBit(void);
void HandleCmdReadModifiedBit(void);
void HandleCmdReadFirmwareVersion(void);

void HandleCmdReadUpstreamAttn(void);
void HandleCmdReadPreAttnMod1(void);
void HandleCmdReadInterstageAttnMod1(void);
void HandleCmdReadUpstreamEqu(void);
void HandleCmdReadPreEquMod1(void);
void HandleCmdReadInterstageEquMod1(void);
void HandleCmdReadATCTemperature(void);
void HandleCmdReadATCEnabledDisabled(void);
void HandleCmdReadDownstreamPowerSwMod1(void);

void HandleCmdReadProductMainboardIdentifier(void);
void HandleCmdReadAux1Value(void);

void HandleCmdReadAllAttnAtOnce(void);
void HandleCmdReadAllEquAtOnce(void);
void HandleCmdReadAllSwAtOnce(void);
void HandleCmdReadAllSettingsAtOnce(void);

void HandleCmdWriteUpstreamAttn(int New_Upstream_Att, int OnlyValueCheck);
void HandleCmdWritePreAttnMod1(int New_Pre_Att_Mod_1, int OnlyValueCheck);
void HandleCmdWriteInterstageAttnMod1(int New_Interstage_Att_Mod_1, int OnlyValueCheck);

void HandleCmdWriteUpstreamEqu(int New_Upstream_Equ, int OnlyValueCheck);
void HandleCmdWritePreEquMod1(int New_Pre_Equ_Mod_1, int OnlyValueCheck);
void HandleCmdWriteInterstageEquMod1(int New_Interstage_Equ_Mod_1, int OnlyValueCheck);

void HandleCmdWriteATCEnabledDisabled(int New_ATC_Enabled_Disabled, int OnlyValueCheck);
void HandleCmdWriteDownstreamPowerSwMod1(int New_Downstream_Power_Sw_Mod_1, int OnlyValueCheck);
void HandleCmdWriteAux1Value(int New_Aux_1_Value, int OnlyValueCheck);

void HandleCmdWriteAllAttnAtOnce(int New_Upstream_Att, int New_Pre_Att_Mod_1, int New_Interstage_Att_Mod_1, int ReceivedBytes);
void HandleCmdWriteAllEquAtOnce(int New_Upstream_Equ, int New_Pre_Equ_Mod_1, int New_Interstage_Equ_Mod_1, int ReceivedBytes);
void HandleCmdWriteAllSwAtOnce(int New_Downstream_Power_Sw_Mod_1, int ReceivedBytes);
void HandleCmdWriteAllSettingsAtOnce(int New_Upstream_Att, int New_Pre_Att_Mod_1, int New_Interstage_Att_Mod_1, int New_Upstream_Equ, int New_Pre_Equ_Mod_1, int New_Interstage_Equ_Mod_1, int New_Downstream_Power_Sw_Mod_1, int ReceivedBytes);

void HandleCmdSetToReset(void);
void HandleCmdSetToFactoryDefault(void);
void HandleCmdUnknown(void);

void ExecuteLoConCommand(int LoConCommand);
void ArraytoLoConFunction(void);
void ReceiveLoConFunction(void);
