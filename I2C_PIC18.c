//*************************************************************************
//
//Description:	Source-file for PIC18F4550 Microchip
//		Contains all I2C functionality
//
//Filename	: I2C_PIC18.c
//Version	: 1.0
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//		  
//
//*************************************************************************

//#include "/USB/Include/Compiler.h"
//#include "Microprocessor_Selector.h"
//#include "Definitions.h"
#include "library.h"

#define I2C_DELAY       50                                                      //

//******************************************************************************
//********************* Universal Serial Functions ***************************
#if defined(USE_I2C_FUNCTIONS)
void I2CDelay(unsigned int j)                                                   //Function to implement a time delay to receive LoCon
{
  while(j>0)
  {
  	j--;
  }
  
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void SetSCL(void)                                                               //Function to set Serial Clock to 1
{
	MID_SCL_High();                                                             //Set SCL = 1;                              _|SCL
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void ClrSCL(void)                                                               //Function to set Serial Clock to 0
{
	MID_SCL_Low();                                                              //Set SCL = 0;                              -|SCL
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void SetSDA(void)                                                               //Function to set Serial Data to 1
{
	MID_SDA_High();                                                             //Set SDA = 1;                              _|SDA
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void ClrSDA(void)                                                               //Functionto set Serial Data to 0
{
	MID_SDA_Low();                                                              //Set SDA = 0;                              -|SDA
}
#endif

#if defined(USE_I2C_FUNCTIONS)
char PollSDA(void)                                                              //Function to check if Serial Data is available
{   
	char ReturnValue=0x00;                                                      //Local variable, set value to 0x00
                                                
	if (MID_SDA_INPUT_State == 1)                                               //If SDA==1
    {
        ReturnValue=0x01;                                                       //Set Returnvalue to 0x01 if data is available on SDA
    }
    	
	return(ReturnValue);                                                        //Return result to caller of function
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void SDAisInput(void)                                                           //Function to switch port SDA to input port
{
	mInitI2CSDAInput();                                                         //SDA to input
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void SDAisOutput(void)                                                          //Function to switch port SDA to output port
{
	mInitI2CSDAOutput();                                                        //SDA to output
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void I2CStart(void)                                                             //Function for start bit on I2C port (Start Bit)
{       
    SetSCL();
    SetSDA();                                                               //Set SDA = 1                               _|SDA
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	ClrSDA();                                                                   //Set SDA = 0 to start transmission         -|SDA
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	ClrSCL();                                                                   //Set SCL = 0                               -|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
}
#endif
 
#if defined(USE_I2C_FUNCTIONS)
void I2CStop(void)                                                              //Function for stop bit on I2C port (Stop Bit)
{                                                            
	ClrSCL();                                                                   //Set SCL = 0                               _|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	ClrSDA();                                                                   //Set SDA = 0                               -|SDA
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	SetSCL();                                                                   //Set SCL = 1                               _|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	SetSDA();                                                                   //Set SDA = 1 :STOP                         _|SDA
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	//SDAisInput();                                                             //Change direction of SDA to Input port      <>
}
#endif

#if defined(USE_I2C_FUNCTIONS)
char ReadI2CAck(void)                                                           //Function to read ACK from I2C device
{
	char ReturnValue;                                                           //Local value to return to Caller

	ClrSCL();                                                                   //Set SCL = 0                                                           -|SCL
    I2CDelay(I2C_DELAY);                                                        //Short Delay      
	SDAisInput();                                                               //Change direction of SDA to Input port                                 <>
	I2CDelay(I2C_DELAY);                                                        //Short Delay   
      SetSCL();                                                                 //Set SCL = 1                                                           _|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                                                           ...
	ReturnValue = PollSDA();                                                    //Call function PollSDA to check if serial data is available
	ClrSCL();                                                                   //Set SCL = 0                                                           -|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                                                           ...
	SDAisOutput();                                                              //Change direction of SDA to Output port. Take control of SDA again     <>
	return(ReturnValue);                                                        //ACK='0', NACK='1'
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void SendI2CAck(void)                                                           //Function to send Ack on I2C
{
	ClrSCL();                                                                   //Set SCL = 0                               -|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	ClrSDA();                                                                   //Set SDA = 0                               -|SDA
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	SetSCL();                                                                   //Set SCL = 1                               _|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
	ClrSCL();                                                                   //Set SCL = 0                               -|SCL
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void SendI2CNoAck(void)                                                         //Function to send Negative ACK (NACK) on I2C
{
	ClrSCL();                                                                   //Set SCL = 0                               -|SCL
    I2CDelay(I2C_DELAY);                                                        //Short Delay                               ...
	
    SetSDA();                                                                   //Set SDA = 1                               _|SDA
    I2CDelay(I2C_DELAY);                                                        //Short Delay                               ...
	
    SetSCL();                                                                   //Set SCL = 1                               _|SCL
    I2CDelay(I2C_DELAY);                                                        //Short Delay                               ...
	ClrSCL();                                                                   //Set SCL = 0                               -|SCL
}
#endif

#if defined(USE_I2C_FUNCTIONS)
void WriteI2CByte(char Byte)                                                    //Function to write complete byte to I2C bus
{
	int i;                                                                      //Local variable i as counter
 
	for(i=0; i<8; i++)                                                          //8 Bits per byte, MSB transmitted first
	{
        if( ((Byte<<i) & 0x80) == 0x80)                                         //Send MSB first till LSB is reached
		{                                                                       //Transmit bit '1'
			SetSDA();                                                           //Set SDA = 1                               _|SDA
            I2CDelay(I2C_DELAY);                                                //Short Delay                               ...
		
            SetSCL();                                                           //Set SCL = 1                               _|SCL
            I2CDelay(I2C_DELAY);                                                //Short Delay                               ...
			
            ClrSCL();                                                           //Set SCL = 0                               -|SCL
		}
		else
		{                                                                       //Transmit bit '0'
			ClrSDA();                                                           //Set SDA = 0                               -|SDA
            
            I2CDelay(I2C_DELAY);                                                //Short Delay                               ...
			SetSCL();                                                           //Set SCL = 1                               _|SCL
            
            I2CDelay(I2C_DELAY);                                                //Short Delay                               ...
			ClrSCL();                                                           //Set SCL = 0                               -|SCL
		}
	}
}
#endif

#if defined(USE_I2C_FUNCTIONS)
unsigned char ReadI2CByte(void)                                                          //Function to read complete byte on I2C bus
{
	unsigned char i, ReturnValue;                                                        //Local variable counter and returnvalue
 
	SDAisInput();                                                               //Enable slave to control SDA               <>
	for(i=0; i<8; i++)                                                          //Read all 8 bits to create byte
	{                                             	
		SetSCL();                                                               //Set SCL = 1                               _|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay                               ...
		ReturnValue = ReturnValue << 1;                                         //Shift left one bit
		if(PollSDA() == 0x01)                                                   //Check if Serial data is available on I2C bus
		{
			ReturnValue = ReturnValue | 0x01;                                   //Set LSB
		}
		else
		{
			ReturnValue = ReturnValue & 0xFE;                                   //Clear LSB
		}                                              
		ClrSCL();                                                               //Set Serial data to 0. SCL = 0             -|SCL
        I2CDelay(I2C_DELAY);                                                    //Short Delay
	}                  
	SDAisOutput();                                                              //Get control of SDA again                  <>
	
    return(ReturnValue);                                                        //Return received byte
}                                               	
#endif


#if defined(USE_I2C_FUNCTIONS)
int ReadI2CMID(int Address, int Register)                                       //Local variable to Read value from I2C Product identification
{
 	int ReturnValue=0;                                                          //Local variable to return to caller of Function
    int temp_address;
 	int temp = 0xFF;                                                            //Local variable to write to DAC

    I2CStart();                                                                 //Send Start

    temp_address = Address << 1;
    temp_address = (temp_address | MID_I2C_WRITE_COMMAND) ;                     //And the I2C Address with the Read Command
 	WriteI2CByte(temp_address);                                                 //Send write-command to set address

    if(ReadI2CAck()==0)                                                         //If ACK received
 	{
   		WriteI2CByte(Register);                                                 //Send Address
   		ReadI2CAck();                                                           //Read last ACK
  	}

    I2CStop();
    I2CDelay(I2C_DELAY);
    I2CStart();                                                                 //Send Start

    temp_address = Address << 1;
    temp_address = (temp_address | MID_I2C_READ_COMMAND) ;                      //And the I2C Address with the Read Command
 	WriteI2CByte(temp_address);                                                 //Send write-command to set address

    if(ReadI2CAck()==0)                                                         //If ACK received
  	{
   		temp = (int)ReadI2CByte();                                              //Read data sent by MID
        SendI2CNoAck();
  	}
 	I2CStop();                                                                  //Send Stop
 	ReturnValue = temp;                                                         //Return read 8bits from Device name (remove control bits)
 	return(ReturnValue);                                                        //Return result to caller of Function
}
#endif



#if defined(USE_I2C_FUNCTIONS)
void WriteI2CMID(int Address, int Register, int Senddata)                       //Function to set I2C Registers
{
    int temp_address;
 	I2CStart();                                                                 //Send Start

    temp_address = Address << 1;
    temp_address = (temp_address | MID_I2C_WRITE_COMMAND) ;                     //And the I2C Address with the Read Command
 	WriteI2CByte(temp_address);                                                 //Send write-command to set address

    if(ReadI2CAck()==0)                                                         //If ACK received
 	{
   		WriteI2CByte(Register);                                                 //Send Address
   		if(ReadI2CAck()==0)                                                     //If ACK received
        {
            WriteI2CByte(Senddata);                                             //Send Address
        }
   		ReadI2CAck();                                                           //Read last ACK
  	}
 	I2CStop();                                                                  //Send Stop
}
#endif


#if defined(USE_I2C_FUNCTIONS)
int ReadI2CReg(unsigned char *Data, int readLen)                                         //Total readLen including address  + reg pointer
{
 	int ReturnValue = 0;                                                        //Local variable to return to caller of Function
    int rdIdx = 2;
    int wrIdx = 0;
    int res   = 1;

    Data[0] &= 0xFE;   

    I2CStart();                                                                 //Send Start   
      
    do
    {
        WriteI2CByte(Data[wrIdx]);
        res = ReadI2CAck();
       
    }while(++wrIdx < 2 );                                                       //Write register address + configuration pointer = 2 bytes
    
    if (!res)
    {        
        I2CStart(); 
        WriteI2CByte(Data[0] | I2C_READ_COMMAND);                               //Write Address again
        
        if (!ReadI2CAck())
        {
            do
            {
                Data[rdIdx] = ReadI2CByte();
                SendI2CAck();
            }while((++rdIdx) < readLen - 1);                                    //Read rest of data
            
            Data[rdIdx] = ReadI2CByte();                                        //Last byte is sent with nAck
            SendI2CNoAck();                                                     //Send nAck
        }        
    }    
         
    I2CStop();                                                                  //Send Stop
 	if (!res)
    {
        ReturnValue = 1;
    }
    	
 	return(ReturnValue);                                                        //Return result to caller of Function
}
#endif


#if defined(USE_I2C_FUNCTIONS)
int WriteI2CReg(unsigned char *Data, int writeLen)                              //Local variable to Read 2 byte register from I2C Device
{
 	int ReturnValue = 0;                                                        //Local variable to return to caller of Function
    int wrIdx = 0;
    int res   = 0;

    Data[0] == Data[0] << 1;                                                    
    Data[0] &= 0xFE;
    
    I2CStart();                                                                 //Send Start
       
    do
    {
        WriteI2CByte(Data[wrIdx]);          
        res = ReadI2CAck();
        
    }while((++wrIdx < writeLen)&& !res);                                        //Write bytes as long Ack received
    
    I2CStop();                                                                  //Send Stop
 	
    if (!res)
    {
        ReturnValue = 1;
    }
    
 	
 	return ReturnValue;                                                         //Return result to caller of Function
}
#endif

