/********************************************************************
 FileName:      MCP9808.c
 Designer:		Ing. Hani Shawa
 Processor:		PIC18F45K50
 Hardware:		HardwareProfile.h file.
 Complier:      Microchip C18 (for PIC18)
 Company:		Technetix B.V.
 ********************************************************************
 File Description:
 Change History:
  Rev   Date         Description
  1.00   30/07/2018  Getter functions for MCP9808 temperature sensor
 ********************************************************************/
//#include "variables.h"
//#include "main.h"
//#include "Microprocessor_Selector.h"
//#include "Definitions.h"
//#include "/USB/Include/GenericTypeDefs.h"
//#include "/USB/Include/Compiler.h"
//#include "MCP9808.h"
//#include "I2C_PIC18.h"
//#include "ATC.h"
#include "library.h"

 /****************************** Defines **************************************/
#define MCP9808_CONF_REG_PTR                                                    0x01
#define MCP9808_TEMP_REG_PTR                                                    0x05
#define MCP9808_MANUFACTURER_ID_REG_PTR                                         0x06
#define MCP9808_DEVICE_ID_REG_PTR                                               0x07
#define MCP9808_RESOLUTION_REG_PTR                                              0x08

#define MCP9808_I2C_ADDR                                                        0x30                  
 

int MCP9808_checkSensor(void)                                                   
{
    BOOL result = 0;
    unsigned char Data[4];
    
    Data[0] = MCP9808_I2C_ADDR;
    Data[1] = MCP9808_TEMP_REG_PTR;
    
    if(ReadI2CReg(Data, 4) == 1)
    {
        result = 1;
    }
      return result;
}


float MCP9808_getTemp( void )   
{
    float temperature = 0;
    int iTemperature = 0;
    unsigned char Data[4];
    
    Data[0] = MCP9808_I2C_ADDR;
    Data[1] = MCP9808_TEMP_REG_PTR;
    
    if(ReadI2CReg(Data, 4) == 1)
    {
        //Clear flag bits
        Data[2] = Data[2] & 0x1F;
        if(( Data[2] & 0x10 ) == 0x10 )
        {
            Data[2] = Data[2] & 0x0F;                   // Clear Sign bit
            iTemperature = Data[2]*256+Data[3];         // Concatenate bytes that contain temp info (max 0xFFF)
            iTemperature = 0xFFF - (1+iTemperature);    // Negate 2 complements value
            temperature = (float)(iTemperature)/16.0;   // Divide to get decimal values back
            temperature *= -1;                          // And make the float negative
        }
        else
        {
            //Calculate the temperature in degrees Celsius
            //Signed fixed-point with sign at highest position
            temperature = ( Data[2] * 16.0 ) + ( Data[3] / 16.0 );
        }
    }
    
    return temperature;
}

void MCP9808_getManufacturerId( char* id )                                                     
{
    unsigned char Data[4];
    
    Data[0] = MCP9808_I2C_ADDR;
    Data[1] = MCP9808_MANUFACTURER_ID_REG_PTR;

    if(ReadI2CReg(Data, 4) == 1)
    { 
        id[0] = Data[2];
        id[1] = Data[3];
    }
}

void MCP9808_getDeviceId( char* id )                                                     
{
    unsigned char Data[4];
    
    Data[0] = MCP9808_I2C_ADDR;
    Data[1] = MCP9808_DEVICE_ID_REG_PTR;

    if(ReadI2CReg(Data, 4) == 1)
    { 
        id[0] = Data[2];
        id[1] = Data[3];
    }
}

#ifdef USE_READOUT_INT16_TEMPERATURE
float MCP9808_getTempRaw( char* iTemp )   
{
    float temperature = 0;
    int iTemperature;
    
    //int12 i12 = 0; // for temp storage
    unsigned char Data[4];
    BOOL negative = 0;
    
    Data[0] = MCP9808_I2C_ADDR;
    Data[1] = MCP9808_TEMP_REG_PTR;
    
    if(ReadI2CReg(Data, 4) == 1)
    {
        //Clear flag bits
        Data[2] = Data[2] & 0x1F;
        if(( Data[2] & 0x10 ) == 0x10 )
        {
            // Clear Sign
            negative = 1;
            Data[2] = Data[2] & 0x0F;                   // Clear Sign bit
            iTemperature = Data[2]*256+Data[3];         // Concatenate bytes that contain temp info (max 0xFFF)
            iTemperature = 0xFFF - (1+iTemperature);    // Negate 2 complements value
            temperature = (float)(iTemperature)/16.0;   // Divide to get decimal values back
        }
        else
        {
            iTemperature = Data[2]*256+Data[3];
            temperature = (float)(iTemperature)/16.0;
        }
    }
    //Calculate the temperature in degrees Celsius
    //Signed fixed-point with sign at highest position
    iTemperature = (float)(temperature * 100.0);
    iTemp[0] = (iTemperature >> 8) & 0x00FF;
    iTemp[1] = (iTemperature & 0x00FF);
    
    if(negative)
        iTemp[0]|=0x10; // add sign bit to indicate neg value
    
    return temperature;
}

#ifdef chris_debug_on
float MCP9808_getTempRaw_test(char *Data, char* iTemp )   
{
    float temperature = 0;
    int iTemperature = 0;
    //unsigned char Data[4];
    
    Data[0] = MCP9808_I2C_ADDR;
    Data[1] = MCP9808_TEMP_REG_PTR;
    
    //if(ReadI2CReg(Data, 4) == 1)
    {
        //Clear flag bits
        Data[2] = Data[2] & 0x1F;
        if(( Data[2] & 0x10 ) == 0x10 )
        {
            // Clear Sign
            Data[2] = Data[2] & 0x0F;
            temperature = ((( Data[2] * 16.0 ) + ( Data[3] / 16.0 )) * -1.0 );
        }
        else
        {
            temperature = ( Data[2] * 16.0 ) + ( Data[3] / 16.0 );
        }
    }
    //Calculate the temperature in degrees Celsius
    //Signed fixed-point with sign at highest position
    iTemperature = (float)(temperature * 100.0);
    iTemp[0] = (iTemperature >> 8) & 0x00FF;
    iTemp[1] = (iTemperature & 0x00FF);
    
    return temperature;
}
#endif

//typedef union
//{
//    float Temperature;
//    int TempIntegers;
//    int TempHundredths;
//}UNHW; 
//
//UNHW HW;

void TEMP_CalculateTemperature(void)
{
    HARDWARE.Temperature = MCP9808_getTemp();
    HARDWARE.TempIntegers = (int) HARDWARE.Temperature; //Calculate integers
    HARDWARE.TempHundredths = ABSOLUTE(100 *(HARDWARE.Temperature - (float)HARDWARE.TempIntegers)); //Decimals after the decimal point multiplied by 100
}

