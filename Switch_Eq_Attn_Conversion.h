//*************************************************************************
//
//Description:	Header-file for Microchip-firmware for MaCom & Peregrine attenuators
//		Contains all functionality to change a received value to a readable value
//
//Filename	: Switch_Eq_Attn_Conversion.h
//Version	: 0.1
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 04-12-2012 Modification: Added verification functions for Attenuators and Equalizers
//*************************************************************************


int Calculate_ADC_mA_Value(int ADC_Measured_Value);
int CheckAmplifierCurrent(void);

int DecimalToSameHex(int ValueDecimal);
int HEXToSameDecimal(int ValueHEX);

int VerifyAttnValue31_5dB(int AttnValue);
int VerifyAttnValue22dB(int AttnValue);
int VerifyAttnValue16dB(int AttnValue);

int VerifyEquValue31_5dB(int EquValue);
int VerifyEquValue25dB(int EquValue);
int VerifyEquValue16dB(int EquValue);
int VerifyEquValue10dB(int EquValue);

void DelayLoop(void);
void StepToValueIn0_5dBSteps(int NewSetting, int PreviousSetting, int LatchPinMicro);
