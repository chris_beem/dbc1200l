//*************************************************************************
//
//Description:	Main Header implementation of the PIC4550 Microprocessor
//
//Filename	: Main.h
//Version	: 0.1
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// Modifications 11-12-2012: New list generated
//*************************************************************************

void _reset (void);
void Remapped_High_ISR (void);
void Remapped_Low_ISR (void);
void High_ISR (void);
void Low_ISR (void);
void YourHighPriorityISRCode();
void YourLowPriorityISRCode();

void main(void);
void ATC_task( void );
void USB_task( void );
void USB_Valid_Check( void );
void ReadEEPROMSettings(void);
void Delay_Boot(void);
void Delay_Boot_Short(void);
void ControlAllSwitches(void);
void ControlAllSwitchesPeriodically(void);
void MainboardIdentifier(void);
void InitializeInterrupt(void);
void InterruptInit(void);
void BoardInit(void);

void UserInit(void);
int ReadCurrentMonitorADC(void);
//void BlinkUSBStatus(void);
void USBCBSuspend(void);
void USBCBWakeFromSuspend(void);
void USBCB_SOF_Handler(void);
void USBCBErrorHandler(void);
void USBCBCheckOtherReq(void);
void USBCBStdSetDscHandler(void);
void USBCBInitEP(void);
void USBCBSendResume(void);
