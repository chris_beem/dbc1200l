//*************************************************************************
//
//Description:	Header-file for Microchip-firmware for USB Commands
//		Contains all functionality to change a received value to a readable value
//
//Filename	: USB_Commands.h
//Version	: 0.1
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//		  
//*************************************************************************
void USBDelay(unsigned int j);
void ExecuteLoConDataToUSB(void);
void Lose_Connection(void);
void Delay_Reset1(void);
void Delay_Reset2(void);

void ReplyUSBDelay(unsigned int j);
void Reset_Micro(void);
void ClRWdT_reset(void);
void SendToUSB(void);
void BuildReply_USB(void);

void SendInterfaceInfoToUSB(void);
void Send_Unknown_Framing_Byte(void);
void Send_Unknown_Command_Byte(void);
void Send_Unknown_Handle_Byte(void);
void SendUSBMessageLengthError(void);
void Reset_Device_Reply_USB_Byte(void);
void Bootloader_Device_Reply_USB_Byte(void);
void ProcessIO(void);
void ExecuteUSBCommand(int LoConCommand);





