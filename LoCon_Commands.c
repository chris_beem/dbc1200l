//*************************************************************************
//
//Description:	Source-file for PIC18F4550-firmware LoCon interface
//				Contains all LoCon-General commands routines
//
//Filename	: LoCon_Commands.c
//Version	: 0.2
//Date		: 05-02-2013
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 07-09-2012 Modification: Start with implementing General functions
// 11-09-2012 Modification: First LoCon command list finished. All functions are implemented. Ready for Beta Release.
// 05-02-2013 Modification: Change for DBD and DBC support at Upstream Attenuator, IDS switch 1 and 2 and IDS SwitchOnOff 1 and 2 (read + write change)
// 04-12-2018 Modification: DBC-1200 LITE Amplifier added in Firmware (FIXED to support also this specific amplifier. It works the same as a DBD Amplifier. Hardware Line detection for Motherboard is switched off!
//*************************************************************************

//#include "/USB/Include/GenericTypeDefs.h"
//#include "/USB/Include/Compiler.h"
//#include "/USB/Include/Usb/usb.h"
//#include "/USB/Include/Usb/usb_function_hid.h"
//
//#include "main.h"
//#include "Microprocessor_Selector.h"
//#include "Definitions.h"
//#include "EEPROM_Function.h"
//#include "LoCon_Interface_PIC18.h"
//#include "Switch_Eq_Attn_Conversion.h"
//#include "Serial_Data_V01.h"
//#include "USB_Commands.h"
//#include "LoCon_USB-LoCon.h"
//#include "ATC.h"
//#include "MCP9808.h"
#include "library.h"

//Main declarations, needs to be there always!
#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
int LoConTransmitString[19];                                                    //Global declaration of the LoConTransmitString
int LoConReceiveString[19];                                                     //Global declaration of the LoConReceiveString
#endif

#if defined(USB_INTERNAL_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    extern unsigned char ToSendDataBuffer[64];
    extern USB_HANDLE USBInHandle;
    extern BOOL ReplyUSB;
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
int NrOfReceivedLoConBytes;                                                     //Global declaration of the Number of received bytes by LoCon
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
int LoConFraming;                                                               //Global declaration of the LoCon Framing byte
int LoConAddress;                                                               //Global declaration of the LoCon Address byte
int LoConCommand;                                                               //Global declaration of the LoCon Command byte
int LoConData;                                                                  //Global declaration of the LoCon Data byte
int OwnLoConAddress;                                                            //Global declaration of the OwnLoConAddress
int ModifiedBit;                                                                //Global declaration of the Modified bit
int ReplyDestination;                                                           //Global declaration of the Reply Destination
int ErrorByte = 0x00;                                                           //Global declaration of the ErrorByte
#endif

extern int ATC_Enabled_Disabled;
extern int _OLD_Downstream_Power_Sw_Mod_1;

extern int _OLD_Pre_Att_Mod_1;
extern int _OLD_Interstage_Att_Mod_1;
extern int _OLD_Pre_Equ_Mod_1;
extern int _OLD_Interstage_Equ_Mod_1;
extern int _OLD_Upstream_Att;
extern int _OLD_Upstream_Equ;

extern int MainboardVersion;
extern int Aux_1_Value;
extern int Amplifier_ID_BLL;

extern int Setting_0_5dB_Steps;
extern int Setting_0_5dB_Steps_Time_Delay;
extern int Upstream_Attenuator_6dB;
extern int EQ_Hinge_Downstream_1;

#if defined(USE_ADC_FUNCTIONS)
extern int AmplifierCurrentAlarm;                                               //Global declaration of the AmplifierCurrentAlarm
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
void ClearLoConTransmitBuffers(void)                                            //Function for cleaning LoCon Transmit data arrays
{  
    int i;                                                                      //Local variable as counter
    for(i=0; i<=LOCON_BUFFERLENGTH; i++)                                        //Clear all LoCon data from the Buffers
    {
    	LoConTransmitString[i]=0;                                               //Fill LoCon Receive array with zeros
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
void ClearLoConReceiveBuffers(void)                                             //Function for cleaning LoCon Receive data arrays
{
    int i;                                                                      //Local variable as counter
    for(i=0; i<=LOCON_BUFFERLENGTH; i++)                                        //Clear all LoCon data from the Buffers
    {
    	LoConReceiveString[i]=0;                                                //Fill LoCon Transmit array with zeros
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdUnknown(void)                                                     //Function to handle all unknown commands as Unknown command (0xE5)
{
 	if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
  	{
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
            LoConTransmitString[0] = 0xE5;                                      //Framing Command 0xE5 to indicate reply from slave
            LoConTransmitString[1] = LoConAddress;                              //Reply with same address as called with (Also 0xAF possible as Address)
            LoConTransmitString[2] = LoConCommand;                              //Read address command
            if (ReplyDestination == DESTINATION_LOCON)                          //Send information straight to the LoCon Interface
            {
                SendLoCon(3, LoConTransmitString);                              //Send reply to LoCon-master
            }
            #endif
        }
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte. 
            ToSendDataBuffer[1] = 0x03;                                         //3 bytes back as reply
            ToSendDataBuffer[2] = 0xE5;                                         //Framing Command 0xE5 to indicate reply from slave
            ToSendDataBuffer[3] = LoConAddress;                                 //Reply with same address as called with (Also 0xAF possible as Address)
            ToSendDataBuffer[4] = LoConCommand;                                 //Read address command
            #endif
        }
	}
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadSetting_EQ_Hinge_DS_1(void)                                   //Function to Read the the EQ Hinge for Downstream Module 1
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = EQ_Hinge_Downstream_1;                         //Read the EQ Hinge for Downstream Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = EQ_Hinge_Downstream_1;                        //LoCon Reply the EQ Hinge for Downstream Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteSetting_EQ_Hinge_DS_1(int New_EQ_Hinge_Downstream_1, int OnlyValueCheck)             //Function to Write the the EQ Hinge for Downstream Module 1
{
    if ((EQ_Hinge_Downstream_1 != New_EQ_Hinge_Downstream_1) || (OnlyValueCheck == BOOT_UP))            //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
    {
        if (New_EQ_Hinge_Downstream_1 != 0xFF)
        {
             EQ_Hinge_Downstream_1 = New_EQ_Hinge_Downstream_1;                                         //Update non Local Register
             if (OnlyValueCheck != BOOT_UP)                                                             //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
             {
                 WriteEEPROM(EE_ADR_SETTING_EQ_HINGE_DS1, New_EQ_Hinge_Downstream_1);                   //Write new settings to EEPROM
             }
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))                   //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                                  //Framing
        LoConTransmitString[1] = LoConAddress;                                                          //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                                          //Read address command
        LoConTransmitString[3] = EQ_Hinge_Downstream_1;                                                 //Write the EQ Hinge for Downstream Module 1
        if (ReplyDestination == DESTINATION_LOCON)                                                      //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                                          //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                                        //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                                 //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                                 //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                                         //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                                         //LoCon Command for Request action
            ToSendDataBuffer[5] = EQ_Hinge_Downstream_1;                                                //Write the EQ Hinge for Downstream Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadTypeIDAmplifier(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Amplifier_ID_BLL;                              //Read the Type ID of Amplifier Type ID for BLL
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Amplifier_ID_BLL;                             //LoCon Reply with Type ID of Amplifier Type ID for BLL
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteTypeIDAmplifier(int New_Amplifier_ID_BLL, int OnlyValueCheck)       //Function to Write the Amplifier type ID
{
    if ((Amplifier_ID_BLL != New_Amplifier_ID_BLL) || (OnlyValueCheck == BOOT_UP))     //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
    {
        if (New_Amplifier_ID_BLL != 0xFF)
        {
             Amplifier_ID_BLL = New_Amplifier_ID_BLL;                                  //Update non Local Register
             if (OnlyValueCheck != BOOT_UP)                                            //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
             {
                 WriteEEPROM(EE_ADR_TYPE_ID_AMPLIFIER, New_Amplifier_ID_BLL);          //Write new settings to EEPROM
             }
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))  //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                 //Framing
        LoConTransmitString[1] = LoConAddress;                                         //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                         //Read address command
        LoConTransmitString[3] = Amplifier_ID_BLL;                                     //Write Amplifier Type ID for BLL
        if (ReplyDestination == DESTINATION_LOCON)                                     //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                         //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                       //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                        //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                        //LoCon Command for Request action
            ToSendDataBuffer[5] = Amplifier_ID_BLL;                                    //Write Amplifier Type ID for BLL
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadFactoryDefaultSetting(void)
{
    int Factory_Default_Settings[11];
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        Factory_Default_Settings[0]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_ATTN_SETTING);
        Factory_Default_Settings[1]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_ATTN_MOD_1_SETTING);
        Factory_Default_Settings[2]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_ATTN_MOD_1_SETTING);
        Factory_Default_Settings[3]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_EQU_SETTING);
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        Factory_Default_Settings[4]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_EQU_MOD_1_SETTING);
        Factory_Default_Settings[5]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_EQU_MOD_1_SETTING);
        Factory_Default_Settings[6]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_DOWNSTREAM_POWER_SW_MOD_1_SETTING);
        Factory_Default_Settings[7]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_TYPE_ID_AMPLIFIER);        
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        Factory_Default_Settings[8]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_ENABLED);
        Factory_Default_Settings[9]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_TIME_DELAY_MS);
        Factory_Default_Settings[10] = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_EQ_HINGE_DS1);

        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            HandleCmdUnknown();
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0]  = 0x40;                                        //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1]  = 0x0E;                                        //14 bytes back as reply
            ToSendDataBuffer[2]  = 0xE4;                                        //LoCon Command for Date request
            ToSendDataBuffer[3]  = LoConAddress;                                //LoCon Address for Addressing device
            ToSendDataBuffer[4]  = LoConCommand;                                //LoCon Command for Request action
            ToSendDataBuffer[5]  = Factory_Default_Settings[0];                 //Read Upstream Attenuator
            ToSendDataBuffer[6]  = Factory_Default_Settings[1];                 //Read Pre Attenuator Module 1
            ToSendDataBuffer[7]  = Factory_Default_Settings[2];                 //Read Interstage Attenuator Module 1
            ToSendDataBuffer[8]  = Factory_Default_Settings[3];                 //Read Upstream Equalizer
            ToSendDataBuffer[9]  = Factory_Default_Settings[4];                 //Read Pre Equalizer Module 1
            ToSendDataBuffer[10] = Factory_Default_Settings[5];                 //Read Interstage Equalizer Module 1
            ToSendDataBuffer[11] = Factory_Default_Settings[6];                 //Read Downstream Power Switch Module 1
            ToSendDataBuffer[12] = Factory_Default_Settings[7];                 //Read Type ID for BLL
            ToSendDataBuffer[13] = Factory_Default_Settings[8];                 //Read Setting for 0.5dB steps enabled or disabled (0x00 or 0x01)
            ToSendDataBuffer[14] = Factory_Default_Settings[9];                 //Read Setting for 0.5dB steps Time Delay in Ms
            ToSendDataBuffer[15] = Factory_Default_Settings[10];                //Read EQ Hinge For Downstream Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteFactoryDefaultSetting(int Factory_New_Upstream_Att, int Factory_New_Pre_Att_Mod_1, int Factory_New_Interstage_Att_Mod_1,
                               int Factory_New_Upstream_Equ, int Factory_New_Pre_Equ_Mod_1, int Factory_New_Interstage_Equ_Mod_1,
                               int Factory_New_Downstream_Power_Sw_Mod_1,
                               int Factory_New_Amplifier_ID_BLL, 
                               int Factory_New_Setting_0_5dB_Steps, int Factory_New_Setting_0_5dB_Steps_Time_Delay_Ms, int Factory_New_EQ_Hinge_Downstream_1, int ReceivedBytes)                      //Function to Write all Factory settings in one global command.
{
    int i,j;
    int VerifiedValue;
    int DecimalSetting;
    int Temp_FactorySetting;
    int Factory_Default_Settings[11];

    Factory_Default_Settings[0]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_ATTN_SETTING);
    Factory_Default_Settings[1]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_ATTN_MOD_1_SETTING);
    Factory_Default_Settings[2]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_ATTN_MOD_1_SETTING);
    Factory_Default_Settings[3]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_EQU_SETTING);
    #if defined(USB_INTERNAL_FUNCTIONS)
    {
        USBDeviceTasks();
    }
    #endif
    Factory_Default_Settings[4]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_EQU_MOD_1_SETTING);
    Factory_Default_Settings[5]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_EQU_MOD_1_SETTING);
    Factory_Default_Settings[6]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_DOWNSTREAM_POWER_SW_MOD_1_SETTING);
    Factory_Default_Settings[7]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_TYPE_ID_AMPLIFIER);    
    #if defined(USB_INTERNAL_FUNCTIONS)
    {
        USBDeviceTasks();
    }
    #endif
    Factory_Default_Settings[8]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_ENABLED);
    Factory_Default_Settings[9]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_TIME_DELAY_MS);
    Factory_Default_Settings[10] = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_EQ_HINGE_DS1);

    if (ReceivedBytes == 14)                                                                                //Framing + Address + Command + [Data 11 bytes] = 14 bytes
    {
        if (Factory_New_Upstream_Att != Factory_Default_Settings[0])
        {
            DecimalSetting = HEXToSameDecimal(Factory_New_Upstream_Att);                                    //Result is always between 0 and 255
            VerifiedValue = VerifyAttnValue31_5dB(DecimalSetting);                                          //Now check if received value is valid for Attenuator setting. If not, the return value is maximum (>63). Now changed to decimal, 0 to 63
            if (VerifiedValue != 127)
            {
                Factory_New_Upstream_Att = DecimalToSameHex(VerifiedValue);                                 //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
                Temp_FactorySetting = Factory_New_Upstream_Att;                                             //Update non Local Register
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_ATTN_SETTING,Temp_FactorySetting);              //Write to EEPROM new setting (if it's changed)
            }
        }
        if (Factory_New_Pre_Att_Mod_1 != Factory_Default_Settings[1])
        {
            DecimalSetting = HEXToSameDecimal(Factory_New_Pre_Att_Mod_1);                                   //Result is always between 0 and 255
            VerifiedValue = VerifyAttnValue31_5dB(DecimalSetting);                                          //Now check if received value is valid for Attenuator setting. If not, the returnvalue is maximum (>63). Now changed to decimal, 0 to 63
            if (VerifiedValue != 127)
            {
                Factory_New_Pre_Att_Mod_1 = DecimalToSameHex(VerifiedValue);                                //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
                Temp_FactorySetting = Factory_New_Pre_Att_Mod_1;                                            //Update non Local Register
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_PRE_ATTN_MOD_1_SETTING,Temp_FactorySetting);             //Write to EEPROM new setting (if it's changed)
            }
        }
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
             USBDeviceTasks();
        }
        #endif
        if (Factory_New_Interstage_Att_Mod_1 != Factory_Default_Settings[3])
        {
            DecimalSetting = HEXToSameDecimal(Factory_New_Interstage_Att_Mod_1);                            //Result is always between 0 and 255
            VerifiedValue = VerifyAttnValue31_5dB(DecimalSetting);                                          //Now check if received value is valid for Attenuator setting. If not, the returnvalue is maximum (>63). Now changed to decimal, 0 to 63
            if (VerifiedValue != 127)
            {
                Factory_New_Interstage_Att_Mod_1 = DecimalToSameHex(VerifiedValue);                         //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
                Temp_FactorySetting = Factory_New_Interstage_Att_Mod_1;                                     //Update non Local Register
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_ATTN_MOD_1_SETTING,Temp_FactorySetting);      //Write to EEPROM new setting (if it's changed)
            }
        }
        if (Factory_New_Upstream_Equ != Factory_Default_Settings[5])
        {
            DecimalSetting = HEXToSameDecimal(Factory_New_Upstream_Equ);                                    //Result is always between 0 and 255
            VerifiedValue = VerifyEquValue31_5dB(DecimalSetting);                                           //Now check if received value is valid for Attenuator setting. If not, the returnvalue is maximum (>63). Now changed to decimal, 0 to 63
            if (VerifiedValue != 127)
            {
                Factory_New_Upstream_Equ = DecimalToSameHex(VerifiedValue);                                 //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
                Temp_FactorySetting = Factory_New_Upstream_Equ;                                             //Update non Local Register
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_EQU_SETTING,Temp_FactorySetting);               //Write to EEPROM new setting (if it's changed)
            }
        }
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        if (Factory_New_Pre_Equ_Mod_1 != Factory_Default_Settings[6])
        {
            DecimalSetting = HEXToSameDecimal(Factory_New_Pre_Equ_Mod_1);                                   //Result is always between 0 and 255
            VerifiedValue = VerifyEquValue31_5dB(DecimalSetting);                                           //Now check if received value is valid for Attenuator setting. If not, the returnvalue is maximum (>63). Now changed to decimal, 0 to 63
            if (VerifiedValue != 127)
            {
                Factory_New_Pre_Equ_Mod_1 = DecimalToSameHex(VerifiedValue);                                //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
                Temp_FactorySetting = Factory_New_Pre_Equ_Mod_1;                                            //Update non Local Register
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_PRE_EQU_MOD_1_SETTING,Temp_FactorySetting);              //Write to EEPROM new setting (if it's changed)
            }
        }
        if (Factory_New_Interstage_Equ_Mod_1 != Factory_Default_Settings[8])
        {
            DecimalSetting = HEXToSameDecimal(Factory_New_Interstage_Equ_Mod_1);                            //Result is always between 0 and 255
            VerifiedValue = VerifyEquValue31_5dB(DecimalSetting);                                           //Now check if received value is valid for Attenuator setting. If not, the returnvalue is maximum (>63). Now changed to decimal, 0 to 63
            if (VerifiedValue != 127)
            {
                Factory_New_Interstage_Equ_Mod_1 = DecimalToSameHex(VerifiedValue);                         //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
                Temp_FactorySetting = Factory_New_Interstage_Equ_Mod_1;                                     //Update non Local Register
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_EQU_MOD_1_SETTING,Temp_FactorySetting);       //Write to EEPROM new setting (if it's changed)
            }
        }
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
             USBDeviceTasks();
        }
        #endif
        if (Factory_New_Downstream_Power_Sw_Mod_1 != Factory_Default_Settings[12])
        {
            if ((Factory_New_Downstream_Power_Sw_Mod_1 == 0x00) || (Factory_New_Downstream_Power_Sw_Mod_1 == 0x01))
            {
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_DOWNSTREAM_POWER_SW_MOD_1_SETTING,Factory_New_Downstream_Power_Sw_Mod_1);
            }
        }
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        if (Factory_New_Amplifier_ID_BLL != Factory_Default_Settings[18])
        {
            if (Factory_New_Amplifier_ID_BLL != 0xFF)
            {
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_TYPE_ID_AMPLIFIER,Factory_New_Amplifier_ID_BLL);
            }
        }
        if (Factory_New_Setting_0_5dB_Steps != Factory_Default_Settings[19])
        {
            if ((Factory_New_Setting_0_5dB_Steps == 0x00) || (Factory_New_Setting_0_5dB_Steps == 0x01))
            {
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_ENABLED,Factory_New_Setting_0_5dB_Steps);
            }
        }
        if (Factory_New_Setting_0_5dB_Steps_Time_Delay_Ms != Factory_Default_Settings[20])
        {
            if (Factory_New_Setting_0_5dB_Steps_Time_Delay_Ms != 0xFF)
            {
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_TIME_DELAY_MS,Factory_New_Setting_0_5dB_Steps_Time_Delay_Ms);
            }
        }
        if (Factory_New_EQ_Hinge_Downstream_1 != Factory_Default_Settings[21])
        {
            if (Factory_New_EQ_Hinge_Downstream_1 != 0xFF)
            {
                WriteEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_EQ_HINGE_DS1,Factory_New_EQ_Hinge_Downstream_1);
            }
        }
    }
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                        //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            HandleCmdUnknown();
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            //Read back all settings from EEPROM
            #if defined(USB_INTERNAL_FUNCTIONS)
            Factory_Default_Settings[0]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_ATTN_SETTING);
            Factory_Default_Settings[1]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_ATTN_MOD_1_SETTING);
            Factory_Default_Settings[2]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_ATTN_MOD_1_SETTING);
            Factory_Default_Settings[3]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_EQU_SETTING);
            Factory_Default_Settings[4]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_EQU_MOD_1_SETTING);
            #if defined(USB_INTERNAL_FUNCTIONS)
            {
                USBDeviceTasks();
            }
            #endif
            Factory_Default_Settings[5]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_EQU_MOD_1_SETTING);
            Factory_Default_Settings[6]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_DOWNSTREAM_POWER_SW_MOD_1_SETTING);
            #if defined(USB_INTERNAL_FUNCTIONS)
            {
                USBDeviceTasks();
            }
            #endif
            Factory_Default_Settings[7]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_TYPE_ID_AMPLIFIER);
            Factory_Default_Settings[8]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_ENABLED);
            Factory_Default_Settings[9]  = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_TIME_DELAY_MS);
            Factory_Default_Settings[10] = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_EQ_HINGE_DS1);
            #if defined(USB_INTERNAL_FUNCTIONS)
            {
                USBDeviceTasks();
            }
            #endif
            ToSendDataBuffer[0]  = 0x40;                                        //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1]  = 0x0E;                                        //14 bytes back as reply
            ToSendDataBuffer[2]  = 0xE4;                                        //LoCon Command for Date request
            ToSendDataBuffer[3]  = LoConAddress;                                //LoCon Address for Addressing device
            ToSendDataBuffer[4]  = LoConCommand;                                //LoCon Command for Request action
            ToSendDataBuffer[5]  = Factory_Default_Settings[0];                 //Factory_New_Upstream_Att. Read Upstream Attenuator
            ToSendDataBuffer[6]  = Factory_Default_Settings[1];                 //Factory_New_Pre_Att_Mod_1. Read Pre Attenuator Module 1
            ToSendDataBuffer[7]  = Factory_Default_Settings[2];                 //Factory_New_Interstage_Att_Mod_1. Read Interstage Attenuator Module 1
            ToSendDataBuffer[8]  = Factory_Default_Settings[3];                 //Factory_New_Upstream_Equ. Read Upstream Equalizer
            ToSendDataBuffer[9]  = Factory_Default_Settings[4];                 //Factory_New_Pre_Equ_Mod_1. Read Pre Equalizer Module 1
            ToSendDataBuffer[10] = Factory_Default_Settings[5];                 //Factory_New_Interstage_Equ_Mod_1. Read Interstage Equalizer Module 1
            ToSendDataBuffer[11] = Factory_Default_Settings[6];                 //Factory_New_Downstream_Power_Sw_Mod_1. Downstream Power Switch Module 1 value
            ToSendDataBuffer[12] = Factory_Default_Settings[7];                 //Factory_New_Amplifier_ID_BLL. BLL Software Type Byte
            ToSendDataBuffer[13] = Factory_Default_Settings[8];                 //Factory_New_Setting_0_5dB_Steps. Steps 0_5dB is enabled or disabled (0x01 enabled or 0x00 for disabled)
            ToSendDataBuffer[14] = Factory_Default_Settings[9];                 //Factory_New_Setting_0_5dB_Steps_Time_Delay_Ms. Steps 0_5dB Time delay setting
            ToSendDataBuffer[15] = Factory_Default_Settings[10];                //Factory_New_EQ_Hinge_Downstream_1. EQ Hinge For Downstream Module 1
            #endif
        }
    }
    /*
    #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    for (i=0;i<5;i++)
    {
        mLED_Alarm_On();
        for (j=0 ;j<15;j++)
        {
            LoConDelay(10000);
        }
        mLED_Alarm_Off();
        for (j=0 ;j<15;j++)
        {
            LoConDelay(10000);
        }
    }
    #endif
    #if defined(USB_INTERNAL_FUNCTIONS)
    for (i=0;i<5;i++)
    {
        mLED_Alarm_On();
        for (j=0 ;j<15;j++)
        {
            USBDelay(3000);
        }
        mLED_Alarm_Off();
        for (j=0 ;j<15;j++)
        {
            USBDelay(3000);
        }
    }
    #endif
    #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        ClearLoConReceiveBuffers();                                                     //Clear LoCon Receive buffers
    #endif
    Reset_Micro();                                                                      //Bye bye, Kill all applications and -->RESET!<--
    */
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadSetting_0_5_Steps_Enabled(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                        //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                      //Framing
        LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Setting_0_5dB_Steps;                                       //Read if the 0.5dB Steps are enabled or disabled
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                              //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                     //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Setting_0_5dB_Steps;                                      //LoCon Reply with if the 0.5dB Steps are enabled or disabled (0x00=disabled) (0x01 = enabled)
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteSetting_0_5_Steps_Enabled(int New_Setting_0_5dB_Steps, int OnlyValueCheck)       //Function to Write new setting for 0.5dB Steps Enabled or Disabled
{
    if((New_Setting_0_5dB_Steps == 0x00) || (New_Setting_0_5dB_Steps == 0x01))
    {
        if ((Setting_0_5dB_Steps != New_Setting_0_5dB_Steps) || (OnlyValueCheck == BOOT_UP))        //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
        {
            Setting_0_5dB_Steps = New_Setting_0_5dB_Steps;                                          //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                                          //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                WriteEEPROM(EE_ADR_SETTING_0_5_STEPS_ENABLED,New_Setting_0_5dB_Steps);              //Write new settings to EEPROM
            }
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))               //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                              //Framing
        LoConTransmitString[1] = LoConAddress;                                                      //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                                      //Read address command
        LoConTransmitString[3] = Setting_0_5dB_Steps;                                               //Write Setting_0_5dB_Steps 0x00 = disabled, 0x01 is enabled
        if (ReplyDestination == DESTINATION_LOCON)                                                  //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                                      //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                                    //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                             //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                             //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                             //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                                     //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                                     //LoCon Command for Request action
            ToSendDataBuffer[5] = Setting_0_5dB_Steps;                                              //Write Amplifier Type ID for BLL
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadSetting_0_5_Steps_Time_Delay_Ms(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                        //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                      //Framing
        LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Setting_0_5dB_Steps_Time_Delay;                            //Read if the 0.5dB Steps are enabled or disabled
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                              //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                     //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Setting_0_5dB_Steps_Time_Delay;                           //LoCon Reply with if the 0.5dB Steps are enabled or disabled (0x00=disabled) (0x01 = enabled)
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteSetting_0_5_Steps_Time_Delay_Ms(int New_Setting_0_5dB_Steps_Time_Delay, int OnlyValueCheck)      //Function to Write new setting for 0.5dB Steps Enabled or Disabled
{
    if ((Setting_0_5dB_Steps_Time_Delay != New_Setting_0_5dB_Steps_Time_Delay) || (OnlyValueCheck == BOOT_UP))      //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
    {
        if (New_Setting_0_5dB_Steps_Time_Delay != 0xFF)
        {
            Setting_0_5dB_Steps_Time_Delay = New_Setting_0_5dB_Steps_Time_Delay;                                    //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                                                          //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                WriteEEPROM(EE_ADR_SETTING_0_5_STEPS_TIME_DELAY_MS,New_Setting_0_5dB_Steps_Time_Delay);             //Write new settings to EEPROM
            }
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))                               //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                                  //Framing
        LoConTransmitString[1] = LoConAddress;                                                          //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                                          //Read address command
        LoConTransmitString[3] = Setting_0_5dB_Steps_Time_Delay;                                        //Write Setting_0_5dB_Steps 0x00 = disabled, 0x01 is enabled
        if (ReplyDestination == DESTINATION_LOCON)                                                      //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                                          //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                                        //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                                 //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                                 //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                                         //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                                         //LoCon Command for Request action
            ToSendDataBuffer[5] = Setting_0_5dB_Steps_Time_Delay;                                       //Write Amplifier Type ID for BLL
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteAddress(int LoConAddr)                                       //Function to handle if there is a request to change LoCon Address
{
    #if !defined(USE_FIXED_LOCON_ADDRESS)
    if (OwnLoConAddress != LoConAddr)                                           //Check if LoCon address is changed from actual address
    {
        WriteEEPROM(EE_ADR_OWNLOCONADDRESS, LoConAddr);                         //Store new LoCon-address in EEPROM
    }
    #if defined(USB_INTERNAL_FUNCTIONS)
    {
        USBDeviceTasks();
    }
    #endif
    OwnLoConAddress = ReadEEPROM(EE_ADR_OWNLOCONADDRESS);
   #endif
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
         #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
         LoConTransmitString[0] = 0xE4;                                         //Framing
         LoConTransmitString[1] = LoConAddress;                                 //Reply with same address as called with (might be 0xAF)
         LoConTransmitString[2] = LoConCommand;                                 //Request for read address command
         LoConTransmitString[3] = OwnLoConAddress;                              //Actual Own LoCon addressed address command
         if (ReplyDestination == DESTINATION_LOCON)                             //Send information straight to the LoCon Interface
         {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
         }
         #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //7 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = LoConAddr;                                    //Data byte for setting
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadAddress(void)                                                 //Function to Read own LoCon Address
{
    #if !defined (USE_FIXED_LOCON_ADDRESS)                                      //Check if there is a fixed LoCon Address Requested
	OwnLoConAddress = ReadEEPROM(EE_ADR_OWNLOCONADDRESS);
    #endif
	if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
	{
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = OwnLoConAddress;                               //Own LoConAddresss minus 0xA0 for internal coding
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = OwnLoConAddress;                              //Data byte for setting
            #endif
        }
	}
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdRequestID(void)                                                   //Function to read ID of DBC-1200 LITE-Amplifier
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = OWN_LOCON_DEVICE_ID;                           //LoCon-device ID of DBC-1200 LITE-Amplifier
        LoConTransmitString[4] = PRODUCTIONDATEyearMSB;                         //MSB of Year of production
        LoConTransmitString[5] = PRODUCTIONDATEyearLSB;                         //LSB of Year of production
        LoConTransmitString[6] = PRODUCTIONDATEmonth;                           //Month of production
        LoConTransmitString[7] = PRODUCTIONDATEday;                             //Day of production
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
           SendLoCon(8, LoConTransmitString);                                   //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x08;                                         //8 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = OWN_LOCON_DEVICE_ID;                          //LoCon-device ID of DBC-1200 LITE-Amplifier
            ToSendDataBuffer[6] = PRODUCTIONDATEyearMSB;                        //MSB of Year of production
            ToSendDataBuffer[7] = PRODUCTIONDATEyearLSB;                        //LSB of Year of production
            ToSendDataBuffer[8] = PRODUCTIONDATEmonth;                          //Month of production
            ToSendDataBuffer[9] = PRODUCTIONDATEday;                            //Day of production
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadErrors(void)                                                  //Function to read Alarm of DBC-1200 LITE-Amplifier
{
    #if defined(USE_ADC_FUNCTIONS)
    AmplifierCurrentAlarm = CheckAmplifierCurrent();                            //Read ADC again to check if the AmplifierCurrentAlarm is in alarm
    if (AmplifierCurrentAlarm == 0x01)                                          //Check if Alarm is present. If so the RFAlarm value is 1!
    {
        //TODO ENABLE NEXT LINE IN FINAL RELEASE TO ENABLE ALARM LINES
        mLED_Alarm_On();                                                        //Set Alarm LED to RED
    }
                                                                                //TODO Now when the alarm is solved the LED will be back to green, check with client if this reset is needed in polling Rack function. In Main polling the LED will stay on Red in case of error!
    else if (AmplifierCurrentAlarm == 0x00)                                     //Check if there is no Alarm
    {
        //TODO ENABLE NEXT LINE IN FINAL RELEASE TO ENABLE ALARM LINES
        mLED_Alarm_Off()                                                        //Set LED to green if there is no Alarm
    }
    #endif
    #if !defined(USE_ADC_FUNCTIONS)
    {
        mLED_Alarm_Off();
    }
    #endif
 	if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
  	{
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
	 	LoConTransmitString[0] = 0xE4;                                          //Framing Command 0xE4 to indicate reply from slave
	 	LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (Also 0xAF possible as Address)
	 	LoConTransmitString[2] = LoConCommand;                                  //Read address command
	 	LoConTransmitString[3] = ErrorByte;                                     //Error to indicate if there is an alarm occurred. (0x00 is no alarm, 0x01 is alarm)
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = ErrorByte;                                    //Data byte for setting
            #endif
        }
  	}     
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadStatus(void)                                                  //Function to read Status of device
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] =  0xE4;                                         /Framing Command 0xE4 to indicate reply from slave
        LoConTransmitString[1] =  LoConAddress;                                 /Reply with same Address as called with (Also 0xAF possible as Address)
        LoConTransmitString[2] =  LoConCommand;                                 /Read address command
        LoConTransmitString[3] =  Upstream_Att;                                 /Status Alarm_RF. 0x01 indicates no Error (good)
        LoConTransmitString[4] =  Pre_Att_Mod_1;                                /Read Pre Attenuator Module 1
        LoConTransmitString[5] =  Interstage_Att_Mod_1;                         /Read Interstage Attenuator Module 1
        LoConTransmitString[6] =  Upstream_Equ;                                 /Read Upstream Equalizer
        LoConTransmitString[7] =  Pre_Equ_Mod_1;                                /Read Pre Equalizer Module 1
        LoConTransmitString[8] =  Interstage_Equ_Mod_1;                         //Read Interstage Equalizer Module 1
        LoConTransmitString[9] =  Downstream_Power_Sw_Mod_1;                    //Read Downstream Power Switch Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(10, LoConTransmitString);                                 //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0]  = 0x40;                                        //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1]  = 0x0A;                                        //10 bytes back as reply
            ToSendDataBuffer[2]  = 0xE4;                                        //LoCon Command for Date request
            ToSendDataBuffer[3]  = LoConAddress;                                //LoCon Address for Addressing device
            ToSendDataBuffer[4]  = LoConCommand;                                //LoCon Command for Request action
            ToSendDataBuffer[5]  = Info.SET.att_us;                                //Status Alarm_RF. 0x01 indicates no Error (good)
            ToSendDataBuffer[6]  = Info.SET.att_pre;                               //Read Pre Attenuator Module 1
            ToSendDataBuffer[7]  = Info.SET.att_int;                        //Read Interstage Attenuator Module 1
            ToSendDataBuffer[8]  = Info.SET.eq_us;                                //Read Upstream Equalizer
            ToSendDataBuffer[9]  = Info.SET.eq_pre;                               //Read Pre Equalizer Module 1
            ToSendDataBuffer[10] = Info.SET.eq_int;                        //Read Interstage Equalizer Module 1
            ToSendDataBuffer[11] = Info.SET.pwr_sw;                   //Read Downstream Power Switch Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdResetModifiedBit(void)                                            //Function to Reset Modified bit
{
    ModifiedBit=0;                                                              //Clear modified-bit
    WriteEEPROM(EE_ADR_MODIFIED_BIT, ModifiedBit);                              //Store value of modified bit
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(3, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x03;                                         //3 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdSetModifiedBit(void)                                              //Function to Set Modified bit
{
    ModifiedBit=1;                                                              //Set modified-bit
    WriteEEPROM(EE_ADR_MODIFIED_BIT, ModifiedBit);                              //Store value of modified bit
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(3, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x03;                                         //3 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadModifiedBit(void)                                             //Function to Read Modified bit
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = ModifiedBit;                                   //Value of modified-bit
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = ModifiedBit;                                  //LoCon Command for Request action
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadFirmwareVersion(void)                                         //Read firmware version of DBC-1200 LITE-Amplifier
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = FIRMWAREVERSIONMSB;                            //MSB of firmware-version of DBC-1200 LITE-Amplifier
        LoConTransmitString[4] = FIRMWAREVERSIONLSB;                            //MSB of firmware-version of DBC-1200 LITE-Amplifier
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(5, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x05;                                         //5 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = FIRMWAREVERSIONMSB;                           //LoCon Reply with MSB Firmware version
            ToSendDataBuffer[6] = FIRMWAREVERSIONLSB;                           //LoCon Reply with LSB Firmware version
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadUpstreamAttn(void)                                            //Function to read the position of the Broadcast Input Attenuator
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Upstream_Att;                                  //Read the position of the Upstream Attenuator
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_us;                                 //LoCon Reply with Upstream Attenuation value
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadPreAttnMod1(void)                                             //Function to read the position of the Broadcast Output Attenuator
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Pre_Att_Mod_1;                                 //Read the position of the Pre Attenuator Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_pre;                                //LoCon Reply with Pre Attenuator Module 1 value
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadInterstageAttnMod1(void)                                      //Function to read the position interstage Attenuator of Module 1
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Interstage_Att_Mod_1;                          //Read the position of the Interstage Attenuator Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_int;                         //LoCon Reply with Interstage Attenuator Module 1 value
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadUpstreamEqu(void)                                             //Function to read Upstream Equalizer
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Upstream_Equ;                                  //Read the position of the Upstream Equalizer
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_us;                                 //LoCon Reply with Upstream Equalizer value
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadPreEquMod1(void)                                              //Function to read the position of the Pre Equalizer Module 1
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
	LoConTransmitString[0] = 0xE4;                                              //Framing
	LoConTransmitString[1] = LoConAddress;                                      //Reply with same address as called with (might be 0xAF)
	LoConTransmitString[2] = LoConCommand;                                      //Read address command
	LoConTransmitString[3] = Pre_Equ_Mod_1;                                     //Read the position of the Pre Equalizer Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_pre;                                //LoCon Reply with Pre Equalizer Module 1 value
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadInterstageEquMod1(void)                                       //Function to read the position of the Interstage Equalizer Module 1
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Interstage_Equ_Mod_1;                          //Read the position of the Interstage Equalizer Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_int;                         //LoCon Reply with Interstage Equalizer Module 1 value
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadATCTemperature(void)                                          //Function to read ATC temperature of the DBC-1200 LITE
{
    char sensorId[2] = { 0x99, 0x99 };                                          //Set "empty" value in array for Sensor ID
    char id[2]       = { 0x55, 0x55 };                                          //Set "empty" value in Manufacturer ID
    char itemp[2]    = { 0x88, 0x88 };                                          //Set "empty" value in Temperature Result
    
    #if defined(USE_READOUT_INT16_TEMPERATURE)
        MCP9808_getManufacturerId ( &sensorId[0]);                              //Read from MCP9808 the Manufacturer ID
        MCP9808_getDeviceId ( &id[0] );                                         //Read from MCP9808 Device ID
        MCP9808_getTempRaw ( &itemp[0] );                                       //Read from MCP9808 the Temperature        
    #endif

    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = sensorId[0];                                   //I2C MCP9808 Address for MCP9808 Temperature Sensor
        LoConTransmitString[4] = sensorId[1];                                   //I2C MCP9808 Manufacturer ID Register 
        LoConTransmitString[5] = id[0];                                         //I2C MCP9808 ID and Revision Number (High byte)
        LoConTransmitString[6] = id[1];                                         //I2C MCP9808 ID and Revision Number (Low byte)
        LoConTransmitString[7] = itemp[0];                                      //I2C MCP9808 Raw Temperature High byte
        LoConTransmitString[8] = itemp[1];                                      //I2C MCP9808 Raw Temperature Low byte
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
           SendLoCon(9, LoConTransmitString);                                   //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0]  = 0x40;                                        //Echo back to the host PC the command we are fulfilling in the first byte. 
            ToSendDataBuffer[1]  = 0x09;                                        //9 bytes back as reply
            ToSendDataBuffer[2]  = 0xE4;                                        //LoCon Command for Date request
            ToSendDataBuffer[3]  = LoConAddress;                                //LoCon Address for Addressing device
            ToSendDataBuffer[4]  = LoConCommand;                                //LoCon Command for Request action
            ToSendDataBuffer[5]  = sensorId[0];                                 //I2C MCP9808 Address for MCP9808 Temperature Sensor
            ToSendDataBuffer[6]  = sensorId[1];                                 //I2C MCP9808 Manufacturer ID Register 
            ToSendDataBuffer[7]  = id[0];                                       //I2C MCP9808 ID and Revision Number (High byte)
            ToSendDataBuffer[8]  = id[1];                                       //I2C MCP9808 ID and Revision Number (Low byte)
            ToSendDataBuffer[9]  = itemp[0];                                    //I2C MCP9808 Raw Temperature High byte of INT16
            ToSendDataBuffer[10] = itemp[1];                                    //I2C MCP9808 Raw Temperature Low byte of INT16
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadATCEnabledDisabled(void)                                      //Function to Read ATC Enabled or Disabled
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = ATC_Enabled_Disabled;                          //Value of ATC Enabled or Disabled
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = ATC_Enabled_Disabled;                         //LoCon Reply ATC Enabled or Disabled
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadDownstreamPowerSwMod1(void)                                   //Function to read the Downstream Power Switch Module 1 Setting
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Downstream_Power_Sw_Mod_1;                     //Read the position of the Downstream Power Switch Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                           //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                           //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                           //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                   //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                   //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.pwr_sw;                      //LoCon Reply with the Downstream Power Switch Module 1
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadProductMainboardIdentifier(void)
{
    MainboardIdentifier();                                                      //Detect Mainboard version
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = MainboardVersion;                              //Read the position of the IDS Switch On Off Output 2
            if (ReplyDestination == DESTINATION_LOCON)                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = MainboardVersion;                             //LoCon Reply with Product version. (0x00 = DBC / 0x01 = DBD / 0xF1 = DBC-1200 LITE Amplifier)
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadAux1Value(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Aux_1_Value;                                   //Read the Aux 1 value given by Software
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Aux_1_Value;                                  //LoCon Reply with Aux 1 value given by Software
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadAllAttnAtOnce(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
    	LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
    	LoConTransmitString[2] = LoConCommand;                                  //Read address command
    	LoConTransmitString[3] = Upstream_Att;                                  //Read Upstream Attenuator
        LoConTransmitString[4] = Pre_Att_Mod_1;                                 //Read Pre Attenuator Module 1
        LoConTransmitString[5] = Interstage_Att_Mod_1;                          //Read Interstage Attenuator Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(6, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x06;                                         //6 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_us;                                 //Read Upstream Attenuator
            ToSendDataBuffer[6] = Info.SET.att_pre;                                //Read Pre Attenuator Module 1
            ToSendDataBuffer[7] = Info.SET.att_int;                         //Read Interstage Attenuator Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadAllEquAtOnce(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
    	LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
    	LoConTransmitString[2] = LoConCommand;                                  //Read address command
    	LoConTransmitString[3] = Upstream_Equ;                                  //Read Upstream Equalizer
        LoConTransmitString[4] = Pre_Equ_Mod_1;                                 //Read Pre Equalizer Module 1
        LoConTransmitString[5] = Interstage_Equ_Mod_1;                          //Read Interstage Equalizer Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(6, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x06;                                         //6 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_us;                                 //Read Upstream Equalizer
            ToSendDataBuffer[6] = Info.SET.eq_pre;                                //Read Pre Equalizer Module 1
            ToSendDataBuffer[7] = Info.SET.eq_int;                         //Read Interstage Equalizer Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadAllSwAtOnce(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Downstream_Power_Sw_Mod_1;                     //Read Downstream Power Switch Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.pwr_sw;                    //Read Downstream Power Switch Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdReadAllSettingsAtOnce(void)
{
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                          //Framing
    	LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
    	LoConTransmitString[2] = LoConCommand;                                  //Read address command
    	LoConTransmitString[3] = Upstream_Att;                                  //Read Upstream Attenuator
        LoConTransmitString[4] = Pre_Att_Mod_1;                                 //Read Pre Attenuator Module 1
        LoConTransmitString[5] = Interstage_Att_Mod_1;                          //Read Interstage Attenuator Module 1
    	LoConTransmitString[6] = Upstream_Equ;                                  //Read Upstream Equalizer
        LoConTransmitString[7] = Pre_Equ_Mod_1;                                 //Read Pre Equalizer Module 1
        LoConTransmitString[8] = Interstage_Equ_Mod_1;                          //Read Interstage Equalizer Module 1
        LoConTransmitString[9] = Downstream_Power_Sw_Mod_1;                     //Read Downstream Power Switch Module 1
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(10, LoConTransmitString);                                 //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0]  = 0x40;                                        //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1]  = 0x0A;                                        //10 bytes back as reply
            ToSendDataBuffer[2]  = 0xE4;                                        //LoCon Command for Date request
            ToSendDataBuffer[3]  = LoConAddress;                                //LoCon Address for Addressing device
            ToSendDataBuffer[4]  = LoConCommand;                                //LoCon Command for Request action
            ToSendDataBuffer[5]  = Info.SET.att_us;                                //Read Upstream Attenuator
            ToSendDataBuffer[6]  = Info.SET.att_pre;                               //Read Pre Attenuator Module 1
            ToSendDataBuffer[7]  = Info.SET.att_int;                        //Read Interstage Attenuator Module 1
            ToSendDataBuffer[8]  = Info.SET.eq_us;                                //Read Upstream Equalizer
            ToSendDataBuffer[9]  = Info.SET.eq_pre;                               //Read Pre Equalizer Module 1
            ToSendDataBuffer[10] = Info.SET.eq_int;                        //Read Interstage Equalizer Module 1
            ToSendDataBuffer[11] = Info.SET.pwr_sw;                   //Read Downstream Power Switch Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteUpstreamAttn(int New_Upstream_Att, int OnlyValueCheck)       //Function to Write new Upstream Attenuator setting (HEX value same shape as decimal value)
{
    int DecimalSetting;                                                         //Converted Received HEX value in Decimal
    int Old_DecimalSetting;
    int VerifiedValue;                                                          //Decimal value for Valid check
    int DecimalSettingWithIDS;
    int Old_Upstream_Att;

    if ((Info.SET.att_us != New_Upstream_Att) || (OnlyValueCheck == BOOT_UP))
    {
        DecimalSetting = HEXToSameDecimal(New_Upstream_Att);                    //Result is always between 0 and 255
        //VerifiedValue = VerifyAttnValue22dB(DecimalSetting);                  //Now check if received value is valid for Attenuator setting. If not, the return value is maximum (>44). Now changed to decimal, 0 to 44
        VerifiedValue = VerifyAttnValue31_5dB(DecimalSetting);                  //Now check if received value is valid for Attenuator setting. If not, the return value is maximum (>63). Now changed to decimal, 0 to 63
        if (VerifiedValue != 127)
        {
            New_Upstream_Att = DecimalToSameHex(VerifiedValue);                 //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
            Old_Upstream_Att = Info.SET.att_us;
            Old_DecimalSetting = HEXToSameDecimal(Old_Upstream_Att);            //Result is always between 0 and 255
            Info.SET.att_us = New_Upstream_Att;                                    //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                      //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                 WriteEEPROM(EE_ADR_UPSTREAM_ATTN_SETTING,Info.SET.att_us);        //Write to EEPROM new setting (if it's changed)
            }
        }
        if ((MainboardVersion == DBC) && (Upstream_Attenuator_6dB == 0x01))     //DBC Motherboard & IDS function enabled
        {
            DecimalSettingWithIDS = HEXToSameDecimal((Info.SET.att_us + 0x12));    //Result is always between 0 and 255 + 6dB attenuator
        }
        #if defined(USE_SERIAL_DATA_FUNCTIONS)
        if ((MainboardVersion == DBC) && (Upstream_Attenuator_6dB == 0x01))     //DBC Motherboard
        {
            if ((OnlyValueCheck != BOOT_UP) && (Setting_0_5dB_Steps == 0x01))   //Check if not boot-up and step mode is enabled
            {
                StepToValueIn0_5dBSteps(DecimalSettingWithIDS, Old_DecimalSetting, UPSTREAM_ATTENUATOR);    // Take values to StepFunction
            }
            else
            {
                WriteSerialData(DecimalSettingWithIDS,UPSTREAM_ATTENUATOR);     //Now finally update the Real attenuator (IC). Give Latch to BC Input Attenuator
            }
        }
        else
        {
            if ((OnlyValueCheck != BOOT_UP) && (Setting_0_5dB_Steps == 0x01))   //Check if not boot-up and step mode is enabled
            {
                StepToValueIn0_5dBSteps(DecimalSetting, Old_DecimalSetting, UPSTREAM_ATTENUATOR);    //Take values to StepFunction
            }
            else
            {
                WriteSerialData(DecimalSetting,UPSTREAM_ATTENUATOR);            //Now finally update the Real attenuator (IC). Give Latch to BC Input Attenuator
            }
        }
        #endif
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))       //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Upstream_Att;                                  //Upstream Attenuator value
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_us;                                 //Read Upstream Attenuator
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWritePreAttnMod1(int New_Pre_Att_Mod_1, int OnlyValueCheck)       //Function to Write new Pre Attenuator Module 1 setting (HEX value same shape as decimal value)
{
    int DecimalSetting;                                                         //Converted Received HEX value in Decimal
    int Old_DecimalSetting;
    int VerifiedValue;                                                          //Decimal value for Valid check
    int Old_Pre_Att_Mod_1;
    
    if ((Info.SET.att_pre != New_Pre_Att_Mod_1) || (OnlyValueCheck == BOOT_UP))
    {
        DecimalSetting = HEXToSameDecimal(New_Pre_Att_Mod_1);                   //Result is always between 0 and 255
        //VerifiedValue = VerifyAttnValue22dB(DecimalSetting);                  //Now check if received value is valid for Attenuator setting. If not, the return value is maximum (>44). Now changed to decimal, 0 to 44
        VerifiedValue = VerifyAttnValue31_5dB(DecimalSetting);                  //Now check if received value is valid for Attenuator setting. If not, the return value is maximum (>63). Now changed to decimal, 0 to 63
        if (VerifiedValue != 127)
        {
            New_Pre_Att_Mod_1 = DecimalToSameHex(VerifiedValue);                //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
            Old_Pre_Att_Mod_1 = Info.SET.att_pre;
            Old_DecimalSetting = HEXToSameDecimal(Old_Pre_Att_Mod_1);           //Result is always between 0 and 255
            //Info.SET.att_pre = New_Pre_Att_Mod_1;                                  //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                      //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                Info.SET.att_pre = New_Pre_Att_Mod_1;
                WriteEEPROM(EE_ADR_PRE_ATTN_MOD_1_SETTING,Info.SET.att_pre);       //Write to EEPROM new setting (if it's changed)
            }
            #if defined(USE_SERIAL_DATA_FUNCTIONS)
            if ((OnlyValueCheck != BOOT_UP) && (Setting_0_5dB_Steps == 0x01))   //Check if not boot-up and step mode is enabled
            {
                StepToValueIn0_5dBSteps(DecimalSetting, Old_DecimalSetting, PRE_ATTENUATOR_MODULE_1);    //Take values to StepFunction
            }
            else
            {
                WriteSerialData(DecimalSetting,PRE_ATTENUATOR_MODULE_1);        //Now finally update the Real attenuator (IC). Give Latch to BC Input Attenuator
            }
            #endif
        }

    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))       //Only reply if asked for by master
    {
         
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Pre_Att_Mod_1;                                 //Pre Attenuator Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                         //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_pre;                                //Read Pre Attenuator Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteInterstageAttnMod1(int New_Interstage_Att_Mod_1, int OnlyValueCheck)     //Function to Write new Interstage Attenuator Module 1 setting (HEX value same shape as decimal value)
{
    int DecimalSetting;                                                                     //Converted Received HEX value in Decimal
    int Old_DecimalSetting;
    int VerifiedValue;                                                                      //Decimal value for Valid check
    int Old_Interstage_Att_Mod_1;
    
    if ((Info.SET.att_int != New_Interstage_Att_Mod_1) || (OnlyValueCheck == BOOT_UP))
    {
        DecimalSetting = HEXToSameDecimal(New_Interstage_Att_Mod_1);                        //Result is always between 0 and 255
        //VerifiedValue = VerifyAttnValue16dB(DecimalSetting);                              //Now check if received value is valid for Attenuator setting. If not, the return value is maximum (>32). Now changed to decimal, 0 to 32
        VerifiedValue = VerifyAttnValue31_5dB(DecimalSetting);                              //Now check if received value is valid for Attenuator setting. If not, the return value is maximum (>63). Now changed to decimal, 0 to 63
        if (VerifiedValue != 127)
        {
            New_Interstage_Att_Mod_1 = DecimalToSameHex(VerifiedValue);                     //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
            Old_Interstage_Att_Mod_1 = Info.SET.att_int;
            Old_DecimalSetting = HEXToSameDecimal(Old_Interstage_Att_Mod_1);                //Result is always between 0 and 255
            //Info.SET.att_int = New_Interstage_Att_Mod_1;                                //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                                  //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                Info.SET.att_int = New_Interstage_Att_Mod_1; 
                WriteEEPROM(EE_ADR_INTERSTAGE_ATTN_MOD_1_SETTING,Info.SET.att_int);     //Write to EEPROM new setting (if it's changed)
            }
            #if defined(USE_SERIAL_DATA_FUNCTIONS)
            if ((OnlyValueCheck != BOOT_UP) && (Setting_0_5dB_Steps == 0x01))               //Check if not boot-up and step mode is enabled
            {
                StepToValueIn0_5dBSteps(DecimalSetting, Old_DecimalSetting, INTERSTAGE_ATTENUATOR_MODULE_1);    //Take values to StepFunction
            }
            else
            {
                WriteSerialData(DecimalSetting,INTERSTAGE_ATTENUATOR_MODULE_1);             //Now finally update the Real attenuator (IC). Give Latch to BC Input Attenuator
            }
            #endif
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))       //Only reply if asked for by master
    {
        
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                      //Framing
        LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Interstage_Att_Mod_1;                                      //Interstage Attenuator Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                              //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                     //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_int;                                     //Read Interstage Attenuator Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteUpstreamEqu(int New_Upstream_Equ, int OnlyValueCheck)                    //Function to Write new Upstream Equalizer setting (HEX value same shape as decimal value)
{
    int DecimalSetting;                                                                     //Converted Received HEX value in Decimal
    int Old_DecimalSetting;
    int VerifiedValue;                                                                      //Decimal value for Valid check
    int Old_Upstream_Equ;
    
    if ((Info.SET.eq_us != New_Upstream_Equ) || (OnlyValueCheck == BOOT_UP))
    {
        DecimalSetting = HEXToSameDecimal(New_Upstream_Equ);                                //Result is always between 0 and 255
        //VerifiedValue = VerifyEquValue10dB(DecimalSetting);                               //Now check if received value is valid for Equalizer setting. If not, the return value is maximum (>20). Now changed to decimal, 0 to 20
        VerifiedValue = VerifyEquValue31_5dB(DecimalSetting);                               //Now check if received value is valid for Equalizer setting. If not, the return value is maximum (>63). Now changed to decimal, 0 to 63
        if (VerifiedValue != 127)
        {
            New_Upstream_Equ = DecimalToSameHex(VerifiedValue);                             //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
            Old_Upstream_Equ = Info.SET.eq_us;
            Old_DecimalSetting = HEXToSameDecimal(Old_Upstream_Equ);                        //Result is always between 0 and 255
            Info.SET.eq_us = New_Upstream_Equ;                                                //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                                  //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                WriteEEPROM(EE_ADR_UPSTREAM_EQU_SETTING,Info.SET.eq_us);                      //Write new settings to EEPROM
            }
            #if defined(USE_SERIAL_DATA_FUNCTIONS)
            if ((OnlyValueCheck != BOOT_UP) && (Setting_0_5dB_Steps == 0x01))               //Check if not boot-up and step mode is enabled
            {
                StepToValueIn0_5dBSteps(DecimalSetting, Old_DecimalSetting, UPSTREAM_EQUALIZER);    //Take values to StepFunction
            }
            else
            {
                WriteSerialData(DecimalSetting,UPSTREAM_EQUALIZER);                             //Give Latch to Upstream Equalizer
            }
            #endif
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))       //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                      //Framing
        LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Upstream_Equ;                                              //Write Upstream Equalizer value
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                              //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                     //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_us;                                             //Write Upstream Equalizer value
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWritePreEquMod1(int New_Pre_Equ_Mod_1, int OnlyValueCheck)                    //Function to Write new Pre Equalizer Module 1 setting (HEX value same shape as decimal value)
{
    int DecimalSetting;                                                                     //Converted Received HEX value in Decimal
    int Old_DecimalSetting;
    int VerifiedValue;                                                                      //Decimal value for Valid check
    int Old_Pre_Equ_Mod_1;
    
    if ((Info.SET.eq_pre != New_Pre_Equ_Mod_1) || (OnlyValueCheck == BOOT_UP))
    {
        DecimalSetting = HEXToSameDecimal(New_Pre_Equ_Mod_1);                               //Result is always between 0 and 255
        //VerifiedValue = VerifyEquValue25dB(DecimalSetting);                               //Now check if received value is valid for Equalizer setting. If not, the return value is maximum (>50). Now changed to decimal, 0 to 50
        VerifiedValue = VerifyEquValue31_5dB(DecimalSetting);                               //Now check if received value is valid for Equalizer setting. If not, the return value is maximum (>63). Now changed to decimal, 0 to 63
        if (VerifiedValue != 127)
        {
            New_Pre_Equ_Mod_1 = DecimalToSameHex(VerifiedValue);                            //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
            Old_Pre_Equ_Mod_1 = Info.SET.eq_pre;
            Old_DecimalSetting = HEXToSameDecimal(Old_Pre_Equ_Mod_1);                       //Result is always between 0 and 255
            Info.SET.eq_pre = New_Pre_Equ_Mod_1;                                              //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                                  //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                WriteEEPROM(EE_ADR_PRE_EQU_MOD_1_SETTING,Info.SET.eq_pre);                    //Write new settings to EEPROM
            }
            #if defined(USE_SERIAL_DATA_FUNCTIONS)
            if ((OnlyValueCheck != BOOT_UP) && (Setting_0_5dB_Steps == 0x01))               //Check if not boot-up and step mode is enabled
            {
                StepToValueIn0_5dBSteps(DecimalSetting, Old_DecimalSetting, PRE_EQUALIZER_MODULE_1);    // Take values to StepFunction
            }
            else
            {
                WriteSerialData(DecimalSetting,PRE_EQUALIZER_MODULE_1);                     //Give Latch to Pre Equalizer Module 1
            }
            #endif
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))       //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                      //Framing
        LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Pre_Equ_Mod_1;                                             //Write Pre Equalizer Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                              //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                     //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_pre;                                            //Write Pre Equalizer Module 1 value
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteInterstageEquMod1(int New_Interstage_Equ_Mod_1, int OnlyValueCheck)      //Function to Write new Interstage Equalizer Module 1 setting (HEX value same shape as decimal value)
{
    int DecimalSetting;                                                                     //Converted Received HEX value in Decimal
    int Old_DecimalSetting;
    int VerifiedValue;                                                                      //Decimal value for Valid check
    int Old_Interstage_Equ_Mod_1;
    
    if ((Info.SET.eq_int != New_Interstage_Equ_Mod_1) || (OnlyValueCheck == BOOT_UP))
    {
        DecimalSetting = HEXToSameDecimal(New_Interstage_Equ_Mod_1);                        //Result is always between 0 and 255
        //VerifiedValue = VerifyEquValue16dB(DecimalSetting);                               //Now check if received value is valid for Equalizer setting. If not, the return value is maximum (>16). Now changed to decimal, 0 to 16
        VerifiedValue = VerifyEquValue31_5dB(DecimalSetting);                               //Now check if received value is valid for Equalizer setting. If not, the return value is maximum (>63). Now changed to decimal, 0 to 63
        if (VerifiedValue != 127)
        {
            New_Interstage_Equ_Mod_1 = DecimalToSameHex(VerifiedValue);                     //To store new value, convert Decimal again to HEX. New updated and checked HEX is stored in original Value
            Old_Interstage_Equ_Mod_1 = Info.SET.eq_int;
            Old_DecimalSetting = HEXToSameDecimal(Old_Interstage_Equ_Mod_1);
            Info.SET.eq_int = New_Interstage_Equ_Mod_1;                                //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                                  //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                WriteEEPROM(EE_ADR_INTERSTAGE_EQU_MOD_1_SETTING,Info.SET.eq_int);      //Write new settings to EEPROM
            }
            #if defined(USE_SERIAL_DATA_FUNCTIONS)
            if ((OnlyValueCheck != BOOT_UP) && (Setting_0_5dB_Steps == 0x01))               //Check if not boot-up and step mode is enabled
            {
                StepToValueIn0_5dBSteps(DecimalSetting, Old_DecimalSetting, INTERSTAGE_EQUALIZER_MODULE_1);    // Take values to StepFunction
            }
            else
            {
                WriteSerialData(DecimalSetting,INTERSTAGE_EQUALIZER_MODULE_1);               //Give Latch to Interstage Equalizer Module 1
            }
            #endif
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))       //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                      //Framing
        LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Interstage_Equ_Mod_1;                                      //Write Interstage Equalizer Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                              //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                     //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_int;                                     //Write Interstage Equalizer Module 1 value
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteATCEnabledDisabled(int New_ATC_Enabled_Disabled, int OnlyValueCheck)                 //Function to Write if ATC is Enabled or Disabled
{
    if((New_ATC_Enabled_Disabled == 0x00) || (New_ATC_Enabled_Disabled == 0x01))
    {
       if ((ATC_Enabled_Disabled != New_ATC_Enabled_Disabled) || (OnlyValueCheck == BOOT_UP))           //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
       {
           ATC_Enabled_Disabled = New_ATC_Enabled_Disabled;                                             //Update non Local Register
           if (ATC_Enabled_Disabled == 1)                                                               //Check if ATC Function is enabled
           {
               ATC_Init();
               ATC_UpdateAllOffsets(DS_MOD_CHANGE_WRITE, DS_OFF_TEMP_UPDATE, DS_ATC_STEPPING); //ATC_update();                                                                            //Update ATC
           }
           if (OnlyValueCheck != BOOT_UP)                                                               //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
           {
               WriteEEPROM(EE_ADR_SETTING_ATC_ENABLED_DISABLED, New_ATC_Enabled_Disabled);              //Write new settings to EEPROM
           }
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))                   //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                                  //Framing
        LoConTransmitString[1] = LoConAddress;                                                          //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                                          //Read address command
        LoConTransmitString[3] = ATC_Enabled_Disabled;                                                  //ATC Enabled or Disabled (0x00 = Disabled 0x01 = Enabled)
        if (ReplyDestination == DESTINATION_LOCON)                                                      //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                                          //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                                        //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                                 //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                                 //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                                         //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                                         //LoCon Command for Request action
            ToSendDataBuffer[5] = ATC_Enabled_Disabled;                                                 //Write ATC Enabled or Disabled (0x00 = Disabled 0x01 = Enabled)
            #endif
        }
    }

}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteDownstreamPowerSwMod1(int New_Downstream_Power_Sw_Mod_1, int OnlyValueCheck)          //Function to Write new Downstream Power Switch Module 1 setting
{
    if((New_Downstream_Power_Sw_Mod_1 == 0x00) || (New_Downstream_Power_Sw_Mod_1 == 0x01))
    {
       if ((Info.SET.pwr_sw != New_Downstream_Power_Sw_Mod_1) || (OnlyValueCheck == BOOT_UP)) //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
       {
           Info.SET.pwr_sw = New_Downstream_Power_Sw_Mod_1;                                   //Update non Local Register
            if (OnlyValueCheck != BOOT_UP)                                                              //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
            {
                WriteEEPROM(EE_ADR_DOWNSTREAM_POWER_SW_MOD_1_SETTING,New_Downstream_Power_Sw_Mod_1);    //Write new settings to EEPROM
            }
            if (Info.SET.pwr_sw == 0x00)
            {
                Downstream_Power_Switch_Mod_1_Off();                                                    //Set output to Low (Off)
            }
            else if (Info.SET.pwr_sw == 0x01)
            {
                Downstream_Power_Switch_Mod_1_On();                                                     //Set output to High (On)
            }
        }
    }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))                   //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                                  //Framing
        LoConTransmitString[1] = LoConAddress;                                                          //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                                          //Read address command
        LoConTransmitString[3] = Downstream_Power_Sw_Mod_1;                                             //Downstream Power Switch Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                                                      //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                                          //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                                        //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                                 //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                                 //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                                         //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                                         //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.pwr_sw;                                            //Write Downstream Power Switch Module 1 value
            #endif
        }
    }

}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteAux1Value(int New_Aux_1_Value, int OnlyValueCheck)                                   //Function to Write new Aux 1 Value
{
       if ((Aux_1_Value != New_Aux_1_Value) || (OnlyValueCheck == BOOT_UP))                             //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
       {
           if (New_Aux_1_Value != 0xFF)
           {
                Aux_1_Value = New_Aux_1_Value;                                                          //Update non Local Register
                if (OnlyValueCheck != BOOT_UP)                                                          //Only Write new EEPROM setting when the DBC-1200 LITE-Amplifier is not just powered up
                {
                    WriteEEPROM(EE_ADR_AUX_1_VALUE, New_Aux_1_Value);                                   //Write new settings to EEPROM
                }
           }
        }
    if(((LoConFraming==0xE2) || (LoConFraming==0xE3)) && (OnlyValueCheck == DEFAULT))                   //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                                  //Framing
        LoConTransmitString[1] = LoConAddress;                                                          //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                                          //Read address command
        LoConTransmitString[3] = Aux_1_Value;                                                           //Write Aux 1 Value for Software
        if (ReplyDestination == DESTINATION_LOCON)                                                      //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                                          //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                                        //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                                 //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                                 //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                                         //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                                         //LoCon Command for Request action
            ToSendDataBuffer[5] = Aux_1_Value;                                                          //Write Aux 1 Value for Software
            #endif
        }
    }

}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteAllAttnAtOnce(int New_Upstream_Att, int New_Pre_Att_Mod_1, int New_Interstage_Att_Mod_1,
                                 int ReceivedBytes)                             //Function to Write all Attenuator settings in one global command.
{
    if (ReceivedBytes == 6)
    {
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        HandleCmdWriteUpstreamAttn(New_Upstream_Att, NON_DEFAULT);              //Process the New_Upstream_Att value to check valid and conversation. Finally send to switch
        HandleCmdWritePreAttnMod1(New_Pre_Att_Mod_1, NON_DEFAULT);              //Process the New_Pre_Att_Mod_1 value to check valid and conversation. Finally send to switch
        HandleCmdWriteInterstageAttnMod1(New_Interstage_Att_Mod_1, NON_DEFAULT);//Process the New_Interstage_Att_Mod_1 value to check valid and conversation. Finally send to switch
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
    }
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                            //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                          //Framing
        LoConTransmitString[1] = LoConAddress;                                  //Reply with same address as called with (might be 0xAF)
        LoConTransmitString[2] = LoConCommand;                                  //Read address command
        LoConTransmitString[3] = Upstream_Att;                                  //Upstream Attenuator value
        LoConTransmitString[4] = Pre_Att_Mod_1;                                 //Pre Attenuator Module 1 value
        LoConTransmitString[5] = Interstage_Att_Mod_1;                          //Interstage Attenuator Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                              //Send information straight to the LoCon Interface
        {
            SendLoCon(6, LoConTransmitString);                                  //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                         //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x06;                                         //6 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                         //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                 //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                 //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.att_us;                                 //Read Upstream Attenuator
            ToSendDataBuffer[6] = Info.SET.att_pre;                                //Read Pre Attenuator Module 1
            ToSendDataBuffer[7] = Info.SET.att_int;                         //Read Interstage Attenuator Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteAllEquAtOnce(int New_Upstream_Equ, int New_Pre_Equ_Mod_1,
                                int New_Interstage_Equ_Mod_1, int ReceivedBytes)            //Function to Write all Equalizer settings in one global command.
{
    if (ReceivedBytes == 6)
    {
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        HandleCmdWriteUpstreamEqu(New_Upstream_Equ, NON_DEFAULT);                           //Process the New_Upstream_Equ value to check valid and conversation. Finally send to switch
        HandleCmdWritePreEquMod1(New_Pre_Equ_Mod_1, NON_DEFAULT);                           //Process the New_Pre_Equ_Mod_1 value to check valid and conversation. Finally send to switch
        HandleCmdWriteInterstageEquMod1(New_Interstage_Equ_Mod_1, NON_DEFAULT);             //Process the New_Interstage_Equ_Mod_1 value to check valid and conversation. Finally send to switch
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
      
    }
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                        //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        LoConTransmitString[0] = 0xE4;                                                      //Framing
        LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
    	LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Upstream_Equ;                                              //Upstream Equalizer value
    	LoConTransmitString[4] = Pre_Equ_Mod_1;                                             //Pre Equalizer Module 1 value
    	LoConTransmitString[5] = Interstage_Equ_Mod_1;                                      //Interstage Equalizer Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(6, LoConTransmitString);                                              //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x06;                                                     //8 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.eq_us;                                             //Read Upstream Equalizer
            ToSendDataBuffer[6] = Info.SET.eq_pre;                                            //Read Pre Equalizer Module 1
            ToSendDataBuffer[7] = Info.SET.eq_int;                                     //Read Interstage Equalizer Module 1
            #endif
        }
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteAllSwAtOnce(int New_Downstream_Power_Sw_Mod_1, 
                               int ReceivedBytes)                                           //Function to Write all Equalizer settings in one global command.
{
    if (ReceivedBytes == 11)
    {
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        HandleCmdWriteDownstreamPowerSwMod1(New_Downstream_Power_Sw_Mod_1, NON_DEFAULT);    //Process the New_Downstream_Power_Sw_Mod_1 value to check valid and conversation. Finally send to switch
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
    }
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                        //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                                      //Framing
    	LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
    	LoConTransmitString[2] = LoConCommand;                                              //Read address command
    	LoConTransmitString[3] = Downstream_Power_Sw_Mod_1;                                 //Downstream Power Switch Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(4, LoConTransmitString);                                             //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x04;                                                     //4 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = LoConCommand;                                             //LoCon Command for Request action
            ToSendDataBuffer[5] = Info.SET.pwr_sw;                                //Downstream Power Switch Module 1 value
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdWriteAllSettingsAtOnce(int New_Upstream_Att, int New_Pre_Att_Mod_1, int New_Interstage_Att_Mod_1, 
                               int New_Upstream_Equ, int New_Pre_Equ_Mod_1, int New_Interstage_Equ_Mod_1,
                               int New_Downstream_Power_Sw_Mod_1, 
                               int ReceivedBytes)                                           //Function to Write all Equalizer settings in one global command.
{
    if (ReceivedBytes == 10)
    {
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        HandleCmdWriteUpstreamAttn(New_Upstream_Att, NON_DEFAULT);                          //Process the New_Upstream_Att value to check valid and conversation. Finally send to switch
        HandleCmdWritePreAttnMod1(New_Pre_Att_Mod_1, NON_DEFAULT);                          //Process the New_Pre_Att_Mod_1 value to check valid and conversation. Finally send to switch
        HandleCmdWriteInterstageAttnMod1(New_Interstage_Att_Mod_1, NON_DEFAULT);            //Process the New_Interstage_Att_Mod_1 value to check valid and conversation. Finally send to switch
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        HandleCmdWriteUpstreamEqu(New_Upstream_Equ, NON_DEFAULT);                           //Process the New_Upstream_Equ value to check valid and conversation. Finally send to switch
        HandleCmdWritePreEquMod1(New_Pre_Equ_Mod_1, NON_DEFAULT);                           //Process the New_Pre_Equ_Mod_1 value to check valid and conversation. Finally send to switch
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        HandleCmdWriteInterstageEquMod1(New_Interstage_Equ_Mod_1, NON_DEFAULT);             //Process the New_Interstage_Equ_Mod_1 value to check valid and conversation. Finally send to switch

        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
        HandleCmdWriteDownstreamPowerSwMod1(New_Downstream_Power_Sw_Mod_1, NON_DEFAULT);    //Process the New_Downstream_Power_Sw_Mod_1 value to check valid and convertation. Finally send to switch
        
        #if defined(USB_INTERNAL_FUNCTIONS)
        {
            USBDeviceTasks();
        }
        #endif
    }
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                        //Only reply if asked for by master
    {
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    	LoConTransmitString[0] = 0xE4;                                                      //Framing
    	LoConTransmitString[1] = LoConAddress;                                              //Reply with same address as called with (might be 0xAF)
    	LoConTransmitString[2] = LoConCommand;                                              //Read address command
        LoConTransmitString[3] = Upstream_Att;                                              //Upstream Attenuator value
    	LoConTransmitString[4] = Pre_Att_Mod_1;                                             //Pre Attenuator Module 1 value
    	LoConTransmitString[5] = Interstage_Att_Mod_1;                                      //Interstage Attenuator Module 1 value
     	LoConTransmitString[6] = Upstream_Equ;                                              //Upstream Equalizer value
    	LoConTransmitString[7] = Pre_Equ_Mod_1;                                             //Pre Equalizer Module 1 value
    	LoConTransmitString[8] = Interstage_Equ_Mod_1;                                      /Interstage Equalizer Module 1 value
    	LoConTransmitString[9] = Downstream_Power_Sw_Mod_1;                                 /Downstream Power Switch Module 1 value
        if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            SendLoCon(10, LoConTransmitString);                                             //Send reply to LoCon-master
        }
        #endif
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0]  = 0x40;                                                    //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1]  = 0x0A;                                                    //10 bytes back as reply
            ToSendDataBuffer[2]  = 0xE4;                                                    //LoCon Command for Date request
            ToSendDataBuffer[3]  = LoConAddress;                                            //LoCon Address for Addressing device
            ToSendDataBuffer[4]  = LoConCommand;                                            //LoCon Command for Request action
            ToSendDataBuffer[5]  = Info.SET.att_us;                                            //Read Upstream Attenuator
            ToSendDataBuffer[6]  = Info.SET.att_pre;                                           //Read Pre Attenuator Module 1
            ToSendDataBuffer[7]  = Info.SET.att_int;                                    //Read Interstage Attenuator Module 1
            ToSendDataBuffer[8]  = Info.SET.eq_us;                                            //Read Upstream Equalizer
            ToSendDataBuffer[9]  = Info.SET.eq_pre;                                           //Read Pre Equalizer Module 1
            ToSendDataBuffer[10] = Info.SET.eq_int;                                    //Read Interstage Equalizer Module 1
            ToSendDataBuffer[11] = Info.SET.pwr_sw;                               //Downstream Power Switch Module 1 value
            #endif
        }
    }
}
#endif


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdSetToReset(void)                                                              //Function to Reset the device with a LoCon Command
{
    int i;
    int j;
    
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                        //Only reply if asked for by master
    {
    	if (ReplyDestination == DESTINATION_LOCON)                                          //Send information straight to the LoCon Interface
        {
            #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
            LoConTransmitString[0] = 0xE4;                                                  //Framing
            LoConTransmitString[1] = LoConAddress;                                          //Reply with same address as called with (might be 0xAF)
            LoConTransmitString[2] = LoConCommand;                                          //Read address command
            LoConTransmitString[3] = LOCON_CMD_SET_RESET;                                   //Reset device command
            if (ReplyDestination == DESTINATION_LOCON)                                      //Send information straight to the LoCon Interface
            {
                SendLoCon(4, LoConTransmitString);                                          //Send reply to LoCon-master
            }
            #endif
        }
        if (ReplyDestination == DESTINATION_USB)                                            //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                     //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x03;                                                     //3 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                     //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                             //LoCon Address for Addressing device
            ToSendDataBuffer[4] = USB_LOCON_CMD_SET_RESET;                                  //Reset USB device for new detection
            if(!HIDTxHandleBusy(USBInHandle))
            {
                ReplyUSB = FALSE;
                SendToUSB();                                                                //Send answer to USB
            }
            #endif
        }
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        for (i=0;i<5;i++)
        {
            mLED_Alarm_On();
            for (j=0 ;j<15;j++)
            {
                LoConDelay(10000);
            }
            mLED_Alarm_Off();
            for (j=0 ;j<15;j++)
            {
                LoConDelay(10000);
            }
        }
        #endif
        #if defined(USB_INTERNAL_FUNCTIONS)
        for (i=0;i<1;i++)
        {
            mLED_Alarm_On();
            for (j=0 ;j<15;j++)
            {
                USBDelay(10000);
            }
            mLED_Alarm_Off();
        }
        #endif
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
            ClearLoConReceiveBuffers();                                                 //Clear LoCon Receive buffers
        #endif
        Reset_Micro();
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
void HandleCmdSetToFactoryDefault(void)                                                 //Function to set all settings to default
{
    int i;
    int j;
    int Temp_Value;
    
    OwnLoConAddress = 0xA0;                                                             //Setting to Factory default, EEPROM is not set yet
    WriteEEPROM(EE_ADR_OWNLOCONADDRESS, OwnLoConAddress);                               //Set OwnLoCon address to Factory default 0xA0

    ModifiedBit = 0x00;                                                                 //Setting to Factory default, EEPROM is not set yet
    WriteEEPROM(EE_ADR_MODIFIED_BIT, ModifiedBit);                                      //Set Modified bit to Factory default 0x00


    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_ATTN_SETTING);              //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_UPSTREAM_ATTN_SETTING , Temp_Value);                         //Set Upstream Attenuator setting to Factory default 0x00 (0dB)
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_ATTN_MOD_1_SETTING);             //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_PRE_ATTN_MOD_1_SETTING , Temp_Value);                        //Set Pre Attenuator for Module 1 to Factory default 0x00 (0dB)
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_ATTN_MOD_1_SETTING);      //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_INTERSTAGE_ATTN_MOD_1_SETTING , Temp_Value);                 //Set Interstage Attenuator for Module 1 to Factory default 0x00 (0dB)
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_UPSTREAM_EQU_SETTING);               //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_UPSTREAM_EQU_SETTING , Temp_Value);                          //Set Upstream Equalizer to Factory default 0x00 (0dB)
    }
    #if defined(USB_INTERNAL_FUNCTIONS)
    {
         USBDeviceTasks();
    }
    #endif

    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_PRE_EQU_MOD_1_SETTING);              //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_PRE_EQU_MOD_1_SETTING  , Temp_Value);                        //Set Pre Equalizer for Module 1 to Factory default 0x00 (0dB)
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_INTERSTAGE_EQU_MOD_1_SETTING);       //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_INTERSTAGE_EQU_MOD_1_SETTING  , Temp_Value);                 //Set Interstage Equalizer for Module 1 to Factory default 0x00 (0dB)
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_DOWNSTREAM_POWER_SW_MOD_1_SETTING);  //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_DOWNSTREAM_POWER_SW_MOD_1_SETTING , Temp_Value);             //Set Downstream Power Switch Module 1 to Factory default 0x00 (Off, high power mode selected)
    }
    #if defined(USB_INTERNAL_FUNCTIONS)
    {
         USBDeviceTasks();
    }
    #endif
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_TYPE_ID_AMPLIFIER);                  //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_TYPE_ID_AMPLIFIER , Temp_Value);                             //Set the Amplifier type to Factory default
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_ENABLED);          //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_SETTING_0_5_STEPS_ENABLED , Temp_Value);                     //Set the Amplifier type to Factory default
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_TIME_DELAY_MS);    //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_SETTING_0_5_STEPS_TIME_DELAY_MS , Temp_Value);               //Set the Amplifier type to Factory default
    }
    Temp_Value = ReadEEPROM(EE_ADR_FACTORY_RESTORE_SETTING_EQ_HINGE_DS1);               //Setting to Factory default, EEPROM is not set yet
    if (Temp_Value != 0xFF)
    {
        WriteEEPROM(EE_ADR_SETTING_EQ_HINGE_DS1 , Temp_Value);                          //Set the Amplifier type to Factory default
    }
    if((LoConFraming==0xE2) || (LoConFraming==0xE3))                                    //Only reply if asked for by master
    {

    	if (ReplyDestination == DESTINATION_LOCON)                                      //Send information straight to the LoCon Interface
        {
            #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
            LoConTransmitString[0]=0xE4;                                                //Framing
            LoConTransmitString[1]=LoConAddress;                                        //Reply with same address as called with (might be 0xAF)
            LoConTransmitString[2]=LoConCommand;                                        //Read address command
            LoConTransmitString[3]=LOCON_CMD_SET_FACTORY_DEFAULT ;                      //Set all settings back to default
            if (ReplyDestination == DESTINATION_LOCON)                                  //Send information straight to the LoCon Interface
            {
                SendLoCon(4, LoConTransmitString);                                      //Send reply to LoCon-master
            }
            #endif
            
        }
        if (ReplyDestination == DESTINATION_USB)                                        //Send the information to the USB port to Host
        {
            #if defined(USB_INTERNAL_FUNCTIONS)
            ToSendDataBuffer[0] = 0x40;                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
            ToSendDataBuffer[1] = 0x03;                                                 //3 bytes back as reply
            ToSendDataBuffer[2] = 0xE4;                                                 //LoCon Command for Date request
            ToSendDataBuffer[3] = LoConAddress;                                         //LoCon Address for Addressing device
            ToSendDataBuffer[4] = USB_LOCON_CMD_SET_FACTORY_DEFAULT;                    //Set all settings back to default
            if(!HIDTxHandleBusy(USBInHandle))
            {
                ReplyUSB = FALSE;
                SendToUSB();                                                            //Send answer to USB
            }
            #endif
        }
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
        for (i=0;i<5;i++)
        {
            mLED_Alarm_On();
            for (j=0 ;j<15;j++)
            {
                LoConDelay(10000);
            }
            mLED_Alarm_Off();
            for (j=0 ;j<15;j++)
            {
                LoConDelay(10000);
            }
        }
        #endif
        #if defined(USB_INTERNAL_FUNCTIONS)
        for (i=0;i<5;i++)
        {
            mLED_Alarm_On();
            for (j=0 ;j<15;j++)
            {
                USBDelay(3000);
            }
            mLED_Alarm_Off();
            for (j=0 ;j<15;j++)
            {
                USBDelay(3000);
            }
        }
        #endif
        #if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
            ClearLoConReceiveBuffers();                                                 //Clear LoCon Receive buffers
        #endif
        Reset_Micro();                                                                  //Bye bye, Kill all applications and -->RESET!<--
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
void ArraytoLoConFunction(void)
{
    LoConFraming= (int) LoConReceiveString[0];                                          //Store received LoCon Framing in variable
    LoConAddress= (int) LoConReceiveString[1];                                          //Store received LoCon Address in variable
    LoConCommand= (int) LoConReceiveString[2];                                          //Store received LoCon Command in variable
    LoConData   = (int) LoConReceiveString[3];                                          //Store received LoCon Data in variable
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
void ExecuteLoConCommand(int LoConCommand)                                      //Function to execute received LoCon command
{    
	switch(LoConCommand)                                                        //Switch function to find function at received LoCon command
	{
        case    LOCON_CMD_WRITE_SETTING_EQ_HINGE_DS1 :                          //Check if received command is to write the state of EQ Hinge Downstream Module 1
                HandleCmdWriteSetting_EQ_Hinge_DS_1(LoConData, DEFAULT);        //Write actual state of EQ Hinge Downstream Module 1
        break;
        case    LOCON_CMD_READ_AUX_1_VALUE_BLL :                                //Read software only to provide a future function Aux 1
                HandleCmdReadAux1Value();                                       //Read actual state of the Aux 1
        break;
        case    LOCON_CMD_WRITE_AUX_1_VALUE_BLL :                               //Write software only to provide a future function Aux 1
                HandleCmdWriteAux1Value(LoConData, DEFAULT);                    //Write state of the Aux 1
        break;
        case    LOCON_CMD_READ_FACTORY_DEFAULT_SETTING :                        //Check if received command is to read the state of the type of Amplifier
                HandleCmdReadFactoryDefaultSetting();                           //Read actual factory default settings
        break;
        case   LOCON_CMD_WRITE_FACTORY_DEFAULT_SETTING :                       //Check if received command is to write the setting of all Attenuators, Equalizers and Switches in one command
               HandleCmdWriteFactoryDefaultSetting(LoConReceiveString[3], LoConReceiveString[4], LoConReceiveString[5], LoConReceiveString[6], LoConReceiveString[7],
                                                   LoConReceiveString[8], LoConReceiveString[9], LoConReceiveString[10], LoConReceiveString[11], LoConReceiveString[12],
                                                   LoConReceiveString[12], NrOfReceivedLoConBytes);           //Write new factory default settings
        break;
        case    LOCON_CMD_READ_SETTING_0_5_STEPS_ENABLED :                      //Check if received command is to read the state of the type of Amplifier
                HandleCmdReadSetting_0_5_Steps_Enabled();                       //Read actual state oof the Step mode enabled or disabled
        break;
        case    LOCON_CMD_WRITE_SETTING_0_5_STEPS_ENABLED :                     //Check if received command is to write the state of the type of Amplifier
                HandleCmdWriteSetting_0_5_Steps_Enabled(LoConData, DEFAULT);//Write if the 0.5dB Steps is enabled or disabled
        break;
        case    LOCON_CMD_READ_STEP_0_5_STEPS_TIME_DELAY_MS :                   //Check if received command is to read the state of the type of Amplifier
                HandleCmdReadSetting_0_5_Steps_Time_Delay_Ms();                 //Read actual state of the type of Amplifier
        break;
        case    LOCON_CMD_WRITE_STEP_0_5_STEPS_TIME_DELAY_MS :                  //Check if received command is to write the state of the type of Amplifier
                HandleCmdWriteSetting_0_5_Steps_Time_Delay_Ms(LoConData, DEFAULT);//Write if the 0.5dB Steps is enabled or disabled
        break;
        case 	LOCON_CMD_WRITE_ADDRESS_0 :                                     //Check if Address received address is 0xA0
				HandleCmdWriteAddress(0xA0);                                    //Write LoCOn Address 0
		break;
		case 	LOCON_CMD_WRITE_ADDRESS_1 :                                     //Check if Address received address is 0xA1
	   			HandleCmdWriteAddress(0xA1);                                    //Write LoCOn Address 1
		break;
		case 	LOCON_CMD_WRITE_ADDRESS_2 :                                     //Check if Address received address is 0xA2
				HandleCmdWriteAddress(0xA2);                                    //Write LoCOn Address 2
		break;
		case 	LOCON_CMD_WRITE_ADDRESS_3 :                                     //Check if Address received address is 0xA3
				HandleCmdWriteAddress(0xA3);                                    //Write LoCon Address 3
		break;
		case 	LOCON_CMD_WRITE_ADDRESS_4 :                                     //Check if Address received address is 0xA4
				HandleCmdWriteAddress(0xA4);                                    //Write LoCon Address 4
		break;
		case 	LOCON_CMD_WRITE_ADDRESS_5 :                                     //Check if Address received address is 0xA5
				HandleCmdWriteAddress(0xA5);                                    //Write LoCon Address 5
		break;
		case 	LOCON_CMD_WRITE_ADDRESS_6 :                                     //Check if Address received address is 0xA6
				HandleCmdWriteAddress(0xA6);                                    //Write LoCon Address 6
		break;
  		case 	LOCON_CMD_WRITE_ADDRESS_7 :                                     //Check if Address received address is 0xA7
				HandleCmdWriteAddress(0xA7);                                    //Write LoCon Address 7
		break;
  		case 	LOCON_CMD_WRITE_ADDRESS_8 :                                     //Check if Address received address is 0xA8
				HandleCmdWriteAddress(0xA8);                                    //Write LoCon Address 8
		break;
  		case 	LOCON_CMD_READ_ADDRESS :                                        //Check if received command is Read LoCon Address
				HandleCmdReadAddress();                                         //Read LoCon-address from EEPROM
		break;
  		case 	LOCON_CMD_REQUEST_ID :                                          //Check if received command is to read ID of Product
				HandleCmdRequestID();                                           //Send Device-ID to Master
		break;
  		case 	LOCON_CMD_READ_ERRORS :                                         //Check if received command is to read errors in 1 byte
				HandleCmdReadErrors();                                          //Send Error-status to Master
		break;
  		case 	LOCON_CMD_READ_STATUS :                                         //Check if received command is to read status of the product
				HandleCmdReadStatus();                                          //Send status code to master
		break;
  		case 	LOCON_CMD_RESET_MODIFIED_BIT :                                  //Check if received command is to reset the modified bit
  				HandleCmdResetModifiedBit();                                    //Clear modified bit
  		break;
  		case 	LOCON_CMD_SET_MODIFIED_BIT :                                    //Check if received command is to set the modified bit
  				HandleCmdSetModifiedBit();                                      //Set modified bit
  		break;
  		case 	LOCON_CMD_READ_MODIFIED_BIT :                                   //Check if received command is to read the modified bit
  				HandleCmdReadModifiedBit();                                     //Read modified bit
  		break;
  		case 	LOCON_CMD_READ_FIRMWAREVERSION :                                //Check if received command is to read the actual firmware version number
  				HandleCmdReadFirmwareVersion();                                 //Read actual firmware version (2 bytes)
  		break;
        case    LOCON_CMD_READ_UPSTREAM_ATTN :                                  //Check if received command is to read the actual state of the Upstream Attenuator
                HandleCmdReadUpstreamAttn();                                    //Read actual state of the Upstream Attenuator (1 byte)
        break;
        case    LOCON_CMD_READ_PRE_ATTN_MOD_1 :                                 //Check if received command is to read the actual state of the Pre Attenuator Module 1
                HandleCmdReadPreAttnMod1();                                     //Read actual state of the Pre Attenuator Module 1 (1 byte)
        break;
        case    LOCON_CMD_READ_INTERSTAGE_ATTN_MOD_1   :                        //Check if received command is to read the actual state of the Interstage Attenuator Module 1
                HandleCmdReadInterstageAttnMod1();                              //Read actual state of the Interstage Attenuator Module 1 (1 byte)
        break;
        case    LOCON_CMD_READ_UPSTREAM_EQU :                                   //Check if received command is to read the actual state of the Upstream Equalizer
                HandleCmdReadUpstreamEqu();                                     //Read actual state of the Upstream Equalizer (1 byte)
        break;
        case    LOCON_CMD_READ_PRE_EQU_MOD_1 :                                  //Check if received command is to read the actual state of the Pre Equalizer Module 1
                HandleCmdReadPreEquMod1();                                      //Read actual state of the Pre Equalizer Module 1 (1 byte)
        break;
        case    LOCON_CMD_READ_INTERSTAGE_EQU_MOD_1 :                           //Check if received command is to read the actual state of the Interstage Equalizer Module 1
                HandleCmdReadInterstageEquMod1();                               //Read actual state of the Interstage Equalizer Module 1 (1 byte)
        break;
        case    LOCON_CMD_READ_ATC_TEMPERATURE  :                               //Check if received command is to read the actual state of ATC Temperature
                HandleCmdReadATCTemperature();                                  //Read actual state of the ATC Temperature
        break;      
        case    LOCON_CMD_READ_ATC_ENABLED_DISABLED :                           //Check if received command is to read the actual state of ATC Enabled or Disabled
                HandleCmdReadATCEnabledDisabled(LoConData, DEFAULT);            //Read actual state of the ATC Enabled or Disabled (1 byte)
        break;          
        case    LOCON_CMD_READ_DOWNSTREAM_POWER_SW_MOD_1 :                      //Check if received command is to read the actual state of the Downstream Power Switch Module 1
                HandleCmdReadDownstreamPowerSwMod1();                           //Read actual state of the Downstream Power Switch Module 1 (1 byte)
        break;
        case    LOCON_CMD_READ_PRODUCT_MAINBOARD_IDENTIFIER :                   //Check if received command is to read the Motherboard version (DBD or DBC or DBC-1200 LITE Amplifier)
                HandleCmdReadProductMainboardIdentifier();                      //Read actual state of the IDS Switch On Off Output 2 (-6dB/-40dB)(1 byte)
        break;
        case    LOCON_CMD_READ_TYPE_ID_AMPLIFIER_BLL :                          //Check if received command is to read the Type of Amplifier ID for BLL
                HandleCmdReadTypeIDAmplifier();                                 //Read actual state of the Type of Amplifier ID for BLL
        break;
        case    LOCON_CMD_WRITE_UPSTREAM_ATTN :                                 //Check if received command is to write the actual state of the Broadcast Output attenuator
                HandleCmdWriteUpstreamAttn(LoConData, DEFAULT);                 //Write actual state of the BroadCast Output attenuator (1 byte)
        break;
        case    LOCON_CMD_WRITE_PRE_ATTN_MOD_1 :                                //Check if received command is to write the actual state of the Pre Attenuator Module 1
                HandleCmdWritePreAttnMod1(LoConData, DEFAULT);                  //Write actual state of the Pre Attenuator Module 1 (1 byte)
        break;
        case    LOCON_CMD_WRITE_INTERSTAGE_ATTN_MOD_1 :                         //Check if received command is to write the actual state of the Interstage Attenuator Module 1
                HandleCmdWriteInterstageAttnMod1(LoConData, DEFAULT);           //Write actual state of the Interstage Attenuator Module 1 (1 byte)
        break;
        case    LOCON_CMD_WRITE_UPSTREAM_EQU :                                  //Check if received command is to write the actual state of the Upstream Equalizer
                HandleCmdWriteUpstreamEqu(LoConData, DEFAULT);                  //Write actual state of the Upstream Equalizer (1 byte)
        break;
        case    LOCON_CMD_WRITE_PRE_EQU_MOD_1 :                                 //Check if received command is to write the actual state of the Pre Equalizer Module 1
                HandleCmdWritePreEquMod1(LoConData, DEFAULT);                   //Write actual state of the Pre Equalizer Module 1 (1 byte)
        break;
        case    LOCON_CMD_WRITE_INTERSTAGE_EQU_MOD_1 :                          //Check if received command is to write the actual state of the Interstage Equalizer Module 1
                HandleCmdWriteInterstageEquMod1(LoConData, DEFAULT);            //Write actual state of the Interstage Equalizer Module 1 (1 byte)
        break;
        case    LOCON_CMD_WRITE_ATC_ENABLED_DISABLED :                          //Check if received command is to write the actual state of ATC Enabled or Disabled
                HandleCmdWriteATCEnabledDisabled(LoConData, DEFAULT);           //Write actual state of the ATC Enabled or Disabled (1 byte)
        break;        
        case    LOCON_CMD_WRITE_DOWNSTREAM_POWER_SW_MOD_1 :                     //Check if received command is to write the actual state of the Power Switch Module 1
                HandleCmdWriteDownstreamPowerSwMod1(LoConData, DEFAULT);        //Write actual state of the Power Switch Module 1 (1 byte)
        break;
        case    LOCON_CMD_WRITE_TYPE_ID_AMPLIFIER_BLL :                         //Check if received command is to write the actual state of the Type of Amplifier ID for BLL
                HandleCmdWriteTypeIDAmplifier(LoConData, DEFAULT);              //Write the Type of Amplifier ID for BLL
        break;
        case    LOCON_CMD_READ_ALL_ATTN_AT_ONCE :                               //Check if received command is to read the setting of all Attenuators in 1 command
                HandleCmdReadAllAttnAtOnce();                                   //Read the setting of all Attenuators in 1 command (5 bytes)
        break;
        case    LOCON_CMD_READ_ALL_EQU_AT_ONCE :                                //Check if received command is to read the setting of all Equalizers in 1 command
                HandleCmdReadAllEquAtOnce();                                    //Read actual state of the setting of all Equalizers in 1 command (5 bytes)
        break;
        case    LOCON_CMD_READ_ALL_SWITCHES_AT_ONCE :                           //Check if received command is to read the the setting of all Switches in 1 command
                HandleCmdReadAllSwAtOnce();                                     //Read actual state of the the setting of all Switches in 1 command (8 bytes)
        break;
        case    LOCON_CMD_READ_ALL_SETTINGS_AT_ONCE :                           //Check if received command is to read the actual state of all Settings in one command.
                HandleCmdReadAllSettingsAtOnce();                               //Read actual state of All settings in one command (5x Attenuator 5x Equalizer and 8x switches = 18 bytes)
        break;
        case    LOCON_CMD_WRITE_ALL_ATTN_AT_ONCE :                              //Check if received command is to write the actual state of all Attenuators in one command.
                HandleCmdWriteAllAttnAtOnce(LoConReceiveString[3], LoConReceiveString[4], 
                                            LoConReceiveString[5], NrOfReceivedLoConBytes);
        break;                                                                  //Write actual state of All attenuators
        case    LOCON_CMD_WRITE_ALL_EQU_AT_ONCE :                               //Check if received command is to write the actual state of all equalisers, attenuators and switches
                HandleCmdWriteAllEquAtOnce(LoConReceiveString[3], LoConReceiveString[4], 
                                           LoConReceiveString[5], NrOfReceivedLoConBytes);
        break;                                                                  //Write actual state of All Equalizers
        case    LOCON_CMD_WRITE_ALL_SWITCHES_AT_ONCE :                          //Check if received command is to write the actual switch states
                HandleCmdWriteAllSwAtOnce(LoConReceiveString[3], NrOfReceivedLoConBytes);
        break;                                                                  //Write actual state of All Switches
        case    LOCON_CMD_WRITE_ALL_SETTINGS_AT_ONCE :                          //Check if received command is to write the setting of all Attenuators, Equalizers and Switches in one command
                HandleCmdWriteAllSettingsAtOnce(LoConReceiveString[3], LoConReceiveString[4], LoConReceiveString[5], LoConReceiveString[6], LoConReceiveString[7],
                                                LoConReceiveString[8], LoConReceiveString[9], NrOfReceivedLoConBytes);           //Write actual state of All attenuators      
        break;                                                                  //Write actual state of all Attenuators, Equalizers and Switches
        case    LOCON_CMD_SET_RESET   :                                         //Check if received command is to Reset the DBC-1200 LITE-Amplifier
                HandleCmdSetToReset();                                          //Just reset the DBC-1200 LITE-Amplifier
        break;
        case    LOCON_CMD_SET_FACTORY_DEFAULT  :                                //Check if received command is to set the DBC-1200 LITE-Amplifier to Factory Default position
                HandleCmdSetToFactoryDefault();                                 //Set the DBC-1200 LITE-Amplifier back to Factory Default settings
        break;
        default:                                                                //If command is not given in list above, give back message 0xE5
            HandleCmdUnknown();                                                 //Recycle bin to all commands which are not defined LoCon commands for Product
        break;
 	}
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
void ReceiveLoConFunction(void)                                                 //Function to receive LoCon data from LoCon Bus
{
    NrOfReceivedLoConBytes = ReceiveLoCon(LoConReceiveString);                  //Receive the data and calculate the number of bytes received
    if(NrOfReceivedLoConBytes != -1)                                            //Check if valid number of bytes is received If any data received
    {
        if(NrOfReceivedLoConBytes != -2)                                        //Check if valid number of bytes is received Check if parity OK
        {
            ArraytoLoConFunction();
            if ((ReplyDestination == DESTINATION_LOCON))
            {
                if(ValidLoConMasterFraming(LoConFraming) && ValidLoConAddress(OwnLoConAddress, LoConAddress))       //Back to main loop if no valid framing code or address
                {
                       	ExecuteLoConCommand(LoConCommand);                                                      	//Execute LoCon Command to find function for the LoCon action
                }
            }
        }
    }
    if ((ReplyDestination == DESTINATION_LOCON))
    {
    	ClearLoConTransmitBuffers();                                            //Clear all LoCon transmit buffers to prevent wrong data later on.
        ClearLoConReceiveBuffers();                                             //Clear all LoCon receive buffers to prevent wrong data later on.
    }
}
#endif
