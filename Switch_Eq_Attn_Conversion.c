//*************************************************************************
//
//Description:	Source-file for Microchip-firmware for MaCom & Peregrine Attenuators
//		Contains all functionality to change a received value to a readable value
//
//Filename	: Switch_Eq_Attn_Conversion.c
//Version	: 0.1
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 20-09-2012 Modification: First start for implementing this calculation file
// 26-09-2012 Modification: First Conversion list finished. All General conversion functions are implemented. Ready for Beta Release.
// 04-12-2012 Modification: Added new functions to detect Att 10 and 22dB. Also Equ 10dB, 15dB and 18dB implemented.
// 11-12-2012 Modification: Function checked and ready for Pre release
//*************************************************************************
//#include "/USB/Include/Compiler.h"
//#include "/USB/Include/GenericTypeDefs.h"
//#include "Definitions.h"
//#include "main.h"
//
//#include "Serial_Data_V01.h"
////#include "Definitions.h"
////#include "/USB/Include/GenericTypeDefs.h"
//#include "/USB/Include/Compiler.h"
////#include "Microprocessor_Selector.h"
////#include "USB_Commands.h"
////#include "LoCon_Interface_PIC18.h"
////#include "LoCon_Commands.h"
//
//#include "/USB/Include/Usb/usb.h"
//#include "/USB/Include/Usb/usb_function_hid.h"
//#include "Pin description PIC18F45K50.h"
////#include "LoCon_USB-LoCon.h"
////#include "EEPROM_Function.h"
#include "library.h"

extern int Setting_0_5dB_Steps_Time_Delay;

#if defined(USE_ADC_FUNCTIONS)
int Calculate_ADC_mA_Value(int ADC_Measured_Value)                                          //Function to Convert the ADC value to
{
    int temp;                                                                               //Local help value
    int AmplifierCurrentmA = 0;                                                             //Set the measured value in mA first to 0
    for(temp = 0; (ADC_Measured_Value >= temp); temp = (temp + 50))                         //Try to determine the actual mA value. Increase the ADC value in a loop till the mA value is reached
    {
        AmplifierCurrentmA = AmplifierCurrentmA + 20;                                       //Measure 0..409mA in 20,46 steps
    }
    return (AmplifierCurrentmA);                                                            //Give back measured value in mA for communicating with LoCon/USB
}
#endif

#if defined(USE_ADC_FUNCTIONS)
int CheckAmplifierCurrent(void)                                                             //This function checks if the Amplifier current in the defined window
{
    int ADC_Value;
    int Alarm;
    ADC_Value = ReadCurrentMonitorADC();                                                    //Use ADC to read the I/O pin voltage.

    if((ADC_Value >= UNDER_BOUNDERY_RF_LEVEL ) && (ADC_Value <= UPPER_BOUNDERY_RF_LEVEL ))  //Check if the Amplifier current level is in between windows as given definition.h
    {
        Alarm = 0;                                                                          //No Alarm, Value inside alarm window
    }
    else
    {
        Alarm = 1;                                                                          //Alarm! Value is outer limits given in definitions.h
    }
    return(Alarm);                                                                          //Give back to caller the AmplifierCurrentAlarm Status
}
#endif

int DecimalToSameHex(int ValueDecimal)                                                      //Function translate a Decimal value in a same format HEX value. 63 Decimal will be 0x63 HEX
{
    int Returnvalue;                                                                        //Value to give back to caller after conversion

    Returnvalue = (ValueDecimal / 10) << 4;                                                 //Calculate MSB nibble
    Returnvalue = Returnvalue | (ValueDecimal % 10);                                        //Calculate MSB + LSB nibble
    return (Returnvalue);                                                                   //Give back result
}

int HEXToSameDecimal(int ValueHEX)                                                          //Function to translate a HEX value to a decimal value
{
    int temp_H;                                                                             //Temp value for high nibble
    int temp_L;                                                                             //Temp value for low nibble
    int ReturnValue;                                                                        //Value to give back to caller

    temp_H = ValueHEX & 0xF0;                                                               //Obtain the upper 4 bits (MSBs) of hex number
    temp_H = temp_H >> 4;                                                                   //Rotate nibble to be on the LSB position
    temp_H = (temp_H * 10);                                                                 //Value of high nibble x 10

    temp_L = ValueHEX & 0x0F;                                                               //Obtain the lower 4 bits (LSBs) of HEX number

    ReturnValue = temp_H + temp_L;                                                          //Add both values to create decimal
    return (ReturnValue);                                                                   //Return Decimal
}

int VerifyAttnValue31_5dB(int AttnValue)
{
    if(AttnValue > 63)                                                          //(63/2) = 31.5dB, so when value is bigger than 31.5dB
    {
        AttnValue = 127;                                                        //(63/2) = 31.5dB. Set attenuator invalid So a value above this 31.5dB will be denied (error code = 127)
    }
    return(AttnValue);                                                          //Return checked Attenuator value to caller
}

int VerifyAttnValue22dB(int AttnValue)                                          //Function to verify if Attenuator value is valid
{
    if(AttnValue > 44)                                                          //(44/2) = 22dB, so when value is bigger than 22dB
    {
        AttnValue = 127;                                                        //(44/2) = 22dB. Set attenuator invalid So a value above this 22dB will be denied (error code = 127)
    }
    return(AttnValue);                                                          //Return checked Attenuator value to caller
}

int VerifyAttnValue16dB(int AttnValue)                                          //Function to verify if Attenuator value is valid
{
    if(AttnValue > 32)                                                          //(32/2) = 16dB, so when value is bigger than 16dB
    {
        AttnValue = 127;                                                        //(32/2) = 16dB. Set attenuator invalid So a value above this 16dB will be denied (error code = 127)
    }
    return(AttnValue);                                                          //Return checked Attenuator value to caller
}

int VerifyEquValue31_5dB(int EquValue)
{
    if(EquValue > 63)                                                           //(63/2) = 31.5dB, so when value is bigger than 31.5dB
    {
        EquValue = 127;                                                         //(63/2) = 31.5dB. Set equalizer invalid So a value above this 31.5dB will be denied (error code = 127)
    }
    return(EquValue);                                                           //Return checked Equalizer value to caller
}

int VerifyEquValue25dB(int EquValue)                                            //Function to verify if Equalizer value is valid
{
    if(EquValue > 50)                                                           //(50/2) = 25dB, so when value is bigger than 30dB
    {
        EquValue = 127;                                                         //(50/2) = 25dB. Set equalizer invalid So a value above this 25dB will be denied (error code = 127)
    }
    return(EquValue);                                                           //Return checked Equalizer value to caller
}

int VerifyEquValue16dB(int EquValue)                                            //Function to verify if Equalizer value is valid
{
    if(EquValue > 32)                                                           //(32/2) = 16dB, so when value is bigger than 16dB
    {
        EquValue = 127;                                                         //(32/2) = 16dB. Set equalizer invalid So a value above this 16dB will be denied (error code = 127)
    }
    return(EquValue);                                                           //Return checked Equalizer value to caller
}

int VerifyEquValue10dB(int EquValue)                                            //Function to verify if Equalizer value is valid
{
    if(EquValue > 20)                                                           //(20/2) = 10dB, so when value is bigger than 10dB
    {
        EquValue = 127;                                                         //(20/2) = 10dB. Set equalizer invalid So a value above this 10dB will be denied (error code = 127)
    }
    return(EquValue);                                                           //Return checked Equalizer value to caller
}

void DelayLoop(void)
{
    //Delay is 50 cycles per USB Device Task. XTAL = 20MHz 4 clocks per instruction, so 5MHz Instruction. 1/5000000 * 1000 = 0x01ms per line.. 0.01 x 1500 = 15ms delay
    int i;
    int Time_in_Ms;
    Time_in_Ms = (Setting_0_5dB_Steps_Time_Delay * 111);
    for (i = 0 ; i <Time_in_Ms ; i++)
    {
        USBDeviceTasks();
    }
}

void StepToValueIn0_5dBSteps(int NewSetting, int PreviousSetting, int LatchPinMicro) //Function to go to end value in steps of 0.5dB
{
    int i=0;
    int StepSetting;

    if (PreviousSetting < NewSetting)                                           //Need to step up in steps of 0.5dB
    {
        StepSetting = PreviousSetting;
        for (i=(PreviousSetting + 1); i<=NewSetting; i++)
        {
            StepSetting++;
            WriteSerialData(StepSetting, LatchPinMicro);
            DelayLoop();
        }
        
    }
    else                                                                        //Need to step down in steps of 0.5dB
    {
        StepSetting = PreviousSetting;
        for (i=(PreviousSetting - 1); i>=NewSetting; i--)
        {
            StepSetting--;
            WriteSerialData(StepSetting, LatchPinMicro);
            DelayLoop();
       
        }
    }
}