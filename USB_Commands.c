//*************************************************************************
//
//Description:	Source-file for PIC18F4550-firmware for USB commands set
//
//Filename	: USB_Commands.C
//Version	: 0.1
//Date		: 04-12-2018
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 11-12-2012 Modification: First Definition list finished. All USB Command functions are implemented. Ready for Beta Release.
//
//*************************************************************************
//#include "main.h"
//
//#include "Definitions.h"
//#include "/USB/Include/GenericTypeDefs.h"
//#include "/USB/Include/Compiler.h"
//#include "Microprocessor_Selector.h"
//#include "USB_Commands.h"
//#include "LoCon_Interface_PIC18.h"
//#include "LoCon_Commands.h"
//
//#include "/USB/Include/Usb/usb.h"
//#include "/USB/Include/Usb/usb_function_hid.h"
//#include "LoCon_USB-LoCon.h"
//#include "EEPROM_Function.h"
#include "library.h"

#define USB_DATA_BUFFER_SIZE    64

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
extern int LoConReceiveString[19];
#endif

#if defined (USB_FIRMWARE_UPGRADE) || defined(USB_LOCON_FUNCTIONS)
extern unsigned char ReceivedDataBuffer[64];                                    //Buffer needed for Firmware updates
extern unsigned char ToSendDataBuffer[64];

extern int ReplyDestination;
extern BOOL blinkStatusValid;
extern USB_HANDLE USBOutHandle;
extern USB_HANDLE USBInHandle;
extern BOOL ReplyUSB;

int USBMessageLength;
#endif

#if defined (USB_LOCON_FUNCTIONS) || defined(USE_LOCON_FUNCTIONS)
extern BOOL NoReplyRequired;
extern int LoConTransmitString[19];
extern int NrOfReceivedLoConBytes;
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
extern int LoConFraming;
extern int LoConAddress;
extern int LoConCommand;
extern int LoConData;
extern int OwnLoConAddress;
#endif

unsigned char ByteCounter;
unsigned char USBFrameSize;

#if defined(USB_FIRMWARE_UPGRADE)
void USBDelay(unsigned int j)
{
    while(j>0)
    {
      	j--;
        USBDeviceTasks();
    }
}
#endif

void ExecuteLoConDataToUSB(void)
{
    #if defined(USB_LOCON_FUNCTIONS)
    if (LoConRX == 1 && ReplyUSB == TRUE)                                                                                       //Replace && for || to listen also on LoCon bus for commands to USB! Now blocked to USB -> LoCON Only!!!
	{
        int j = 23700;                                                                                                          //Timeout 250ms with PIC18F14K50
        //mLED_4_On();
        while(j>0)
        {
            j--;
            USBDeviceTasks();
            //BlinkUSBStatus();
            if (LoConRX == 1)
            {
                NrOfReceivedLoConBytes=USBReceiveLoCon(LoConReceiveString);                                                     //Wait for data on LoCon-I/O check LoCon RX
                if(NrOfReceivedLoConBytes!=-1)                                                                                  //If any data received on LoCon-port
                {
                    if(NrOfReceivedLoConBytes!=-2)                                                                              //If parity ok
                    {
                        LoConFraming= (char) LoConReceiveString[0];
                        //LoConAddress= (char) LoConReceiveString[1];
                        //LoConCommand= (char) LoConReceiveString[2];
                        //if(USBValidLoConMasterFraming(LoConFraming) && USBValidLoConAddress(OwnLoConAddress, LoConAddress))   //Back to main loop if no valid framing code or address
                        //{
                        //    ReplyDestination=DESTINATION_LOCON;                                                               //LoCon-reply should be sent to LoCon-bus
                        //    ExecuteLoConCommand(LoConCommand);
                        //}
                        if(USBValidLoConSlaveFraming(LoConFraming) || USBValidLoConMasterFraming(LoConFraming))                 //If valid LoCon-framing code received
                        {
                            ByteCounter=0;
                            USBFrameSize=64;
                            while(ByteCounter!=USBFrameSize)
                            {
                                ToSendDataBuffer[ByteCounter+2] = LoConReceiveString[ByteCounter];                              //Fill USB-buffer to reply to USB host
                                USBDeviceTasks();                                                                               //Don't forget to keep USB alive during configuration
                                ByteCounter++;
                            }
                            ToSendDataBuffer[0] = 0x30;                                                                         //Echo back to the host PC the received LoCon bytes from slave device
                            ToSendDataBuffer[1] = NrOfReceivedLoConBytes;                                                       //1 byte back as reply
                            if(!HIDTxHandleBusy(USBInHandle))
                            {
                                ReplyUSB = FALSE;
                                SendToUSB();
                                //LoConTransmitString[0] = (char) ToSendDataBuffer[0];                                          //0x30 indicates send to USB
                                //LoConTransmitString[1] = (char) ToSendDataBuffer[1];                                          //Length byte of the LoCon Transmit string
                                //LoConTransmitString[2] = (char) ToSendDataBuffer[2];                                          //LoCon Framing
                                //LoConTransmitString[3] = (char) ToSendDataBuffer[3];                                          //LoCon Address
                                //LoConTransmitString[4] = (char) ToSendDataBuffer[4];                                          //LoCon Command
                                //SendLoCon(5, LoConTransmitString);                                                            //Send reply to LoCon-master
                            }
                        }
                    }
                    else
                    {
                        USBMessageLength = 1;
                        ToSendDataBuffer[0] = 0x30;                                                                             //Echo back to the host PC the command we are fulfilling in the first byte. A parity error has occurred
                        ToSendDataBuffer[1] = USBMessageLength;                                                                 //1 bytes back as reply
                        ToSendDataBuffer[2] = USB_LOCON_RECEIVE_PARITY_ERROR;
                        //BuildReply_USB();
                        if(!HIDTxHandleBusy(USBInHandle))
                        {
                            ReplyUSB = FALSE;
                            SendToUSB();
                        }
                    }  
                }
                break;
            }
        }
        //mLED_4_Off();
        if (NoReplyRequired == TRUE)
        {
            if(ReceivedDataBuffer[2] == 0xE0 )
            {
                USBMessageLength = 1;
                ToSendDataBuffer[0] = 0x30;                                                                                     //Echo back to the host PC the command we are fulfilling in the first byte. There is no reply on USB required
                ToSendDataBuffer[1] = USBMessageLength;                                                                         //1 bytes back as reply
                ToSendDataBuffer[2] = USB_LOCON_NO_REPLY_REQUIRED_FIRST;
                BuildReply_USB();
                if(!HIDTxHandleBusy(USBInHandle))
                {
                    ReplyUSB = FALSE;
                	NoReplyRequired = FALSE;
                    SendToUSB();
                }
            }
            else
            {
                if(ReceivedDataBuffer[2] == 0xE1 )
                {
                    USBMessageLength = 1;
                    ToSendDataBuffer[0] = 0x30;                                                                                 //Echo back to the host PC the command we are fulfilling in the first byte. There is no reply on USB required
                    ToSendDataBuffer[1] = USBMessageLength;                                                                     //1 bytes back as reply
                    ToSendDataBuffer[2] = USB_LOCON_NO_REPLY_REQUIRED_REPEAT;
            		BuildReply_USB();
         			if(!HIDTxHandleBusy(USBInHandle))
            		{
                        ReplyUSB = FALSE;
                        NoReplyRequired = FALSE;
                        SendToUSB();
               		}
                }
            }
        }
        if (ReplyUSB == TRUE)
        {
            if(ReceivedDataBuffer[2] == 0xE2 )
            {
                USBMessageLength = 1;
                ToSendDataBuffer[0] = 0x30;                                                                                     //Echo back to the host PC the command we are fulfilling in the first byte. A LoCon Timeout has occured
                ToSendDataBuffer[1] = USBMessageLength;                                                                         //1 bytes back as reply
                ToSendDataBuffer[2] = USB_LOCON_TIMEOUT_E2;
                BuildReply_USB();
                if(!HIDTxHandleBusy(USBInHandle))
                {
                    ReplyUSB = FALSE;
                    SendToUSB();
                }
            }
            else
            {
                if(ReceivedDataBuffer[2] == 0xE3 )
                {
                    USBMessageLength = 1;
                    ToSendDataBuffer[0] = 0x30;                                                                                 //Echo back to the host PC the command we are fulfilling in the first byte. A LoCon Timeout has occured
                    ToSendDataBuffer[1] = USBMessageLength;                                                                     //1 bytes back as reply
                    ToSendDataBuffer[2] = USB_LOCON_TIMEOUT_E3;
                    BuildReply_USB();
                    if(!HIDTxHandleBusy(USBInHandle))
                    {
                        ReplyUSB = FALSE;
                        SendToUSB();
                    }
                }
            }
        }
	}
    #endif
}


#if defined(USE_LOCON_FUNCTIONS) || defined(USB_LOCON_FUNCTIONS) || defined(USB_FIRMWARE_UPGRADE)
void Lose_Connection(void)                                                      //Close USB connection
{
    UCONbits.USBEN = 0;
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_LOCON_FUNCTIONS) || defined(USB_FIRMWARE_UPGRADE)
void Delay_Reset1(void)                                                         //Delay needed in Reset
{
    int j = 1000;
    int	k = 200;
    while(j>0 && k != 0)
    {
	j--;
	if(j == 1)
	{
        USBDeviceTasks();
        k--;
        j = 1000;
	}
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_LOCON_FUNCTIONS) || defined(USB_FIRMWARE_UPGRADE)
void Delay_Reset2(void)                                                         //Delay needed in Reset
{
    int j = 1000;
    int	k = 200;
    while(j>0 && k != 0)
    {
	j--;
	if(j == 1)
	{
            k--;
            j = 1000;
	}
    }
}
#endif

#if defined (USB_FIRMWARE_UPGRADE)
void ReplyUSBDelay(unsigned int j)                                              //Delay with the help of the USBDevice tasks to keep USB alive
{
    while(j>0)
    {
      	j--;
    	USBDeviceTasks();
    	//BlinkUSBStatus();
    }
}
#endif

void Reset_Micro(void)                                                          //Function to push the Microprocessor to a reset mode
{
    _asm reset _endasm
}

void ClRWdT_reset(void)                                                         //Function to clear the watch dog timer
{
    _asm clrwdt _endasm
}

#if defined (USB_FIRMWARE_UPGRADE)
void SendToUSB(void)                                                            //Function to send data back to the USB bus
{
    int clean = ToSendDataBuffer[1] + 2;                                        //2nd byte in string is length save this value
    while(clean < USB_DATA_BUFFER_SIZE)
    {
       	ToSendDataBuffer[clean] = 0xFF;                                         //Fill USB-buffer to reply to USB host
        USBDeviceTasks();                                                       //Don't forget to keep USB alive during configuration
        clean++;
    }
    USBInHandle = HIDTxPacket(HID_EP,(BYTE*)&ToSendDataBuffer[0], USB_DATA_BUFFER_SIZE);
}
#endif

#if defined (USB_LOCON_FUNCTIONS)
void BuildReply_USB(void)
{
    ByteCounter = 1;
    while(ByteCounter!=USBMessageLength)
    {
    	ToSendDataBuffer[ByteCounter+2] = LoConReceiveString[ByteCounter];      //Fill USB-buffer to reply to USB host
        USBDeviceTasks();                                                       //Don't forget to keep USB alive during configuration
    	ByteCounter++;
    }
}
#endif

#if !defined(USB_INTERNAL_FUNCTIONS)
void SendInterfaceInfoToUSB(void)
{
    ToSendDataBuffer[0] = 0x40;                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
    ToSendDataBuffer[1] = 0x07;                                                 //7 bytes back as reply
    ToSendDataBuffer[2] = 0xFD;                                                 //LoCon Command for Date request
    ToSendDataBuffer[3] = FIRMWAREVERSIONMSB;                                   //High byte of the Version
    ToSendDataBuffer[4] = FIRMWAREVERSIONLSB;                                   //Low byte of the version
    ToSendDataBuffer[5] = PRODUCTIONDATEyearMSB;                                //centaury if the ID
    ToSendDataBuffer[6] = PRODUCTIONDATEyearLSB;                                //Year of the ID
    ToSendDataBuffer[7] = PRODUCTIONDATEmonth;                                  //Month of the ID
    ToSendDataBuffer[8] = PRODUCTIONDATEday;                                    //Day of the ID
    if(!HIDTxHandleBusy(USBInHandle))
    {
	ReplyUSB = FALSE;
	SendToUSB();                                                                //Send answer to USB
    }
}
#endif

#if defined(USB_LOCON_FUNCTIONS)
void Send_Unknown_Framing_Byte(void)                                            //Function to give back to handler that the Framing code is incorrect given
{
    USBMessageLength = 1;                                                       //Message Length
    ToSendDataBuffer[0] = 0x30;                                                 //Echo back to the host PC the send handler to give result it's wrong.
    ToSendDataBuffer[1] = USBMessageLength;                                     //1 byte back as reply
    ToSendDataBuffer[2] = USB_FRAMING_ERROR;                                    //Incorrect framing given
    if(!HIDTxHandleBusy(USBInHandle))
    {
    	ReplyUSB = FALSE;
	SendToUSB();                                                                //Send answer to USB
    }
}
#endif

#if defined(USB_FIRMWARE_UPGRADE)
void Send_Unknown_Command_Byte(void)                                            //The given request in the correct LoCon form, is not known by the device as valid command
{
    USBMessageLength = 1;
    ToSendDataBuffer[0] = 0x40;                                                 //Echo back to the host PC the send handler to give result it's wrong.
    ToSendDataBuffer[1] = USBMessageLength;                                     //1 byte back as reply
    ToSendDataBuffer[2] = USB_COMMAND_ERROR;                                    //Unknown command given
    if(!HIDTxHandleBusy(USBInHandle))
    {
	ReplyUSB = FALSE;
	SendToUSB();                                                                //Send answer to USB
    }
}
#endif

void Send_Unknown_Handle_Byte(void)
{
    USBMessageLength = 1;
    ToSendDataBuffer[0] = ReceivedDataBuffer[0];                                //Echo back to the host PC the command we are fulfilling in the first byte.
    ToSendDataBuffer[1] = USBMessageLength;                                     //bytes back as reply
    ToSendDataBuffer[2] = USB_HANDLE_ERROR;                                     //Indicates the received handle for USB request in firmware is wrong (no 0x30/0xF0 or 0x37)
    if(!HIDTxHandleBusy(USBInHandle))
    {
	ReplyUSB = FALSE;
	SendToUSB();                                                                //Send answer to USB
    }
}

#if defined(USB_LOCON_FUNCTIONS)
void SendUSBMessageLengthError(void)
{
    USBMessageLength = 1;
    ToSendDataBuffer[0] = ReceivedDataBuffer[0];                                //Echo back to the host PC the command we are fulfilling in the first byte.
    ToSendDataBuffer[1] = USBMessageLength;                                     //Bytes back as reply
    ToSendDataBuffer[2] = USB_MESSAGE_LENGTH_ERROR;                             //Indicates the received USB message length for USB request in firmware is wrong (>20 bytes)
    if(!HIDTxHandleBusy(USBInHandle))
    {
    	ReplyUSB = FALSE;
    	SendToUSB();                                                            //Send answer to USB
        //USBInHandle = HIDTxPacket(HID_EP,(BYTE*)&ToSendDataBuffer[0],64);
    }
}
#endif

#if defined(USB_FIRMWARE_UPGRADE)
void Reset_Device_Reply_USB_Byte(void)
{
    USBMessageLength = 1;
    ToSendDataBuffer[0] = 0xF0;                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
    ToSendDataBuffer[1] = USBMessageLength;                                     //Bytes back as reply
    ToSendDataBuffer[2] = USB_RESET_FIRMWARE;                                   //Echo back that a request for Reset given.
    if(!HIDTxHandleBusy(USBInHandle))
    {
    	ReplyUSB = FALSE;
	SendToUSB();                                                                //Send answer to USB
    }
}
#endif

#if defined(USB_FIRMWARE_UPGRADE)
void Bootloader_Device_Reply_USB_Byte(void)
{
    USBMessageLength = 1;
    ToSendDataBuffer[0] = 0xE0;                                                 //Echo back to the host PC the command we are fulfilling in the first byte.
    ToSendDataBuffer[1] = USBMessageLength;                                     //Bytes back as reply
    ToSendDataBuffer[2] = USB_RESET_BOOTLOADER;                                 //Echo back that a request to enter the bootloader is given
    if(!HIDTxHandleBusy(USBInHandle))
    {
    	ReplyUSB = FALSE;
        SendToUSB();                                                            //Send answer to USB
    }
}
#endif

/********************************************************************
 * Function:        void ProcessIO(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        This function is a place holder for other user
 *                  routines. It is a mixture of both USB and
 *                  non-USB tasks.
 * Note:            None
 *******************************************************************/

char Data_tmp[4];
char itemp_tmp[2];
                
void ProcessIO(void)
{
    #if defined(USB_FIRMWARE_UPGRADE)
    //Blink the LEDs according to the USB device status
    if(blinkStatusValid)
    {
	     //BlinkUSBStatus();
    }
    if((USBDeviceState < CONFIGURED_STATE)||(USBSuspendControl==1)) return;
    if(!HIDRxHandleBusy(USBOutHandle))                                                          //Check if data was received from the host.
    {
        switch(ReceivedDataBuffer[0])                                                           //Look at the data the host sent, to see what kind of specific command it sent to the device
        {
            
            #ifdef chris_debug_on
            case 0x66:
                mLED_Alarm_Toggle() ;

                
                Data_tmp[2] = ReceivedDataBuffer[1];
                Data_tmp[3] = ReceivedDataBuffer[2];
                
//                MCP9808_getTempRaw_test(Data_tmp,itemp_tmp);
               
                ToSendDataBuffer[0] = itemp_tmp[0];
                ToSendDataBuffer[1] = itemp_tmp[1];
                
                ToSendDataBuffer[2] = Data_tmp[0];
                ToSendDataBuffer[3] = Data_tmp[1];
                ToSendDataBuffer[4] = Data_tmp[2];
                ToSendDataBuffer[5] = Data_tmp[3];
                ToSendDataBuffer[6] = 0xAA;
                
                
                
                
                //if(ValidLoConMasterFraming(LoConFraming) && ValidLoConAddress(OwnLoConAddress, LoConAddress))           //Back to main loop if no valid framing code or address
                {
                    ReplyDestination = DESTINATION_LOCON;
                    //ExecuteUSBCommand(LoConCommand);                                            //Execute LoCon Command to find function for the LoCon action
                    if(!HIDTxHandleBusy(USBInHandle))
                    {
                        ReplyUSB = FALSE;
                        SendToUSB();                                                            //Send answer to USB
                    }
                }
                
                break;
            #endif
            #if defined(USB_LOCON_FUNCTIONS)
            case 0x30:                                                                          //Toggle LoCon for Receive LoCon Command
            {
                blinkStatusValid = FALSE;                                                       //Stop blinking the LEDs automatically, going to manually control them now.
                ReplyUSB = FALSE;                                                               //NO reply USB necessary till USB request is received.
                USBMessageLength = ReceivedDataBuffer[1];                                       //2nd byte in string is length save this value
                if(USBMessageLength >= 20)
                {
                    SendUSBMessageLengthError();
                }
                else
                {
                    ByteCounter=0;
                    while(ByteCounter!=USBMessageLength)
                    {
                        LoConTransmitString[ByteCounter] = ReceivedDataBuffer[ByteCounter+2];	//Fill USB-buffer to reply to USB host
                        USBDeviceTasks();  														//Don't forget to keep USB alive during configuration
                        ByteCounter++;
                    }
                    LoConFraming = ReceivedDataBuffer[2];
                    LoConAddress = ReceivedDataBuffer[3];
                    LoConCommand = ReceivedDataBuffer[4];
                    if(ReceivedDataBuffer[2] == 0xE0 || ReceivedDataBuffer[2] == 0xE1)
                    {
                        NoReplyRequired = TRUE;
                    }
                    if(USBValidLoConMasterFraming(LoConFraming))                                //If 3d byte ReceivedDataBuffer[2] is LoCon-Framing code
                    {
                        ReplyUSB = TRUE;                                                        //Set ReplyUSB to Reply USB ALWAYS
                        USBSendLoCon(USBMessageLength, LoConTransmitString);                    //Send data to LoCon-bus
                    }
                    else
                    {
                        ReplyUSB = FALSE;
                        Send_Unknown_Framing_Byte();                                            //Data cannot be processed
                    }
                }
                blinkStatusValid = TRUE;                                                        //Start blinking the LEDs automatically, going to manually control them now.
            }
            break;
            #endif
            #if defined(USB_INTERNAL_FUNCTIONS)
            case 0x40:                                                                          //USB Commands for USB gateway module (internal commands)
            {
                blinkStatusValid = FALSE;                                                       //Stop blinking the LEDs automatically, going to manually control them now.
                ReplyUSB = FALSE;                                                               //NO reply USB necessary till USB request is received.
                USBMessageLength    = ReceivedDataBuffer[1];                                    //2nd byte in string is length save this value
                LoConFraming        = ReceivedDataBuffer[2];
                LoConAddress        = ReceivedDataBuffer[3];
                LoConCommand        = ReceivedDataBuffer[4];
                LoConData           = ReceivedDataBuffer[5];
                if(ValidLoConMasterFraming(LoConFraming) && ValidLoConAddress(OwnLoConAddress, LoConAddress))           //Back to main loop if no valid framing code or address
                {
                    ReplyDestination = DESTINATION_USB;
                    ExecuteUSBCommand(LoConCommand);                                            //Execute LoCon Command to find function for the LoCon action
                    if(!HIDTxHandleBusy(USBInHandle))
                    {
                        ReplyUSB = FALSE;
                        SendToUSB();                                                            //Send answer to USB
                    }
                }
                else
                {
                    ReplyUSB = FALSE;
                    Send_Unknown_Command_Byte();                                                //Data cannot be processed
                }
                #if !defined(USB_INTERNAL_FUNCTIONS)
                if((USBMessageLength==0x01) && (ReceivedDataBuffer[2]==0xFD))                   //If message length is 0, no LoCon received send back Interface Info
                {
                    SendInterfaceInfoToUSB();                                                   //Send message to master with firmware version and -date.
                }
                else
                {
                    ReplyUSB = FALSE;
                    Send_Unknown_Command_Byte();                                                //Data cannot be processed
                }
                #endif
                blinkStatusValid = TRUE;                                                        //Start blinking the LEDs automatically, going to manually control them now.
            }
            break;
            #endif
            #if defined(USB_FIRMWARE_UPGRADE)
            case 0xE0:                                                                          // Hardware reset
            {
                unsigned char i;
                USBMessageLength = ReceivedDataBuffer[1];                                       //2nd byte in string is length save this value
                if(USBMessageLength==0xFF)                                                      //If message length is 0, no LoCon received send back Interface Info
                {
                    #if defined(USE_INTERRUPT)
                    INTCON3bits.INT2IF = 0;                                                     //Set External INT2 Flag (before re enabling interrupts)
                    INTCONbits.GIE = 0;                                                         //Disable all interrupts
                    #endif
                    blinkStatusValid = FALSE;                                                   //Stop blinking the LEDs automatically, going to manually control them now.
                    ReplyUSB = FALSE;
                    Bootloader_Device_Reply_USB_Byte();                                         //Reply to USB that USB RESET is done!


                    WriteEEPROM(EE_ADR_BOOTLOADER_FLAG, 0xFF);                                  //Give information to bootloader to start bootloader session
                    WriteEEPROM(EE_ADR_VALID_FIRMWARE_PRESENT, 0x01);                           //Give information to say, Yes the previous firmware was working..
                    //Delay_Reset1();                                                           //Delay 100ms (PIC18F4550) (delay 66ms PIC18F14K50)
                    UCONbits.SUSPND = 0;                                                        //Disable USB module
                    UCON = 0x00;                                                                //Disable USB module
                                                                                                //And wait awhile for the USB cable capacitance to discharge down to disconnected (SE0) state.
                                                                                                //Otherwise host might not realize we disconnected/reconnected when we do the reset.
                                                                                                //A basic for() loop decrementing a 16 bit number would be simpler, but seems to take more code space for
                                                                                                //A given delay.  So do this instead:
                    for(i = 0; i < 0xFF; i++)
                    {
                        WREG = 0xFF;
                        while(WREG)
                        {
                            WREG--;
                            _asm
                            bra	0                                                               //Equivalent to bra $+2, which takes half as much code as 2 NOP instructions
                            bra	0                                                               //Equivalent to bra $+2, which takes half as much code as 2 NOP instructions
                            _endasm
                        }
                    }
                    //mLED_1_Off();
                    while(WDTCONbits.SWDTEN != 0)
                    {
                        WDTCONbits.SWDTEN = 0;                                                  //Disable Watchdog Timer
                    }
                    Reset_Micro();                                                              //Bye bye, time to go to sleep.. good night
                }
                else
                {
                    ReplyUSB = FALSE;
                    Send_Unknown_Command_Byte();
                }
            }
            break;

            #endif
            #if defined(USB_FIRMWARE_UPGRADE)
            case 0xF0:                                                                          // Hardware Soft reset
            {
                unsigned char i;
                #if defined(USE_INTERRUPT)
                    INTCON3bits.INT2F = 0;                                                      //Reset External INT2 Flag (before re enabling interrupts)
                    INTCONbits.GIE = 0;                                                         //Disable all interrupts
                #endif
                USBMessageLength = ReceivedDataBuffer[1];                                       //2nd byte in string is length save this value
                if(USBMessageLength==0xFF)                                                      //If message length is 0, no Locon received send back Interface Info
                {
                    blinkStatusValid = FALSE;                                                   // Stop blinking the LEDs automatically, going to manually control them now.
                    ReplyUSB = FALSE;
                    Reset_Device_Reply_USB_Byte();                                              // Reply to USB that USB RESET is done!
                    Delay_Reset1();                                                             //Delay 100ms (PIC18F4550) (delay 66ms PIC18F14K50)
                    Lose_Connection();                                                          //Kill USB connection to Host Peripheral
                    Delay_Reset2();                                                             //Delay 100ms PIC18F4450 and 66mS PIC18F14K50
                    //mLED_1_Off();
                    Reset_Micro();                                                              //Bye bye, time to go -->RESET<--
                }
                else
                {
                    ReplyUSB = FALSE;
                    Send_Unknown_Command_Byte();
                }
            }
            break;
            #endif
/*          case 0x37:                                                                          //Read POT command.  Uses ADC to measure an analog voltage on one of the ANxx I/O pins, and returns the result to the host
            {
                //WORD_VAL w;
                int ADC_Result;
                if(!HIDTxHandleBusy(USBInHandle))
                {
                    #if defined(USE_ADC_FUNCTIONS)
                        ADC_Result = ReadCurrentMonitorADC();                                   //Use ADC to read the I/O pin voltage.  See the relevant HardwareProfile - xxxxx.h file for the I/O pin that it will measure.
                        ToSendDataBuffer[0] = 0x37;                                             //Echo back to the host the command we are fulfilling in the first byte.  In this case, the Read POT (analog voltage) command.
                        ToSendDataBuffer[1] = ADC_Result;                                       //Measured analog voltage (WARNING, NEEDS TO BE SPLITTED UP IN 2 BYTES!
                        //ToSendDataBuffer[1] = w.v[0];                                         //Measured analog voltage LSB
                        //ToSendDataBuffer[2] = w.v[1];                                         //Measured analog voltage MSB
                    #endif
                    SendToUSB();
                }
            }
            break;*/
            default:
            {
            	Send_Unknown_Handle_Byte();
            }
        }
	USBOutHandle = HIDRxPacket(HID_EP,(BYTE*)&ReceivedDataBuffer,64);                           //Re-arm the OUT endpoint for the next packet
    }
    #endif
}


#if defined(USB_INTERNAL_FUNCTIONS)
void ExecuteUSBCommand(int LoConCommand)                                        //Function to execute received LoCon command
{
	switch(LoConCommand)                                                        //Switch function to find function at received LoCon command
	{
        case    USB_LOCON_CMD_READ_SETTING_EQ_HINGE_DS1 :                       //Check if received command is to read the state of EQ Hinge Downstream Module 1
                HandleCmdReadSetting_EQ_Hinge_DS_1();                           //Read actual state of EQ Hinge Downstream Module 1
        break;
        case    USB_LOCON_CMD_WRITE_SETTING_EQ_HINGE_DS1 :                      //Check if received command is to write the state of EQ Hinge Downstream Module 1
                HandleCmdWriteSetting_EQ_Hinge_DS_1(LoConData, DEFAULT);        //Write actual state of EQ Hinge Downstream Module 1
        break;
        case    USB_LOCON_CMD_READ_AUX_1_VALUE_BLL :                            //Read software only to provide a future function Aux 1
                HandleCmdReadAux1Value();                                       //Read actual state of the Aux 1
        break;
        case    USB_LOCON_CMD_WRITE_AUX_1_VALUE_BLL :                           //Write software only to provide a future function Aux 1
                HandleCmdWriteAux1Value(LoConData, DEFAULT);                    //Write state of the Aux 1
        break;
        case    USB_LOCON_CMD_READ_FACTORY_DEFAULT_SETTING :                    //Check if received command is to read the state of the type of Amplifier
                HandleCmdReadFactoryDefaultSetting();                           //Read actual state of the the Factory default settings
        break;
        case    USB_LOCON_CMD_WRITE_FACTORY_DEFAULT_SETTING :                   //Check if received command is to write the setting of all Attenuators, Equalizers and Switches in one command
                HandleCmdWriteFactoryDefaultSetting(ReceivedDataBuffer[5], ReceivedDataBuffer[6], ReceivedDataBuffer[7], ReceivedDataBuffer[8], ReceivedDataBuffer[9],
                                                    ReceivedDataBuffer[10], ReceivedDataBuffer[11], ReceivedDataBuffer[12], ReceivedDataBuffer[13], ReceivedDataBuffer[14],
                                                    ReceivedDataBuffer[15], USBMessageLength);           //Write new Factory default settings
        break;
        case    USB_LOCON_CMD_READ_SETTING_0_5_STEPS_ENABLED :                  //Check if received command is to read the state of the type of Amplifier
                HandleCmdReadSetting_0_5_Steps_Enabled();                       //Read actual state of the type of Amplifier
        break;
        case    USB_LOCON_CMD_WRITE_SETTING_0_5_STEPS_ENABLED :                 //Check if received command is to write the state of the type of Amplifier
                HandleCmdWriteSetting_0_5_Steps_Enabled(LoConData, DEFAULT);    //Write if the 0.5dB Steps is enabled or disabled
        break;
        case    USB_LOCON_CMD_READ_STEP_0_5_STEPS_TIME_DELAY_MS :               //Check if received command is to read the state of the type of Amplifier
                HandleCmdReadSetting_0_5_Steps_Time_Delay_Ms();                 //Read actual state of the type of Amplifier
        break;
        case    USB_LOCON_CMD_WRITE_STEP_0_5_STEPS_TIME_DELAY_MS :              //Check if received command is to write the state of the type of Amplifier
                HandleCmdWriteSetting_0_5_Steps_Time_Delay_Ms(LoConData, DEFAULT);//Write if the 0.5dB Steps is enabled or disabled
        break;
        case 	USB_LOCON_CMD_WRITE_ADDRESS_0 :                                 //Check if Address received address is 0xA0
                HandleCmdWriteAddress(0xA0);                                    //Write LoCOn Address 0
		break;
		case 	USB_LOCON_CMD_WRITE_ADDRESS_1 :                                 //Check if Address received address is 0xA1
	   			HandleCmdWriteAddress(0xA1);                                    //Write LoCOn Address 1
		break;
		case 	USB_LOCON_CMD_WRITE_ADDRESS_2 :                                 //Check if Address received address is 0xA2
				HandleCmdWriteAddress(0xA2);                                    //Write LoCOn Address 2
		break;
		case 	USB_LOCON_CMD_WRITE_ADDRESS_3 :                                 //Check if Address received address is 0xA3
				HandleCmdWriteAddress(0xA3);                                    //Write LoCon Address 3
		break;
		case 	USB_LOCON_CMD_WRITE_ADDRESS_4 :                                 //Check if Address received address is 0xA4
				HandleCmdWriteAddress(0xA4);                                    //Write LoCon Address 4
		break;
		case 	USB_LOCON_CMD_WRITE_ADDRESS_5 :                                 //Check if Address received address is 0xA5
				HandleCmdWriteAddress(0xA5);                                    //Write LoCon Address 5
		break;
		case 	USB_LOCON_CMD_WRITE_ADDRESS_6 :                                 //Check if Address received address is 0xA6
				HandleCmdWriteAddress(0xA6);                                    //Write LoCon Address 6
		break;
  		case 	USB_LOCON_CMD_WRITE_ADDRESS_7 :                                 //Check if Address received address is 0xA7
				HandleCmdWriteAddress(0xA7);                                    //Write LoCon Address 7
		break;
  		case 	USB_LOCON_CMD_WRITE_ADDRESS_8 :                                 //Check if Address received address is 0xA8
				HandleCmdWriteAddress(0xA8);                                    //Write LoCon Address 8
		break;
  		case 	USB_LOCON_CMD_READ_ADDRESS :                                    //Check if received command is Read LoCon Address
				HandleCmdReadAddress();                                         //Read LoCon-address from EEPROM
		break;
  		case 	USB_LOCON_CMD_REQUEST_ID :                                      //Check if received command is to read ID of Product
				HandleCmdRequestID();                                           //Send Device-ID to Master
		break;
  		case 	USB_LOCON_CMD_READ_ERRORS :                                     //Check if received command is to read errors in 1 byte
				HandleCmdReadErrors();                                          //Send Error-status to Master
		break;
  		case 	USB_LOCON_CMD_READ_STATUS :                                     //Check if received command is to read status of the product
				HandleCmdReadStatus();                                          //Send status code to master
		break;
  		case 	USB_LOCON_CMD_RESET_MODIFIED_BIT :                              //Check if received command is to reset the modified bit
  				HandleCmdResetModifiedBit();                                    //Clear modified bit
  		break;
  		case 	USB_LOCON_CMD_SET_MODIFIED_BIT :                                //Check if received command is to set the modified bit
  				HandleCmdSetModifiedBit();                                      //Set modified bit
  		break;
  		case 	USB_LOCON_CMD_READ_MODIFIED_BIT :                               //Check if received command is to read the modified bit
  				HandleCmdReadModifiedBit();                                     //Read modified bit
  		break;
  		case 	USB_LOCON_CMD_READ_FIRMWAREVERSION :                            //Check if received command is to read the actual firmware version number
  				HandleCmdReadFirmwareVersion();                                 //Read actual firmware version (2 bytes)
  		break;
        case    USB_LOCON_CMD_READ_UPSTREAM_ATTN :                              //Check if received command is to read the actual state of the Upstream Attenuator
                HandleCmdReadUpstreamAttn();                                    //Read actual state of the Upstream Attenuator (1 byte)
        break;
        case    USB_LOCON_CMD_READ_PRE_ATTN_MOD_1 :                             //Check if received command is to read the actual state of the Pre Attenuator Module 1
                HandleCmdReadPreAttnMod1();                                     //Read actual state of the Pre Attenuator Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_READ_INTERSTAGE_ATTN_MOD_1   :                    //Check if received command is to read the actual state of the Interstage Attenuator Module 1
                HandleCmdReadInterstageAttnMod1();                              //Read actual state of the Interstage Attenuator Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_READ_UPSTREAM_EQU :                               //Check if received command is to read the actual state of the Upstream Equalizer
                HandleCmdReadUpstreamEqu();                                     //Read actual state of the Upstream Equalizer (1 byte)
        break;
        case    USB_LOCON_CMD_READ_PRE_EQU_MOD_1 :                              //Check if received command is to read the actual state of the Pre Equalizer Module 1
                HandleCmdReadPreEquMod1();                                      //Read actual state of the Pre Equalizer Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_READ_INTERSTAGE_EQU_MOD_1 :                       //Check if received command is to read the actual state of the Interstage Equalizer Module 1
                HandleCmdReadInterstageEquMod1();                               //Read actual state of the Interstage Equalizer Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_READ_ATC_TEMPERATURE  :                           //Check if received command is to read the actual state of ATC Temperature
                HandleCmdReadATCTemperature();                                  //Read actual state of the ATC Temperature
        break;  
        case    USB_LOCON_CMD_READ_ATC_ENABLED_DISABLED :                       //Check if received command is to read the actual state of ATC Enabled or Disabled
                HandleCmdReadATCEnabledDisabled();                              //Read actual state of the ATC Enabled or Disabled (1 byte)
        break;        
        case    USB_LOCON_CMD_READ_DOWNSTREAM_POWER_SW_MOD_1 :                  //Check if received command is to read the actual state of the Downstream Power Switch Module 1
                HandleCmdReadDownstreamPowerSwMod1();                           //Read actual state of the Downstream Power Switch Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_READ_PRODUCT_MAINBOARD_IDENTIFIER :               //Check if received command is to read the Motherboard version (DBD or DBC or DBC-1200 LITE Amplifier)
                HandleCmdReadProductMainboardIdentifier();                      //Read actual state of the IDS Switch On Off Output 2 (-6dB/-40dB)(1 byte)
        break;
        case    USB_LOCON_CMD_READ_TYPE_ID_AMPLIFIER_BLL :                      //Check if received command is to read the Type of Amplifier ID for BLL
                HandleCmdReadTypeIDAmplifier();                                 //Read actual state of the Type of Amplifier ID for BLL
        break;
        case    USB_LOCON_CMD_WRITE_UPSTREAM_ATTN :                             //Check if received command is to write the actual state of the Broadcast Output attenuator
                HandleCmdWriteUpstreamAttn(LoConData, DEFAULT);                 //Write actual state of the BroadCast Output attenuator (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_PRE_ATTN_MOD_1 :                            //Check if received command is to write the actual state of the Pre Attenuator Module 1
                HandleCmdWritePreAttnMod1(LoConData, DEFAULT);                  //Write actual state of the Pre Attenuator Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_INTERSTAGE_ATTN_MOD_1 :                     //Check if received command is to write the actual state of the Interstage Attenuator Module 1
                HandleCmdWriteInterstageAttnMod1(LoConData, DEFAULT);           //Write actual state of the Interstage Attenuator Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_UPSTREAM_EQU :                              //Check if received command is to write the actual state of the Upstream Equalizer
                HandleCmdWriteUpstreamEqu(LoConData, DEFAULT);                  //Write actual state of the Upstream Equalizer (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_PRE_EQU_MOD_1 :                             //Check if received command is to write the actual state of the Pre Equalizer Module 1
                HandleCmdWritePreEquMod1(LoConData, DEFAULT);                   //Write actual state of the Pre Equalizer Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_INTERSTAGE_EQU_MOD_1 :                      //Check if received command is to write the actual state of the Interstage Equalizer Module 1
                HandleCmdWriteInterstageEquMod1(LoConData, DEFAULT);            //Write actual state of the Interstage Equalizer Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_ATC_ENABLED_DISABLED :                      //Check if received command is to write the actual state of ATC Enabled or Disabled
                HandleCmdWriteATCEnabledDisabled(LoConData, DEFAULT);           //Write actual state of the ATC Enabled or Disabled (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_DOWNSTREAM_POWER_SW_MOD_1 :                 //Check if received command is to write the actual state of the Power Switch Module 1
                HandleCmdWriteDownstreamPowerSwMod1(LoConData, DEFAULT);        //Write actual state of the Power Switch Module 1 (1 byte)
        break;
        case    USB_LOCON_CMD_WRITE_TYPE_ID_AMPLIFIER_BLL :                     //Check if received command is to write the actual state of the Type of Amplifier ID for BLL
                HandleCmdWriteTypeIDAmplifier(LoConData, DEFAULT);              //Write the Type of Amplifier ID for BLL
        break;
        case    USB_LOCON_CMD_READ_ALL_ATTN_AT_ONCE :                           //Check if received command is to read the setting of all Attenuators in 1 command
                HandleCmdReadAllAttnAtOnce();                                   //Read the setting of all Attenuators in 1 command (5 bytes)
        break;
        case    USB_LOCON_CMD_READ_ALL_EQU_AT_ONCE :                            //Check if received command is to read the setting of all Equalizers in 1 command
                HandleCmdReadAllEquAtOnce();                                    //Read actual state of the setting of all Equalizers in 1 command (5 bytes)
        break;
        case    USB_LOCON_CMD_READ_ALL_SWITCHES_AT_ONCE :                       //Check if received command is to read the the setting of all Switches in 1 command
                HandleCmdReadAllSwAtOnce();                                     //Read actual state of the the setting of all Switches in 1 command (8 bytes)
        break;
        case    USB_LOCON_CMD_READ_ALL_SETTINGS_AT_ONCE :                       //Check if received command is to read the actual state of all Settings in one command.
                HandleCmdReadAllSettingsAtOnce();                               //Read actual state of All settings in one command (5x Attenuator 5x Equalizer and 8x switches = 18 bytes)
        break;
        case    USB_LOCON_CMD_WRITE_ALL_ATTN_AT_ONCE :                          //Check if received command is to write the actual state of all Attenuators in one command.
                HandleCmdWriteAllAttnAtOnce(ReceivedDataBuffer[5], ReceivedDataBuffer[6], 
                                            ReceivedDataBuffer[7], USBMessageLength);
        break;                                                                  //Write actual state of All attenuators
        case    USB_LOCON_CMD_WRITE_ALL_EQU_AT_ONCE :                           //Check if received command is to write the actual state of all equalizers, attenuators and switches
                HandleCmdWriteAllEquAtOnce(ReceivedDataBuffer[5], ReceivedDataBuffer[6], 
                                           ReceivedDataBuffer[7], USBMessageLength);
        break;                                                                  //Write actual state of All Equalizers
        case    USB_LOCON_CMD_WRITE_ALL_SWITCHES_AT_ONCE :                      //Check if received command is to write the actual switch states
                HandleCmdWriteAllSwAtOnce(ReceivedDataBuffer[5], USBMessageLength);
        break;                                                                  //Write actual state of All Switches
         case    USB_LOCON_CMD_WRITE_ALL_SETTINGS_AT_ONCE :                     //Check if received command is to write the setting of all Attenuators, Equalizers and Switches in one command
                HandleCmdWriteAllSettingsAtOnce(ReceivedDataBuffer[5], ReceivedDataBuffer[6], ReceivedDataBuffer[7], ReceivedDataBuffer[8], ReceivedDataBuffer[9],
                                                ReceivedDataBuffer[10], ReceivedDataBuffer[11], USBMessageLength);           //Write actual state of All attenuators 
        break;                                                                  //Write actual state of all Attenuators, Equalizers and Switches
        case    USB_LOCON_CMD_SET_RESET   :                                     //Check if received command is to Reset the DBC-1200 LITE-Amplifier
                HandleCmdSetToReset();                                          //Just reset the DBC-1200 LITE-Amplifier
        break;
        case    USB_LOCON_CMD_SET_FACTORY_DEFAULT  :                            //Check if received command is to set the DBC-1200 LITE-Amplifier to Factory Default position
                HandleCmdSetToFactoryDefault();                                 //Set the DBC-1200 LITE-Amplifier back to Factory Default settings
        break;
 		default:                                                                //If command is not given in list above, give back message 0xE5
				HandleCmdUnknown();                                             //Recycle bin to all commands which are not defined LoCon commands for Product
  		break;
 	}
}
#endif


