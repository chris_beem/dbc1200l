//*************************************************************************
//
//Description:	Include-file for PIC18F14K50 and PIC18F4550 LoCon
//				Contains all declarations for LoCon_Interface_PIC18.c
//
//Filename	: LoCon_Interface_PIC18.h
//Version	: 0.1
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: Ing. G. Bronkhorst
//
// Modifications 11-12-2012: New list generated
//*************************************************************************

void LoConDelay(unsigned int j);
int CheckParity(int NrOfBytes, int *LoConByte);
void RemoveParity(int NrOfBytes, int *LoConByte);
int ValidLoConMasterFraming(int FramingCode);
int ValidLoConSlaveFraming(int FramingCode);
int ValidLoConAddress(int OwnAddress, int Address);
int ReceiveLoConData(int *ReceiveLoConByte, int time_out);
int ReceiveLoCon(int *LoConString);
void SendLoConBit(int i);
void SendLoCon(int NbBytes, int *SendLoConByte);

