//*************************************************************************
//
//Description:	Source-file for Timer Functions
//
//Filename	: Timers.c
//Version	: 0.1
//Date		: 15-10-2014
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
//*************************************************************************

//#include "Timers.h"
//#include "Definitions.h"
//#include "/USB/Include/Compiler.h"
//#include "Microprocessor_Selector.h"
//#include "LoCon_Commands.h"
//#include "main.h"
//#include "MCP9808.h"
//#include "ATC.h" 
#include "library.h"

//#define MINUTES(s) ((s*60))
//#define MIN_DEBUG(s) (2*s)

//extern int Upstream_Equ;                                                      //Global value in RAM that contains the actual setting of the Upstream Equalizer
extern int ATC_Enabled_Disabled;
int counterSeconds = 0;                                                         //Local counter used to count the 5 minutes refresh setting of all switches periodically when USB is not connected
int Min,Sec,Msec;                                                               //used for timing compare

void TimerInit(void)                                                            //Initialize Timer 0 in RC oscillator mode
{
    set_ATC_update_slow();
    RCONbits.IPEN = 1;                                                          //Enable Interrupt system priority feature
    T0CONbits.TMR0ON = 1;                                                       //This bit is set to high (1) to enable the Timer0 and set to low (0) to stop it.
    T0CONbits.T08BIT = 0;                                                       //Timer0 is configured as an 16-bit timer/counter
    T0CONbits.PSA = 1;                                                          //Use PS;   Timer0 pre-scaler is  assigned. Timer0 clock input does not bypass pre-scaler. 
    T0CONbits.T0CS = 0;                                                         //Internal instruction cycle clock (CLKO) 
    
    T0CONbits.T0PS = 0b000;                                                     //1:8 scalar;   //These are pre-scaler selection bits.
    
    INTCONbits.GIEH = 1;                                                        //Enable High priority Interrupts (USB)
    INTCONbits.GIEL = 1;                                                        //Enable Low priority Interrupts (TIMER0)
    INTCONbits.TMR0IE = 1;                                                      //Enable Timer0 Interrupt on Overflow
    INTCONbits.TMR0IF = 0;                                                      //Clear Interrupt Flag
    INTCONbits.INT0IE = 0;                                                      //Enable Timer0 Interrupt on Overflow
    preloadTimerValue();
    
}

/**/
//Timer 0 interrupt on overflow. called every 1 ms, 
/**/

int msec_counter = 0;
int sec_counter = 0;
int min_counter = 0;
#define TIME_COMPARE(M,s,m) ((min_counter>=M)&&(sec_counter>=s)&&(msec_counter>=m))
#define RESET_TIME() (min_counter=0);(sec_counter=0);(msec_counter=0);

void handleTMR0Interrupt(void)                                                  //Called in LOW priority ISR at Timer0 interrupt https://www.engineersgarage.com/embedded/pic-microcontroller-projects/timer-circuit
{
        //TEMP_CalculateTemperature();                                          // get the temperature and store in HW struct :TODO, add temperature to USB commands
        preloadTimerValue();
        INTCON3bits.INT2IF = 1;
        msec_counter++;
        
        if(msec_counter>999)
        {
            sec_counter++;
            msec_counter = 0;
        }
        
        if(sec_counter>59)
        {
            sec_counter=0;
            min_counter++;
        }
        
        if(min_counter>59)
        {
            min_counter=0;
        }
        
        if TIME_COMPARE(Min,Sec,Msec)
        {
            //mLED_Alarm_Toggle();
            mDebug_Toggle();
            RESET_TIME();
            Flag.ATC_TIMING = 1;
        }     
}

void set_ATC_update_fast(void)//when USB is connected, update every second or so, to reflect ATC change to user 
{
    Min=0;
    Sec=0;
    Msec=500;
}

void set_ATC_update_slow(void) //when no USB is connected, update every minute or so
{
    Min=1;
    Sec=0;
    Msec=0;
}

void preloadTimerValue(void) // Load timer value to ensure precisely timer interrupt
{
        TMR0H = TIMER0H_VAL;                                                  
        TMR0L = TIMER0L_VAL; 
}

