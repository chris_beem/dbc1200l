//============================================================================//
//Description   : MCP9808 Temperature sensor Module 
//Filename      : MCP9808.h
//Prefix        : MCP9808_
//Date          : 11-07-2018
//Copyright     : Technetix B.V.
//Author        : ing. Hani Shawa
//============================================================================//

//============================== Function prototypes =====================================//

#ifdef USE_READOUT_INT16_TEMPERATURE
float MCP9808_getTempRaw( char* iTemp );
#endif




float MCP9808_getTemp(void); 
int MCP9808_checkSensor(void);
void MCP9808_getManufacturerId(char* id);
void MCP9808_getDeviceId( char* id );
void MCP9808_toggleCleanI2C( void );
void TEMP_CalculateTemperature(void);
