//*************************************************************************
//
//Description:	Include-file for Timers.h
//
//Filename	: Timers.h
//Version	: 0.1
//Date		: 16-10-2014
//Copyright	: Technetix B.V.
//Author	: G. Bronkhorst
//
//*************************************************************************

extern int volatile unsigned TimeoutSeconds;

void TimerInit(void);
void handleTMR0Interrupt(void);
void preloadTimerValue(void);
void set_ATC_update_fast(void);
void set_ATC_update_slow(void);
