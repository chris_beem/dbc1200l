/********************************************************************
 FileName:      Microprocessor_Selector.h
********************************************************************
 File Description:

 Change History:
  Rev   Date         Description
  0.1   19/11/2008   Selector created for PIC18F4550
  0.2   04/12/2008   Selector added wit PIC18F14K50
********************************************************************/

#ifndef MICROPROCESSOR_SELECTOR_H
#define MICROPROCESSOR_SELECTOR_H


#if !defined(MICROPROCESSOR)
    #if defined(__18CXX)
        #if defined(__18F4550)
            #include "Pin description PIC18F4550.h"
        #elif defined(__18F45K50)
            #include "Pin description PIC18F45K50.h"
        #elif defined(__18F14K50)
            #include "Pin description PIC18F14K50.h"
        #endif
    #endif
#endif

#if !defined(MICROPROCESSOR)
    #error "Processor is incorrect definied or not found (or specified) in file Microprocessor_Selector.h"
#endif

#endif  //MICROPROCESSOR_SELECTOR_H
