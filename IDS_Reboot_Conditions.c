//*************************************************************************
//
//Description:	Source-file for Microchip-firmware for MaCom & Peregrine Attenuators
//		Contains all functionality to change a received value to a readable value
//
//Filename	: IDS_Reboot_Conditions.c
//Version	: 0.1
//Date		: 21-10-2013
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 21-10-2013 Modification: First setup for this new function. This function checks if Automatic IDS needs to be enabled or disabled.
// 24-10-2013 Modification: Boot sequence foe set switches is migrated with this function
//*************************************************************************
////#include "/USB/Include/Compiler.h"
////#include "/USB/Include/GenericTypeDefs.h"
//#include "Definitions.h"
//#include "main.h"
//
////#include "/USB/Include/GenericTypeDefs.h"
//#include "/USB/Include/Compiler.h"
//#include "Microprocessor_Selector.h"
////#include "USB_Commands.h"
////#include "LoCon_Interface_PIC18.h"
//#include "LoCon_Commands.h"
//#include "USB_Commands.h"
//
//#include "/USB/Include/Usb/usb.h"
////#include "/USB/Include/Usb/usb_function_hid.h"
////#include "LoCon_USB-LoCon.h"
////#include "EEPROM_Function.h"
//#include "Serial_Data_V01.h"
#include "library.h"

extern int MainboardVersion;
extern int Upstream_Att;
