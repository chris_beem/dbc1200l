#ifndef H_ATC_DEFINED 

//#if defined (H_ATC_DEFINED)
//    #warning "IS DEFINED NOW"
//#else
//    #warning "UNDEFINED AT THIS POINT"
//#endif

#define H_ATC_DEFINED 
//#warning "ATC.C warning"


//============================================================================//
//Description   : Header file for  ATC.c
//Filename      : ATC.h
//Version       : 0.1
//Date          : 04-03-2014
//Copyright     : Technetix B.V.
//Author        : ing. G. van Assenbergh
//============================================================================//


#define ABSOLUTE(N) (((N)<0.0)?((N)*-1):(N))


#define POL_DEG_0     -1.05
#define POL_DEG_1    0.0336
#define POL_DEG_2   0.00012


#define DS_MOD_ALWAYS_WRITE         0                                           //Force write also if noting is changed   
#define DS_MOD_NEVER_WRITE          1                                           //Never write also if someting is changed
#define DS_MOD_CHANGE_WRITE         2                                           //Write only if someting is changed

#define DS_OFF_ALWAYS_UPDATE        0                                           //Always update Offsets       
#define DS_OFF_TEMP_UPDATE          1                                           //Only update Offsets if temperature is changed

#define DS_ATC_STEPPING             0                                           //Use stepping in the ATC function      
#define DS_ATC_NO_STEPPING          1                                           //Do not use stepping in the ATC function

#define TC_MOD_DEPENDENT            0
#define TC_MOD_INDEPENDENT          1

//========================== Offset Definitions ==============================//
#define PRE_EQU                     0       //Array order
#define PRE_ATT                     1
#define INT_EQU                     2
#define INT_ATT                     3

//============================== ES Definitions ==============================//

#define PRE_FIRST                   4
#define INT_FIRST                   5
#define ATTENUATOR                  6
#define EQUALIZER                   7
//=========================== ATC states         ==============================//
#define ATC_OK                          0                                       //AxC succesfully done
#define ATC_CANNOT_REACH_VALUE          1                                       //The required correction cannot be done because the switch limits are reached
//=========================== Module structures ==============================//
typedef struct
{
    float LastTemp;                                                             //Last temperature on which the offsed is determined
    int Require;                                                             //ATC_RequiOffMx in 0.5dB dependent of temperature
    int Perform;                                                             //ATC_PerfoOffM1 before the ATC action or if not possible to perform
    int Dependency[4];                                                          //0 = independent. 1 = dependent
}st_TC;

typedef struct                                                                  //Structure for A3C
{
    //un_ATCState State;          //For every DSx module. Read and RAM only
    char State;          //For every DSx module. Read and RAM only
    int Mode;                   //OFF, ATC, AGC, ASLC. R/W EEPROM
}st_ATC;


typedef struct
{
    int Mod;                                                                    //Contains the Slot Number.  
    int TypeSw;                                                                 //Attenuation or Equalizing
    int Priority;                                                               //First Prestage of First Interstage
    int Required;                                                               //Contains the required action. +/- 1.
}st_ES;

typedef union                                                                   //Union for A2C_PilotSituations
{
    unsigned int All;                                                           //32bits 4*8bits
    struct
    {
        signed char Sw[4];                                                      //Offset in 0.5dB steps
    };
}un_ATCSwOffsets;

typedef struct
{
    un_ATCSwOffsets Mod[4];                                                     //4 DSx modules
}st_ATCAllSwOffsets;


typedef union                                                                   //Union for
{
    unsigned long long int All ;
    struct
    {
        unsigned char SwFlag[4];                                                //the Switch present flag. This bit is 1 if the module has this switch
        unsigned char SwNr[4];                                                  //the element Nr from the corresponding setting in the Info.Mod struct
    };
}un_ATCModSwFlags;

typedef struct
{
    un_ATCModSwFlags Mod[4];                                                    //4 DSx modules

}st_ATCAllSwFlags;


typedef union                                                                   //Union for
{
    unsigned long long int All ;
    struct
    {
        unsigned char SwFlag[4];                                                //the Switch present flag. This bit is 1 if the module has this switch
        unsigned char SwNr[4];                                                  //the element Nr from the corresponding setting in the Info.Mod struct
    };
}un_ATCModSwFlags;


extern un_ATCSwOffsets OF;                                                          //EEPROM. Contains te offset from all the switches
extern un_ATCModSwFlags SW;                                                            //RAM only. Contains the present switches for ASLC and setting index
extern st_ATC ATC;
extern st_TC TC;
extern st_ES ES;


//========================== Function Prototypes =============================//
/**
 * @Description     Clear all ATC parameters 
 */
void ATC_DeInit(void);


/**
 * @Description     Clear all ATC parameters and update ATC
 */
void ATC_Init(void);

/**
 * @Description    Delay used in stepping slowly and not immediately
 */
void StoreOldSetStepping(void);

/**
 * @Description     Clear the ATC Offsets for the selected module
 * @param Mod       Module number. Only DS1, 2, 3, 4. No checking
 */
void ATC_ClearOffsets();


/**
 * @Description     Calculate the required offset depending on the current 
 *                  settings from the switches.
 *                  Update only if offsets are changed
 * @param Mod       The Nr in the Info.Mod structure. DS1, Ds2, 
 * @param WriteMod  0 = Write always
 *                  1 = Write never
 *                  2 = Write only if settings are changed 
 * @param UseStepping 0 = Use stepping in the ATC function 
 *                  1 = Do not use stepping in the ATC function 
 */
void ATC_CheckOffset(int WriteMod, int UseStepping);


/**
 * @Description     Calculate the required offset depending of the mainboard
 *                  version and current temperature.
 * @param Mod       DS1, DS2, DS3, DS4
 * @return          Required offset in 0.5dB Steps. -1 means less attenuation
 */
int ATC_DetermineOffset();



/**
 * @Description     Check if DS module is dependent from a previous DS module. 
 * @param ModID     Module ID
 * @return          0 = Dependent, 1 is [Independent]
 */
//int ATC_CheckModInputDependency(unsigned char ModID);



/**
 * @Description     Load the dependency from a previous DS module in the 
 *                  TC.Dependency array. 0 = Dependent, 1 is [Independent]
 */
//void ATC_DetectModDependency(void);

/**
 * @Description     Update the offsets if the temperature is changed more than 2
 *                  degrees or the UpdateOff flag is set to always update. 
 *                  Call this function every x seconds.
 * @StepA           Update the current temperature
 * @StepB           Calculate the required offset
 * @StepC           Compare the required offset with the performed offset
 * @param WriteMod  0 = Write always
 *                  1 = Write never
 *                  2 = Write only if settings are changed 
 * @param UpdateOff 0 = Offsets are always updated  
 *                  1 = Offsets are only updated if temperature is changed.
 * @param UseStepping 0 = Use stepping in the ATC function 
 *                  1 = Do not use stepping in the ATC function 
 */
void ATC_UpdateAllOffsets(int WriteMod, int UpdateOff, int UseStepping);


void ES_EditSwitch(void);
#endif