//*************************************************************************
//
//Description:	Include-file for PIC18F4550-firmware in USB2.0-LoCon
//				Contains all declarations for LOCON_USB-LoCon.C
//
//Filename	: LOCON_USB-LoCon.h
//Version	: 0.1
//Date		: 25-09-2012
//Copyright	: Technetix B.V.
//Author	: Ing. G. Bronkhorst
//
//*************************************************************************

void USBLoConDelay(unsigned int j);
int USBCheckParity(int NrOfBytes, int *LoConByte);
void USBRemoveParity(int NrOfBytes, int *LoConByte);
int USBValidLoConMasterFraming(int FramingCode);
int USBValidLoConSlaveFraming(int FramingCode);
int USBValidLoConAddress(int OwnAddress, int Address);
int USBReceiveLoConData(int *ReceiveLoConByte, int time_out);
int USBReceiveLoCon(int *LoConString);

void USBSendLoConBit(int i);
void USBSendLoCon(int NbBytes, int *SendLoConByte);
