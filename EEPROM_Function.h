//*************************************************************************
//
//Description:	Implementation of EEPROM in the PIC4550 Microprocessor
//
//Filename	: EEPROM_FUNCTION.h
//Version	: 0.1
//Date		: 19-09-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//		  
//*************************************************************************

void WriteEEPROM(int Address, int Value);
int ReadEEPROM(int Address);
