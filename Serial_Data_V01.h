//*************************************************************************
//
//Description:	Header-file for Microchip-firmware for MaCom & Peregrine attenuators
//				Contains all functionality to communicate with step attenuator chip (PE4307)
//
//Filename	: Serial_Data_V01.h
//Version	: 0.1
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//		  
//*************************************************************************

void SerialDelay(unsigned int j);
void Initialize_Serial_Interface(void);
void GiveLatch(int Device);
void GiveClock(void);
void SendOne(void);
void SendZero(void);
void WriteSerialData(int Data, int Device);

