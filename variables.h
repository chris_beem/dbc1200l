typedef union
{
    unsigned char all[9];
    struct
    {
    unsigned char att_pre;
    unsigned char att_int;
    unsigned char att_us;
    unsigned char eq_pre;
    unsigned char eq_int;
    unsigned char eq_us;
    unsigned char pwr_sw;
    unsigned char att_pre_corrected; // Attenuator after ATC correction, used for stepping
    unsigned char att_int_corrected; // Attenuator after ATC correction, used for stepping
    };
}un_setting;

typedef struct                                                                  
{
    un_setting SET;                                                 
}st_ModuleInfo;

typedef union
{
    unsigned char All[6];
    struct
    {
        unsigned char MainboardID;
        unsigned char MBD_V1;
        unsigned char CtrModID;
        //un_ErrorByte Errors;
        unsigned char Voltage5V;
        unsigned char Voltage24V;
        float Temperature;
        unsigned char TempIntegers;
        unsigned char TempHundredths;
        unsigned char CM_Type;                                                 
    };
}un_HW;
  
typedef struct
{
    unsigned char ATC_TIMING;
    unsigned char USB_CON;
    unsigned char USB_TIMING;
}
st_Flags;

typedef struct
{
    unsigned char current;
    unsigned char last;
    unsigned int count;
}st_USB_PHY; //Physical connection, used to determin if device is plugged or unugged

extern un_HW HARDWARE;
extern st_ModuleInfo Info;
extern st_ModuleInfo Old_Info;
extern st_Flags Flag;
extern st_USB_PHY USB_PHY; 
