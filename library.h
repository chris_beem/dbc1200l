/* 
 * File:   library.h
 * Author: chris.beem
 *
 * Created on July 10, 2019, 2:50 PM
 */

#ifndef LIBRARY_H
#define	LIBRARY_H

#ifdef	__cplusplus
extern "C" {
#endif


#include "Definitions.h"
#include "main.h"
#include "Microprocessor_Selector.h"
#include "/USB/Include/GenericTypeDefs.h"
#include "/USB/Include/Compiler.h"
#include "/USB/Include/Usb/usb_device.h"
#include "/USB/Include/Usb/usb_function_hid.h"
#include "LoCon_Interface_PIC18.h"
#include "LoCon_Commands.h"
#include "USB_Commands.h"
#include "Serial_Data_V01.h"
#include "EEPROM_Function.h"
#include "Switch_Eq_Attn_Conversion.h"
#include "Timers.h"
#include "ATC.h" 

#include "EEPROM_Function.h"
#include "I2C.h"
#include "I2C_PIC18.h"
#include "IDS_Reboot_Conditions.h"
#include "LoCon_USB-LoCon.h"
#include "MCP9808.h"
#include "Microprocessor_Selector.h" 
//#include "Pin description PIC18F4550.h" 
//#include "Pin description PIC18F45K50.h" 
#include "Serial_Data_V01.h"
#include "Switch_Eq_Attn_Conversion.h"
#include "Timers.h"
#include "usb_config.h"
#include "variables.h"
    
#ifdef	__cplusplus
}
#endif

#endif	/* LIBRARY_H */

