/********************************************************************
 FileName:     	Pin description PIC18F4550.h
 Dependencies:  See INCLUDES section in c:\C18 directory
 Processor:     PIC18F4550 USB Microcontrollers
 Hardware:      Designed for LoCon-USB gateway
 Compiler:      Microchip C18
 Company:       Technetix B.V.

 *******************************************************************
 File Description:

 Change History:
  Rev   Date         Description
  0.1   19/11/2008  Initial profile used for testing functionality
  0.2   20/11/2008  Separate pins added for LoCon TX and LoCon RX
  0.3   02/12/2008  Final version of this processor defined
  0.4   04/12/2018  New firmware declared for PIC18F4550 Microprocessor DBC-1200 LITE-Amplifier
********************************************************************/
#include"definitions.h"

#ifndef PIN_DESCRIPTION_PIC18F4550_H
#define PIN_DESCRIPTION_PIC18F4550_H

    /*******************************************************************/
    /******** USB stack hardware selection options *********************/
    /*******************************************************************/
    //The LoCon-USB gateway supports the USE_SELF_POWER_SENSE_IO
    //and USE_USB_BUS_SENSE_IO features.  Uncomment the below line(s) if
    //it is desireable to use one or both of the features.
    //#define USE_SELF_POWER_SENSE_IO
    //#define tris_self_power     TRISAbits.TRISA2                              //Input
    #if defined(USE_SELF_POWER_SENSE_IO)
    #define self_power          PORTAbits.RA2
    #else
    #define self_power          1
    #endif

    //#define USE_USB_BUS_SENSE_IO
    //#define tris_usb_bus_sense  TRISAbits.TRISA1                              //Input
    #if defined(USE_USB_BUS_SENSE_IO)
    #define USB_BUS_SENSE       PORTAbits.RA1
    #else
    #define USB_BUS_SENSE       1
    #endif

    //Uncomment the following line to make the output HEX of this  
    //  project work with the MCHPUSB Bootloader    
    //#define PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER
	
    //Uncomment the following line to make the output HEX of this 
    //  project work with the HID Bootloader
    #define PROGRAMMABLE_WITH_USB_HID_BOOTLOADER                                //Disable for debugging

    /*******************************************************************/
    /******** Application specific definitions *************************/
    /*******************************************************************/

    /** Board definition ***********************************************/
    // 	These definitions will tell the main() function which board is
    //  currently selected.  This will allow the application to add
    //  the correct configuration bits as wells use the correct
    //  initialization functions for the LoCon_USB gateway. 
    #define MICROPROCESSOR PIC18F4550_MICRO
    #define PIC18F4550_MICRO
    #define CLOCK_FREQ 48000000

    /** LOCON **********************************************************/
    /** TX **********************************************************/
    #if defined(USE_LOCON_FUNCTIONS) || defined(USB_LOCON_FUNCTIONS)
    #define mInitLoConTX()	LATDbits.LATD4=0; TRISDbits.TRISD4=0;
    #define LoConTX             LATDbits.LATD4

    #define LoConTX_On()        LoConTX = 1;                                    //This is LoCon TX
    #define LoConTX_Off()       LoConTX = 0;                                    //This is LoCon TX

    /** LOCON **********************************************************/
    /** RX **********************************************************/
    #define mInitLoConRX()	TRISBbits.TRISB0=1;                                 //LOCON RX PIN
    #define LoConRX		PORTBbits.RB0                                           //LoCon RX PIN
    #endif
    /** USB Attach line Active when interrupt is not available. Default OFF *********/
    /** RX **************************************************************************/

    #if !defined(USE_INTERRUPT)
    #define mInitUSBAttach()	TRISBbits.TRISB2=1; //USB Attach as Input Pin
    #define USBAttach		PORTBbits.RB2
    #endif

    /** LED ************************************************************/
     #define mInitAllLEDs()      LATDbits.LATD1=0; TRISDbits.TRISD1=0;           //Rd1 all set to output, initial state is LEDs off
    //#define mInitSpareLEDs()    LATDbits.LATD0=0; TRISDbits.TRISD0=0; LATDbits.LATD2=0; TRISDbits.TRISD2=0; Rd0 and Rd2 all set to output, initial state is LEDs off
    //#define mInitAllLEDs()      LATD &= 0xF0; TRISD &= 0xF0;

    //#define mLED_1              LATDbits.LATD0
    #define mLED_Alarm          LATDbits.LATD1
    //#define mLED_2              LATDbits.LATD2
   
    //TODO DISABLE IN FINAL RELEASE
    //USED FOR LOCON COMMUNICATION TESTING CAN BE DISABLED IN FINAL RELEASE
    //#define mLED_1              LATDbits.LATD0
    //#define mLED_2              LATDbits.LATD1
    //#define mLED_3              LATDbits.LATD2
    //#define mLED_4              LATDbits.LATD3
    //END DISABLE

    //#define mGetLED_1()         mLED_1
    //#define mGetLED_2()         mLED_2

    //TODO DISABLE IN FINAL RELEASE
    //USED FOR LOCON COMMUNICATION TESTING CAN BE DISABLED IN FINAL RELEASE
    //#define mGetLED_3()         mLED_3  //This is LoCon TX
    //#define mGetLED_4()         mLED_4  //This is LoCon RX
    //END DISABLE

    //#define mLED_1_On()         mLED_1 = 1;
    //#define mLED_2_On()         mLED_2 = 1;
    //TODO ENABLE NEXT LINE TO ENABLE ALARM LEDS in FINAL RELEASE
    #define mLED_Alarm_On()   mLED_Alarm = 1;
    #define mLED_Alarm_Toggle()   mLED_Alarm = !mLED_Alarm; //ONLY FOR TESTING

    //TODO DISABLE IN FINAL RELEASE
    //USED FOR LOCON COMMUNICATION TESTING CAN BE DISABLED IN FINAL RELEASE
    //#define mLED_3_On()         mLED_3 = 1; //This is LoCon TX
    //#define mLED_4_On()         mLED_4 = 1; //This is LoCon RX
    //END DISABLE

    //#define mLED_1_Off()        mLED_1 = 0;
    //#define mLED_2_Off()        mLED_2 = 0;
    //TODO ENABLE NEXT LINE TO ENABLE ALARM LEDS IN FINAL RELEASE
    #define mLED_Alarm_Off()   mLED_Alarm = 0;

    //TODO DISABLE IN FINAL RELEASE
    //USED FOR LOCON COMMUNICATION TESTING CAN BE DISABLED IN FINAL RELEASE
    //#define mLED_3_Off()        mLED_3 = 0; //This is LoCon TX
    //#define mLED_4_Off()        mLED_4 = 0; //This is LoCon RX
    //END DISABLE

    //#define mLED_1_Toggle()     mLED_1 = !mLED_1;
    //#define mLED_2_Toggle()     mLED_2 = !mLED_2;

    //TODO DISABLE IN FINAL RELEASE
    //USED FOR LOCON COMMUNICATION TESTING CAN BE DISABLED IN FINAL RELEASE
    //#define mLED_3_Toggle()     mLED_3 = !mLED_3;
    //#define mLED_4_Toggle()     mLED_4 = !mLED_4;
    //END DISABLE

/*********** STEP ATTENUATOR (PE4307 definitions *********************/
/** ******************************************************************/
// Product specific names. Used for the function call to determine
// which attenuator must be set. You can also use the module names.

    #define SERIAL_DATA_TRIS	(TRISCbits.TRISC7)                              //Data pin port direction in initialise procedure needs to be set to output
    #define SERIAL_CLK_TRIS     (TRISDbits.TRISD6)                              //Clock pin port direction in initialise procedure needs to be set to output

    #define SERIAL_LE_1_TRIS	(TRISCbits.TRISC0)                              //Pre Attenuator Module 1 port direction in initialise procedure needs to be set to output
    #define SERIAL_LE_2_TRIS	(TRISBbits.TRISB5)                              //Interstage Attenuator Module 1 port direction in initialise procedure needs to be set to output
    #define SERIAL_LE_3_TRIS	(TRISEbits.TRISE0)                              //Pre Equalizer Module 1 port direction in initialise procedure needs to be set to output
    #define SERIAL_LE_4_TRIS	(TRISAbits.TRISA5)                              //Interstage Equalizer Module 1 port direction in initialise procedure needs to be set to output
    #define SERIAL_LE_5_TRIS	(TRISBbits.TRISB4)                              //Upstream Attenuator port direction in initialise procedure needs to be set to output
    #define SERIAL_LE_6_TRIS	(TRISBbits.TRISB3)                              //Upstream Equalizer port direction in initialise procedure needs to be set to output

    #define Serial_DATA_IO      (LATCbits.LATC7)                                //Data pin E2to communicate with Serial configurable IC
    #define Serial_CLK_IO       (LATDbits.LATD6)                                //Clock pin to communicate with Serial configurable IC

    #define Serial_LE_1_IO      (LATCbits.LATC0)                                //Pre Attenuator Module 1 Pin setting description (Latching DATA pin set)
    #define Serial_LE_2_IO      (LATBbits.LATB5)                                //Interstage Attenuator Module 1 Pin setting description (Latching DATA pin set)
    #define Serial_LE_3_IO      (LATEbits.LATE0)                                //Pre Equalizer Module 1 Pin setting description (Latching DATA pin set)
    #define Serial_LE_4_IO      (LATAbits.LATA5)                                //Interstage Equalizer Module 1 Pin setting description (Latching DATA pin set)
    #define Serial_LE_5_IO      (LATBbits.LATB4)                                //Upstream Attenuator Pin setting description (Latching DATA pin set)
    #define Serial_LE_6_IO      (LATBbits.LATB3)                                //Upstream Equalizer Pin setting description (Latching DATA pin set)

/*********** On Off switch for Switch Initialisation *********************/
/*************************************************************************/
// Product specific names. Used for the function call to determine
// which attenuator must be set. You can also use the module names.
    #define mInitDownstreamPowerSwitchMod1()      LATEbits.LATE2 = 0; TRISEbits.TRISE2=0;           //Downstream Power Mode Switch State Module 1 port direction in initialise procedure needs to be set to output
    #define Downstream_Power_Switch_State_Mod_1   LATEbits.LATE2

    #define Downstream_Power_Switch_Mod_1_On()    Downstream_Power_Switch_State_Mod_1 = 1;          //The switch is set to the active (On) position
    #define Downstream_Power_Switch_Mod_1_Off()   Downstream_Power_Switch_State_Mod_1 = 0;          //The switch is set to the passive (Off) position

/*********** Product Detection Initialisation *********************/
/*************************************************************************/
// Product specific names. Used for the function call to determine
// which attenuator must be set. You can also use the module names.
// Probably these names will be used for Serial Data in the future
// with a shift register for readout

#if 0
    #define mInitI2CSCLOutput()     LATDbits.LATD3 = 1;   TRISDbits.TRISD3 = 0; //I2C SCL bus set as output. And LAT to High
    #define mInitI2CSDAOutput()     LATDbits.LATD2 = 1;   TRISDbits.TRISD2 = 0; //I2C SDA bus set as output. And LAT to High
    #define mInitI2CSDAInput()      TRISDbits.TRISD2 = 1;                       //I2C SDA bus set as input. Do not change the LAT register!

    #define MID_SCL_OUTPUT_State    LATDbits.LATD3
    #define MID_SDA_OUTPUT_State    LATDbits.LATD2
    #define MID_SDA_INPUT_State     PORTDbits.RD2

    #define MID_SCL_High()       MID_SCL_OUTPUT_State = 1;                      //SCL is set to High
    #define MID_SCL_Low()        MID_SCL_OUTPUT_State = 0;                      //SCL is set to Low

    #define MID_SDA_High()       MID_SDA_OUTPUT_State = 1;                      //SDA is set to High
    #define MID_SDA_Low()        MID_SDA_OUTPUT_State = 0;                      //SDA is set to Low

#else
/*
    #define mInitI2CSCLOutput()     LATBbits.LATB1 = 1;   TRISBbits.TRISB1 = 0; //I2C SCL bus set as output. And LAT to High
    #define mInitI2CSDAOutput()     LATBbits.LATB0 = 1;   TRISBbits.TRISB0 = 0; //I2C SDA bus set as output. And LAT to High
    #define mInitI2CSDAInput()      TRISBbits.TRISB0 = 1;                       //I2C SDA bus set as input. Do not change the LAT register!

    #define MID_SCL_OUTPUT_State    LATBbits.LATB1
    #define MID_SDA_OUTPUT_State    LATBbits.LATB0
    #define MID_SDA_INPUT_State     PORTBbits.RB0
*/

    #define mInitI2CSCLOutput()     LATDbits.LATD3 = 1;   TRISDbits.TRISD3 = 0; //I2C SCL bus set as output. And LAT to High
    #define mInitI2CSDAOutput()     LATDbits.LATD2 = 1;   TRISDbits.TRISD2 = 0; //I2C SDA bus set as output. And LAT to High
    #define mInitI2CSDAInput()      TRISDbits.TRISD2 = 1;                       //I2C SDA bus set as input. Do not change the LAT register!

    #define MID_SCL_OUTPUT_State    LATDbits.LATD3
    #define MID_SDA_OUTPUT_State    LATDbits.LATD2
    #define MID_SDA_INPUT_State     PORTDbits.RD2


    #define MID_SCL_High()       MID_SCL_OUTPUT_State = 1;                      //SCL is set to High
    #define MID_SCL_Low()        MID_SCL_OUTPUT_State = 0;                      //SCL is set to Low

    #define MID_SDA_High()       MID_SDA_OUTPUT_State = 1;                      //SDA is set to High
    #define MID_SDA_Low()        MID_SDA_OUTPUT_State = 0;                      //SDA is set to Low
#endif

/*********** Unused I/O pins for future purpose *********************/
/*************************************************************************/
    //#define mInitMotherboardIdentifier()        TRISAbits.TRISA4=1; TRISDbits.TRISD0=1; //Motherboard Identifier Pins
    //#define MBD_IDN1                            PORTAbits.RA4                           //Motherboard Detection Pin LSB
    //#define MBD_IDN2                            PORTDbits.RD0                           //Motherboard Detection Pin MSB

/** SWITCH *********************************************************/
//TODO DISABLE IN RELEASE
//  #define mInitBootloader_sw() TRISBbits.TRISB4=1;
//	#define Bootloader_sw        PORTBbits.RB4
//TODO ENABLE IN RELEASE
//Enable bootloader switch for new PCB release!
    #define mInitBootloader_sw() TRISDbits.TRISD5=1;
    #define Bootloader_sw        PORTDbits.RD5

    //#define mInitSwitch3()     TRISBbits.TRISB5=1;
    //#define sw3                PORTBbits.RB5

    /** Current Monitor ADC Setting **************************************/
#if defined (USE_ADC_FUNCTIONS)
#define mInitCurrentMonitor()           {TRISAbits.TRISA0=1;ADCON0=0x01;ADCON1=0x0E;ADCON2=0x3C;ADCON2bits.ADFM = 1;}
#endif

    /** USB external transceiver interface (optional) ******************/
    //#define tris_usb_vpo        TRISBbits.TRISB3    // Output
    //#define tris_usb_vmo        TRISBbits.TRISB2    // Output
    //#define tris_usb_rcv        TRISAbits.TRISA4    // Input
    //#define tris_usb_vp         TRISCbits.TRISC5    // Input
    //#define tris_usb_vm         TRISCbits.TRISC4    // Input
    //#define tris_usb_oe         TRISCbits.TRISC1    // Output
    
    //#define tris_usb_suspnd     TRISAbits.TRISA3    // Output

#endif  //HARDWARE_PROFILE_PICDEM_FSUSB_H
