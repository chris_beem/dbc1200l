//*************************************************************************
//
//Description:	Implementation of EEPROM in the PIC4550 Microprocessor
//
//Filename	: EEPROM_FUNCTION.c
//Version	: 0.1
//Date		: 04-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 19-09-2012 Modification: First buildup of the EEPROM Read/Write functions
// 26-09-2012 Modification: First Definition list finished. All EEPROM functions are implemented. Ready for Beta Release.
// 20-12-2018 Modification: Final release for DBC-1200 LITE-Amplifier product. Minor changes, only non using lines removed. (nothing serious changed)
//*************************************************************************

#include "library.h"


void WriteEEPROM(int Address, int Value)                                        //Function to do a EEPROM Write action on the internal EEPROM memory
{
    




    EEADR = Address;                                                            //Address location to store the EEPROM byte (see definitions.h)
    EEDATA = Value;                                                             //Value to store in EEPROM memory
    Nop();                                                                      //Do nothing (little wait)
    Nop();                                                                      //Do nothing (little wait)
    EECON1bits.EEPGD = 0;                                                       //Set registers to store EEPROM
    EECON1bits.CFGS = 0;                                                        //Set registers to store EEPROM
    EECON1bits.WREN = 1;                                                        //Set registers to store EEPROM
    //INTCONbits.GIE = 0;                                                       //Disable interrupts to prevent an interrupt from USB during EPROM write process
    EECON2 = 0x55;                                                              //Set Configuration register to store byte to selected address (1 of 2)
    EECON2 = 0xAA;                                                              //Set Configuration register to store byte to selected address (2 of 2)
    EECON1bits.WR = 1;                                                          //Set write bit
    while(EECON1bits.WR);                                                       //Wait till write finished flag is set
    //INTCONbits.GIE = 1;                                                       //Enable Interrupts again
    EECON1bits.WREN = 0;                                                        //Close write session in resetting the Write flag (set to read)
}

int ReadEEPROM(int Address)                                                     //Function to do a EEPROM Read action on the internal EEPROM memory
{


    EEADR = Address;                                                            //Location to read, see definitions.h for all allocated read value addresses for EEPROM activity
    EECON1bits.EEPGD = 0;                                                       //Set registers to Read EEPROM
    EECON1bits.CFGS = 0;                                                        //Set registers to Read EEPROM
    EECON1bits.RD = 1;                                                          //Set registers to Read EEPROM
    return(EEDATA);                                                             //Give back to caller the read value
}