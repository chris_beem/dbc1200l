//*************************************************************************
//
//Description:	Source-file for Microchip-firmware for MaCom & Peregrine attenuators
//		Contains all functionality to communicate with step attenuator chip (PE4307)
//
//Filename	: Serial_Data_V01.c
//Version	: 0.1
//Date		: 25-01-2013
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 27-09-2012 Modification: First Definition list finished. All Serial Data functions are implemented. Ready for Beta Release.
// 01-10-2012 Modification: Change switch times
//
//*************************************************************************
//#include "Definitions.h"
//#include "/USB/Include/Compiler.h"
//#include "Microprocessor_Selector.h"
#include "library.h"

//#include "/USB/Include/GenericTypeDefs.h"
//#include "usb_config.h"
//#include "/USB/Include/Usb/usb_device.h"
//#include "/USB/Include/Usb/usb.h"
//#include "/USB/Include/Usb/usb_function_hid.h"


//******************************************************************************
//********************* Universal Serial Functions ***************************
#if defined(USE_SERIAL_DATA_FUNCTIONS)
void SerialDelay(unsigned int j)                                                //Function to implement a time delay to receive LoCon
{
    while(j>0)
    {
        j--;
    }
}
#endif

#if defined(USE_SERIAL_DATA_FUNCTIONS)
void Initialize_Serial_Interface(void)                                          //This function initializes the serial data pins
{
    Serial_LE_1_IO      = 0;                                                        //Set pin to '0'
    Serial_LE_2_IO      = 0;                                                        //Set pin to '0'
    Serial_LE_3_IO      = 0;                                                        //Set pin to '0'
    Serial_LE_4_IO      = 0;                                                        //Set pin to '0'
    Serial_LE_5_IO      = 0;                                                        //Set pin to '0'
    Serial_LE_6_IO      = 0;                                                        //Set pin to '0'
    Serial_DATA_IO      = 0;                                                        //Set pin to '0'
    Serial_CLK_IO       = 0;                                                        //Set pin to '0'
    
    SERIAL_DATA_TRIS    = 0;                                                        //Set pin as digital output
    SERIAL_CLK_TRIS     = 0;                                                        //Set pin as digital output

    SERIAL_LE_1_TRIS    = 0;                                                        //Set pin as digital output
    SERIAL_LE_2_TRIS    = 0;                                                        //Set pin as digital output
    SERIAL_LE_3_TRIS    = 0;                                                        //Set pin as digital output
    SERIAL_LE_4_TRIS    = 0;                                                        //Set pin as digital output
    SERIAL_LE_5_TRIS    = 0;                                                        //Set pin as digital output
    SERIAL_LE_6_TRIS    = 0;                                                        //Set pin as digital output
}
#endif

#if defined(USE_SERIAL_DATA_FUNCTIONS)
void GiveLatch(int Device)                                                      //This function gives a pulse on the specified LE
{
    switch (Device)
    {
        case 1:
            Serial_LE_1_IO = 1;                                                 //Set LE
            SerialDelay(100);
            Serial_LE_1_IO = 0;                                                 //Clear LE
        break;
        case 2:
            Serial_LE_2_IO = 1;                                                 //Set LE
            SerialDelay(100);
            Serial_LE_2_IO = 0;                                                 //Clear LE
        break;
        case 3:
            Serial_LE_3_IO = 1;                                                 //Set LE
            SerialDelay(100);
            Serial_LE_3_IO = 0;                                                 //Clear LE
        break;
        case 4:
            Serial_LE_4_IO = 1;                                                 //Set LE
            SerialDelay(100);                
            Serial_LE_4_IO = 0;                                                 //Clear LE
        break;
        case 5:
            Serial_LE_5_IO = 1;                                                 //Set LE
            SerialDelay(100);
            Serial_LE_5_IO = 0;                                                 //Clear LE
        break;
        case 6:
            Serial_LE_6_IO = 1;                                                 //Set LE
            SerialDelay(100);
            Serial_LE_6_IO = 0;                                                 //Clear LE
        break;
        default:
                                                                                //No action
        break;
    }
}
#endif

#if defined(USE_SERIAL_DATA_FUNCTIONS)
void GiveClock(void)                                                            //This function gives a pulse on the specified CLK
{
    Serial_CLK_IO = 1;                                                          //Set CLK
    SerialDelay(98);
    Serial_CLK_IO = 0;                                                          //Clear CLK
}
#endif

#if defined(USE_SERIAL_DATA_FUNCTIONS)
void SendOne(void)                                                              //This function sends a "one"
{
    Serial_DATA_IO = 1;                                                         //Set data pin to high
    SerialDelay(98);
    GiveClock();                                                                //Give Clock pulse
    SerialDelay(98);
    Serial_DATA_IO = 0;                                                         //Set data pin low
}
#endif

#if defined(USE_SERIAL_DATA_FUNCTIONS)
void SendZero(void)                                                             //This function sends a "two"
{
    Serial_DATA_IO = 0;                                                         //Set data pin low
    SerialDelay(98);
    GiveClock();                                                                //Give Clock pulse
    SerialDelay(98);
    Serial_DATA_IO = 0;                                                         //Set data pin low
}
#endif

#if defined(USE_SERIAL_DATA_FUNCTIONS)
void WriteSerialData(int Data, int Device)                                      //This function writes 6 bits data to the selected device.
{
    int i;                                                                      //Local variable

    for(i=0; i<6; i++)                                                          //Sent 6 bits, MSB transmitted first
    {
        if(((Data<<i) & 0x20) == 0x20)                                          //MSB (6th bit)
        {
            SendOne();                                                          //Send a '1'
        }
        else
        {
            SendZero();                                                         //Send a '0'
        }
    }
    GiveLatch(Device);                                                          //Give Latch pulse for latching new data to the device
}
#endif
