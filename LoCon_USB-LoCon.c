//*************************************************************************
//
//Description:	Source-file for PIC18F4550-firmware in USB-LoCon interface
//				Contains all LoCon-communication routines
//
//Filename	: LOCON_USB-LoCon.c
//Version	: 0.1
//Date		: 27-09-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
// 27-09-2012 Modification: First Definition list finished. All USB-LoCon functions are implemented. Ready for Beta Release.
//
//*************************************************************************
//#include "/USB/Include/GenericTypeDefs.h"
////#include "/USB/Include/Compiler.h"
//
//#include "Microprocessor_Selector.h"
//#include "Definitions.h"
//
//#include "/USB/Include/Usb/usb_device.h"
#include "library.h"

#if defined (USB_LOCON_FUNCTIONS)
//DECLARATIONS
extern int LoConTransmitString[19];
extern int LoConReceiveString[19];
extern unsigned char USBFrameSize;
extern int NrOfReceivedLoConBytes;
extern int NrOfLoConBytesToTransmit;
extern int LoConFraming;
extern int LoConAddress;
extern int LoConCommand;
extern int ReplyDestination;
extern int USBMessageLength;
extern unsigned char ByteCounter;
#endif

#if defined (USB_LOCON_FUNCTIONS)
void USBLoConDelay(unsigned int j)
{
    while(j>0)
    {
      	j--;
    }
} 
#endif

//----------------------- This routine checks parity of array of bytes ------	
//---------------------------- Return values: 0=OK ; -1=NOT OK --------------
#if defined (USB_LOCON_FUNCTIONS)
int USBCheckParity(int NrOfBytes, int *LoConByte)
{ 	
    int i, j, NrOfOnes, ParityBit, ParityError;
    
    ParityError=0;
    for(i=0; i<NrOfBytes; i++)                                                  //For each byte in array
    {
    	ParityBit=LoConByte[i] & 0x0001;                                        //LSB of byte is parity bit
    	NrOfOnes=0;
    	for(j=1; j<9; j++)                                                      //9 bits per byte: 8data, 1 parity
    	{
            if((LoConByte[i]>>j) & 0x0001)                                      //Check LSB (skip parity bit)
            {
                NrOfOnes++;                                                     //LSB=1
            }
    	}
        if(NrOfOnes % 2)                                                        //If odd number of ones
    	{
            if(ParityBit!=0)
            {
            	ParityError=-1;                                                 //Parity error
            }
    	}                                
        else                                                                    //If even number of ones
        {
            if(ParityBit!=1)
            {
            	ParityError=-1;                                                 //Parity error
            }
    	}
    }
    return(ParityError);
}        
#endif

//---------------- //This routine removes the parity bits of an array of bytes ----
#if defined (USB_LOCON_FUNCTIONS)
void USBRemoveParity(int NrOfBytes, int *LoConByte)                             //Function to remove the parity bit from the LoCon Byte
{	
    int i;
    
    for(i=0; i<NrOfBytes; i++)
    {
    	LoConByte[i]>>=1;                                                       //Shift right one bit
    }
}
#endif

#if defined (USB_LOCON_FUNCTIONS)
int USBValidLoConMasterFraming(int FramingCode)                                 //Function to check if the LoCon framing is correct
{
    if((FramingCode & 0xFC) == 0xE0)
    {
    	return(1);                                                              //FramingCode OK
    }
    else
    {
        return(0);                                                              //Invalid FramingCode
    }
}
#endif

#if defined (USB_LOCON_FUNCTIONS)
int USBValidLoConSlaveFraming(int FramingCode)                                  //Function to check if the LoCon slave framing is correct
{
    if((FramingCode & 0xFC) == 0xE4)
    {
    	return(1);                                                              //FramingCode OK
    }
    else
    {
    	return(0);                                                              //Invalid FramingCode
    }
}
#endif

#if defined (USB_LOCON_FUNCTIONS)
int USBValidLoConAddress(int OwnAddress, int Address)                           //Function to check if the received LoCon Address is correctly
{
    if((Address == OwnAddress) || (Address == 0xAF))
    {
    	return(1);                                                              //Address OK
    }
    else
    {
    	return(0);                                                              //Invalid address
    }
}
#endif

//LoCon routines for LoConI/O port:
#if defined (USB_LOCON_FUNCTIONS)
int USBReceiveLoConData(int *ReceiveLoConByte, int time_out)                    //Function used to receive the actual LoCon data on the LoCon USB bus
{
    static int j,k,l,bits,bytes;
    static unsigned i=0;
    
    for(bytes=0;bytes<=19;bytes++)                                              //Maximum of 19 bytes per transaction
    {
        mLED_3_Off(); //This is the TX PIN
    	for(bits=0;bits<9;bits++)                                               //9 bits per byte (8 + Parity)
     	{
            j=0;
            k=0;
            l=0;
            while (LoConRX == 0)                                                //While no data (I/O-pin is low)
            {
                l++;
                if (l == 2000) 
                {
                    return (-1);                                                //Return amount of bytes received
                }
                USBDeviceTasks();                                               //Keep USB alive from suspend mode. Must be repeated every 100us top. Takes 50 cycles, about 7,5us every time.
                if(bytes!=0)
                {
                    i++;
                    USBDeviceTasks();                                           //Keep USB alive from suspend mode. Must be repeated every 100us top. Takes 50 cycles, about 7,5us every time.
                    if (i == 650)                                               //Time-out counter = 650 -> Maximum time between LoCon bytes is 5,5msec (spec is 5msec).
                    {
                        if(time_out==0)                                         //If max. number of timeouts reached this is 0
                        {
                            return(bytes);                                      //Return amount of bytes received
                        }
                        time_out--;                                             //Decrease max. number of timeouts
                        i=0;                                                    //Reset timeout counter
                    }
                }
            }
            while(1)                                                            //While always
            {
            	j++;
            	k=0;
                while ((LoConRX) != 0)                                          //While I/O-pin is high
                {
                    k++;
                    USBDeviceTasks();                                           //Takes 10us every time this is procedure is called
                    if (k == 250)                                               //This indicates it's about 2ms time delay
                    {
                        return (-1);                                            //Return error: bit too long
                    }
                } 
                k=0;
            	while ((LoConRX == 0) && (k<7))                                 //While I/O-pin is low and counter<7          while ((LoConRX == 0) && (k<9))
                {
                    k++;
                    USBDeviceTasks();
                }
                if (k == 7) 
                {
                    mLED_3_On();                                                //This is the TX PIN
                    //mLED_4_On()                                               //Switch on If Pin 4 needed
                    break;                                                      //Break while-loop when I/O-pin low long enough
                }
            }
            ReceiveLoConByte[bytes] = (ReceiveLoConByte[bytes] << 1);           //Shift Receive byte left one bit
            if ((j == 10) || (j == 11) || (j == 12))                            //Number of bits for '1' is 11
            {
                //mLED_4_On();                                                  //ACK Sign on LED for receiving a correct 1 bit
                ReceiveLoConByte[bytes] = (ReceiveLoConByte[bytes] | 0x01);
                //mLED_4_Off();                                                 //ACK Sign on LED for receiving a correct 1 bit
            }
            if ((j == 21) || (j == 22) || (j == 23))                            //Number of bits for '0' is 22
            {
                //mLED_4_On();                                                  //ACK Sign on LED for receiving a correct 0 bit
                ReceiveLoConByte[bytes] = (ReceiveLoConByte[bytes] & 0xFFFE);
                //mLED_4_Off();                                                 //ACK Sign on LED for receiving a correct 0 bit
            }
            if ((j != 10) && (j != 11) && (j != 12) && (j != 21) && (j != 22) && (j != 23))
            {
                return(-1);                                                     //Return error: wrong number of bits
            }
            if (bits <8)                                                        //If not all 9 bits received
            {
                i=0;
                while ((LoConRX == 0) && (i<40000))                             //Wait for I/O-pin to get high again
                {
                    i++;
                    USBDeviceTasks();
                }
                if (i == 40000) 
                {
                    return(-1) ;                                                //Return error: Incomplete byte
                }
            }
     	}
    }
    return(-1);
}
#endif

#if defined (USB_LOCON_FUNCTIONS)
int USBReceiveLoCon(int *LoConString)                                           //Function to receive LoCon data on the LoCon Bus
{
    int NrReceivedBytes;
    char ReturnValue;
    
    NrReceivedBytes = USBReceiveLoConData(LoConString, 0);                      //Wait for data on LoCon-I/O
    if(NrReceivedBytes>=1)                                                      //If data received correct and at least one byte
    {
    	if(USBCheckParity(NrReceivedBytes, LoConString)!=-1)
    	{
            USBRemoveParity(NrReceivedBytes, LoConString);                      //Remove parity bits from data
            ReturnValue=NrReceivedBytes;
    	}
    	else
    	{
            ReturnValue=-2;                                                     //Parity error
    	}
    }
    else
    {
        ReturnValue=-1;                                                         //Nothing received
    }
    return(ReturnValue);
}
#endif

#if defined (USB_LOCON_FUNCTIONS)
void USBSendLoConBit(int i)                                                     //Function to send a LoCon bit on the LoCon bus
{
    int j, dummy=0;
    
    for (j=0;j<33;j++)
    {
        if(j<i)
        {
            LoConTX_On();
            mLED_3_On();
            //for(dummy=0; dummy<11; dummy++);
            for(dummy=0; dummy<12; dummy++);
            //dummy++;
        }
        else
        {
            LoConTX_Off();
            mLED_3_Off();
            //for(dummy=0; dummy<11; dummy++);
            for(dummy=0; dummy<10; dummy++);
            dummy++;
        }
        LoConTX_Off();
        mLED_3_Off();
        for(dummy=0; dummy<10; dummy++);
    }
}
#endif

#if defined (USB_LOCON_FUNCTIONS)
void USBSendLoCon(int NbBytes, int *SendLoConByte)                              //Function used to send LoCon data to the LoCon Bus
{
    int i,j,NbOnes;
    for(i=0;i<NbBytes;i++)
    {
    	USBLoConDelay(620); //Time between LoCon Bytes is 2ms
    	NbOnes=0;
        for(j=0;j<8;j++)
        {
            if((SendLoConByte[i] >> (7-j)) % 2)
            {
            	USBSendLoConBit(11);                                            //Send '1' if LSB=1
            	NbOnes = NbOnes + 1;                                            //Number of '1's transmitted+=1
            }
            else
            {
            	USBSendLoConBit(22);                                            //Send '0' is LSB=0
            }
        } 
    	if(NbOnes % 2)
        {
            USBSendLoConBit(22);                                                //Parity=0 if odd number of 1's transmitted
        }
        else
        {
            USBSendLoConBit(11);                                                //Parity=1 if even number of 1's transmitted
        }                 
    }
}                                               
#endif
