/*********************************************************************
 *
 *   Microchip USB HID Bootloader for PIC18 (Non-J Family) USB Microcontrollers
 *
 *********************************************************************
 * FileName:        main.c
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18
 * Compiler:        C18 3.20+
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its PICmicro� Microcontroller is intended and
 * supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * File Version  Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 1.0			 06/19/2008	Original Version.  Adapted from 
 *					MCHPFSUSB v2.1 HID Bootloader
 *					for PIC18F87J50 Family devices.
 ********************************************************************/

/*********************************************************************
IMPORTANT NOTES: This code is currently configured to work with the
PIC18F4550 using the PICDEM FS USB Demo Board.  However, this code
can be readily adapted for use with the following devices:

PIC18F4553/4458/2553/2458
PIC18F4550/4455/2550/2455
PIC18F4450/2450
PIC18F14K50

To do so, replace the linker script with an appropriate version, and
click "Configure --> Select Device" and select the proper
microcontroller.  Also double check to verify that the io_cfg.h and
usbcfg.h are properly configured to match your desired application
platform.

Verify that the coniguration bits are set correctly for the intended
target application, and fix any build errors that result from either
the #error directives, or due to I/O pin count mismatch issues (such
as when using a 28-pin device, but without making sufficient changes
to the io_cfg.h file)

PIC18F14K50 and PIC18F13K50 devices are not yet supported in this
release.  However, some code sections mention these devices in
anticipation for future support.


This project needs to be built with the C18 compiler optimizations
enabled, or the total code size will be too large to fit within the
program memory range 0x000-0xFFF.  The default linker script included
in the project has this range reserved for the use by the bootloader,
but marks the rest of program memory as "PROTECTED".  If you try to
build this project with the compiler optimizations turned off, or
you try to modify some of this code, but add too much code to fit
within the 0x000-0xFFF region, a linker error like that below may occur:

Error - section '.code_main.o' can not fit the section. Section '.code_main.o' length=0x00000082
To fix this error, either optimize the program to fit within 0x000-0xFFF
(such as by turning on compiler optimizations), or modify the linker
and vector remapping (as well as the application projects) to allow this
bootloader to use more program memory.
*********************************************************************/


// IMPORTANT
//
// This firmware is written to work on the PIC18F4550 and the PIC18F14K50. 
// To let the firmware work on the microprocessor, change the following:

// Configurate to work on the PIC18F14K50:
//
// 1. Select in MPLab the correct microprocessor at Configure --> Select Device
// 		Select the PIC18F14K50 from the list
// 2. Select the correct linker script for the PIC18F14K50 on the work directory
//		If there are other Linker scripts in Linker script tree, delete them from the list
//		Select the Filename: BootModified.18f14k50.lkr
// 3. Find in the Project tree the header file usbcfg.h
//		Select here the correct Microprocessor and uncomment the following:
//		#define PIC18F14K50
// 4. Comment the other Microprocessor in the header file usbcfg.h
//	    //#define PIC18F4550
// 5. Configuration done, and microprocessor changed.
//
// Configurate to work on the PIC18F4550:
//
// 1. Select in MPLab the correct microprocessor at Configure --> Select Device
// 		Select the PIC18F4550 from the list
// 2. Select the correct linker script for the PIC18F4550 on the work directory
//		If there are other Linker scripts in Linker script tree, delete them from the list
//		Select the Filename: BootModified.18f4550.lkr
// 3. Find in the Project tree the header file usbcfg.h
//		Select here the correct Microprocessor and uncomment the following:
//		#define PIC18F4550
// 4. Comment the other Microprocessor in the header file usbcfg.h
//	    //#define PIC18F14K50
// 5. Configuration done, and Microprocessor changed.
//
// ***** End of configuration for correct Microprocessor

/** I N C L U D E S **********************************************************/
#include <p18cxxx.h>
#include "typedefs.h"                   
#include "usb.h"                         
#include "io_cfg.h"                     
#include "BootPIC18NonJ.h"

//#define USE_INTERNAL_16MHZ_OSCILLATOR

extern int Setbit;
extern int ValidFirmware;
int counter = 0;

/** C O N F I G U R A T I O N ************************************************/
// Note: For a complete list of the available config pragmas and their values, 
// see the compiler documentation, and/or click "Help --> Topics..." and then 
// select "PIC18 Config Settings" in the Language Tools section.

#if defined(PIC18F4550)		// Configuration bits for PIC18F4550
        #pragma config PLLDIV   = 5         // (20 MHz crystal on PIC18F4550 board)
        #pragma config CPUDIV   = OSC1_PLL2	
        #pragma config USBDIV   = 2         // Clock source from 96MHz PLL/2
        #pragma config FOSC     = HSPLL_HS
        #pragma config FCMEN    = OFF
        #pragma config IESO     = OFF
        #pragma config PWRT     = OFF
        #pragma config BOR      = OFF       //Changed: Brownout disabled to prevent a brownout when the power is switched off. Because there is an external voltage supervisor this feature is not needed anymore
        #pragma config BORV     = 0         //Previous this was 3 (2.0.. 2.16V) now changed to (4.36V.. 4.82V) but function disabled!!
        #pragma config VREGEN   = ON        //USB Voltage Regulator (On @ 5V supply and Off @ 3.3V supply)
        #pragma config WDT      = OFF
        #pragma config WDTPS    = 32768
        #pragma config MCLRE    = ON
        #pragma config LPT1OSC  = OFF
        #pragma config PBADEN   = OFF       //NOTE: modifying this value here won't have an effect
                                            //on the application.  See the top of the main() function.
                                            //By default the RB4 I/O pin is used to detect if the
                                            //firmware should enter the bootloader or the main application
                                            //firmware after a reset.  In order to do this, it needs to
                                            //configure RB4 as a digital input, thereby changing it from
                                            //the reset value according to this configuration bit.
//      #pragma config CCP2MX   = ON
        #pragma config STVREN   = ON
        #pragma config LVP      = OFF
//      #pragma config ICPRT    = OFF       // Dedicated In-Circuit Debug/Programming
        #pragma config XINST    = OFF       // Extended Instruction Set
        #pragma config CP0      = ON
        #pragma config CP1      = ON
        #pragma config CP2      = ON
        #pragma config CP3      = ON
        #pragma config CPB      = ON
    //    #pragma config CPD      = OFF
        #pragma config WRT0     = OFF
        #pragma config WRT1     = OFF
//      #pragma config WRT2     = OFF
//      #pragma config WRT3     = OFF
        #pragma config WRTB     = OFF       // Boot Block Write Protection
        #pragma config WRTC     = OFF
//      #pragma config WRTD     = OFF
        #pragma config EBTR0    = OFF
        #pragma config EBTR1    = OFF
//      #pragma config EBTR2    = OFF
//      #pragma config EBTR3    = OFF
        #pragma config EBTRB    = OFF

//If using the YOUR_BOARD hardware platform (see usbcfg.h), uncomment below and add pragmas
//#elif defined(YOUR_BOARD)
		//Add the configuration pragmas here for your hardware platform
		//#pragma config ... 		= ...
#elif defined(PIC18F14K50)
        //PIC18F14K50
        #pragma config CPUDIV = NOCLKDIV, USBDIV = OFF                              //CONFIG1L
        #pragma config FOSC = HS, PLLEN=ON, FCMEN = OFF, IESO = OFF                 //CONFIG1H
        #pragma config PWRTEN = OFF, BOREN = ON, BORV = 30 //, VREGEN = ON          //CONFIG2L
        #pragma config WDTEN = OFF, WDTPS = 32768                                   //CONFIG2H
        #pragma config MCLRE = OFF, HFOFST = OFF				    //CONFIG3H
        #pragma config STVREN = ON, LVP = OFF, XINST = OFF, BBSIZ=OFF               //CONFIG4L
        #pragma config CP0 = OFF, CP1 = OFF                                         //CONFIG5L
        #pragma config CPB = OFF                                                    //CONFIG5H
        #pragma config WRT0 = OFF, WRT1 = OFF					    //CONFIG6L
        
		#pragma config WRTB = ON, WRTC = OFF                                //CONFIG6H
        //Disabled WRTB for debugging.  Reenable for real.
        //#pragma config WRTB = OFF, WRTC = OFF                                     //CONFIG6H
        
		#pragma config EBTR0 = OFF, EBTR1 = OFF                             //CONFIG7L
        #pragma config EBTRB = OFF                                                  //CONFIG7H
                
        #ifdef __DEBUG
        //#pragma config BKBUG = ON 
        #endif
        #ifndef __DEBUG
        //#pragma config BKBUG = OFF
        #endif   
#elif defined(PIC18F45K50)                                                      //Configuration bits for PIC18F45K50
    //CONFIG1L 
    #pragma config PLLSEL   = PLL3X                                             //PLL Selection (4x clock multiplier) 
    #pragma config CFGPLLEN = ON                                                //PLL Enable Configuration bit (PLL Disabled (firmware controlled)) 
    #pragma config CPUDIV   = NOCLKDIV                                          //CPU System Clock Postscaler (CPU uses system clock (no divide)) 
    #pragma config LS48MHZ  = SYS48X8                                           //Low Speed USB mode with 48 MHz system clock (System clock at 24 MHz, USB clock divider is set to 4) 
    //CONFIG1H 
    #pragma config FOSC     = HSH                                               //Oscillator Selection (USE_INTERNAL_16MHZ_OSCILLATOR for Internal oscillator FOSC = INTOSCIO)
    #pragma config PCLKEN   = OFF                                               //Primary Oscillator Shutdown (Primary oscillator shutdown firmware controlled) 
    #pragma config FCMEN    = OFF                                               //Fail-Safe Clock Monitor (Fail-Safe Clock Monitor disabled) 
    #pragma config IESO     = OFF                                               //Internal/External Oscillator Switchover (Oscillator Switchover mode disabled) 
    //CONFIG2L 
    #pragma config nPWRTEN  = OFF                                               //Power-up Timer Enable (Power up timer disabled) 
    #pragma config BOREN    = OFF                                               //Brown-out Reset Enable (BOR disabled in hardware (SBOREN is ignored)) 
    #pragma config BORV     = 190                                               //Brown-out Reset Voltage (BOR set to 1.9V nominal) 
    #pragma config nLPBOR   = OFF                                               //Low-Power Brown-out Reset (Low-Power Brown-out Reset disabled) 
    // CONFIG2H 
    #pragma config WDTEN    = ON                                                //Watchdog Timer Enable bits (WDT disabled in hardware (SWDTEN ignored)) 
    #pragma config WDTPS    = 32768                                             //Watchdog Timer Postscaler (1:32768) 
    // CONFIG3H 
    #pragma config CCP2MX   = RC1                                               //CCP2 MUX bit (CCP2 input/output is multiplexed with RC1) 
    #pragma config PBADEN   = OFF                                               //PORTB A/D Enable bit (PORTB<5:0> pins are configured as analog input channels on Reset) 
    #pragma config T3CMX    = RC0                                               //Timer3 Clock Input MUX bit (T3CKI function is on RC0) 
    #pragma config SDOMX    = RB3                                               //SDO Output MUX bit (SDO function is on RB3) 
    #pragma config MCLRE    = ON                                                //Master Clear Reset Pin Enable (MCLR pin enabled; RE3 input disabled) 

    // CONFIG4L 
    #pragma config STVREN   = ON                                                //Stack Full/Underflow Reset (Stack full/underflow will cause Reset) 
    #pragma config LVP      = OFF                                               //Single-Supply ICSP Enable bit (Single-Supply ICSP enabled if MCLRE is also 1) 
    #pragma config ICPRT    = OFF                                               //Dedicated In-Circuit Debug/Programming Port Enable (ICPORT disabled) 
    #pragma config XINST    = OFF                                               //Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled) 
    // CONFIG5L 
    #pragma config CP0      = ON                                                //Block 0 Code Protect (Block 0 is not code-protected) 
    #pragma config CP1      = ON                                                //Block 1 Code Protect (Block 1 is not code-protected) 
    #pragma config CP2      = ON                                                //Block 2 Code Protect (Block 2 is not code-protected) 
    #pragma config CP3      = ON                                                //Block 3 Code Protect (Block 3 is not code-protected) 
    // CONFIG5H 
    #pragma config CPB      = ON                                                //Boot Block Code Protect (Boot block is not code-protected) 
    #pragma config CPD      = OFF                                               //Data EEPROM Code Protect (Data EEPROM is not code-protected) 
    // CONFIG6L 
    #pragma config WRT0     = OFF                                               //Block 0 Write Protect (Block 0 (0800-1FFFh) is not write-protected) 
    #pragma config WRT1     = OFF                                               //Block 1 Write Protect (Block 1 (2000-3FFFh) is not write-protected) 
    #pragma config WRT2     = OFF                                               //Block 2 Write Protect (Block 2 (04000-5FFFh) is not write-protected) 
    #pragma config WRT3     = OFF                                               //Block 3 Write Protect (Block 3 (06000-7FFFh) is not write-protected) 
    // CONFIG6H 
    #pragma config WRTC     = OFF                                               //Configuration Registers Write Protect (Configuration registers (300000-3000FFh) are not write-protected) 
    #pragma config WRTB     = OFF                                               //Boot Block Write Protect (Boot block (0000-7FFh) is not write-protected) 
    #pragma config WRTD     = OFF                                               //Data EEPROM Write Protect (Data EEPROM is not write-protected) 
    // CONFIG7L 
    #pragma config EBTR0    = OFF                                               //Block 0 Table Read Protect (Block 0 is not protected from table reads executed in other blocks) 
    #pragma config EBTR1    = OFF                                               //Block 1 Table Read Protect (Block 1 is not protected from table reads executed in other blocks) 
    #pragma config EBTR2    = OFF                                               //Block 2 Table Read Protect (Block 2 is not protected from table reads executed in other blocks) 
    #pragma config EBTR3    = OFF                                               //Block 3 Table Read Protect (Block 3 is not protected from table reads executed in other blocks) 
    // CONFIG7H 
    #pragma config EBTRB    = OFF                                               //Boot Block Table Read Protect (Boot block is not protected from table reads executed in other blocks) 
#else
	#error Not a supported board (yet), make sure the proper board is selected in usbcfg.h, and if so, set configuration bits in __FILE__, line __LINE__
#endif

/** V A R I A B L E S ********************************************************/
#pragma udata

/** P R I V A T E  P R O T O T Y P E S ***************************************/
static void InitializeSystem(void);
void USBTasks(void);
//#if !defined(__18F14K50) && !defined(__18F13K50) && !defined(__18LF14K50) && !defined(__18LF13K50)
#if !defined(__18F13K50) && !defined(__18LF13K50)
	void BlinkUSBStatus(void);
#else
    #define BlinkUSBStatus()
#endif

/** V E C T O R  R E M A P P I N G *******************************************/
#pragma code high_vector=0x08
void interrupt_at_high_vector(void)
{
    _asm goto 0x1408 _endasm
}
#pragma code low_vector=0x18
void interrupt_at_low_vector(void)
{
    _asm goto 0x1418 _endasm
}
#pragma code


/** D E C L A R A T I O N S **************************************************/
#pragma code

/******************************************************************************
 * Function:        void BootloaderReturnDelay(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        Main program entry point.
 * Note:            None
 *****************************************************************************/
void BootloaderUSBReceiveLoopDelay(unsigned int j)
{
  while(j>0)
  {
  	j--;
        USBTasks();                                         //Need to call USBTasks() periodically to keep USB alive
	BlinkUSBStatus();
	if((usb_device_state == CONFIGURED_STATE) && (UCONbits.SUSPND != 1))
	{
            ProcessIO();                                    //This is where all the actual bootloader related data transfer/self programming takes place
        }                                                   //See ProcessIO() function in the Boot87J50Family.c file.
  }
}

/******************************************************************************
 * Function:        void main(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        Main program entry point.
 * Note:            None
 *****************************************************************************/
void main(void)
{
    #if defined(USE_INTERNAL_16MHZ_OSCILLATOR)
        #if defined(__18F45K50)
            OSCTUNE = 0x80;                                                     //3X PLL ratio mode selected
            OSCCON = 0x04;//0x70;                                               //Switch to 16MHz HFINTOSC
            OSCCON2 = 0x14;//0x10;                                              //Enable PLL, SOSC, PRI OSC drivers turned off
            while(OSCCON2bits.PLLRDY != 1);                                     //Wait for PLL lock
            ACTCON = 0x00;//0x90;                                               //Enable active clock tuning for USB operation
        #endif    
    #endif  
    signed char eepTemp;                                      //Setup a var to read the EEPROM
    //EEADR = 0x0001;                                         //Location to read, I'm useing eeprom address 1
    //EECON1bits.EEPGD = 0;                                   //Set EEPROM to read address
    //EECON1bits.CFGS = 0;                                    //Set EEPROM to read address
    //EECON1bits.RD = 1;                                      //Set EEPROM to read address
    mInitSwitch2();
    mDetectUSB();
    eepTemp = ReadEEPROM(0x0001);                             //Check if NOT enter Bootloader flag is set. If it's not set, then start application
    //eepTemp = EEDATA;                                       //Copy EEPROM data to Local variable
	#if defined(__18F45K50)	
        ANSELDbits.ANSD1 = 0;								  //Disable analog port for digital pins
        ANSELDbits.ANSD2 = 0;								  //Disable analog port for digital pins
		ANSELDbits.ANSD3 = 0; 								  //Disable analog port for digital pins
        ANSELDbits.ANSD4 = 0;								  //Disable analog port for digital pins
        ANSELDbits.ANSD5 = 0;								  //Disable analog port for digital pins
        ANSELDbits.ANSD6 = 0;								  //Disable analog port	for digital pins
        ANSELDbits.ANSD7 = 0;								  //Disable analog port for digital pins
	#endif	
    if ((eepTemp == 0x01) && (sw2 == 1))
    {
        _asm
        goto 0x1400                                           //If the user is not trying to enter the bootloader, go straight to the main application remapped "reset" vector.
        _endasm
    }
    eepTemp = ReadEEPROM(0x0002);
    if (eepTemp == 0x01)
    {
        ValidFirmware = 1;
    }
    else
    {
        ValidFirmware = 0;
    }

    InitializeSystem();
    if ((mDetectUSB_IO == 0) && (ValidFirmware == 1))       // Is USB bus deattached and is there any valid firmware.?
    {
        ClearEEPROMBootloaderFlag();
    }
    while(1)
    {
        ClrWdt();
        //mLED_3_On();
        BootloaderUSBReceiveLoopDelay(50000);
        //mLED_3_Off();
        BootloaderUSBReceiveLoopDelay(50000);
        counter++;
        if (counter == 185)
        {
            ClearEEPROMBootloaderFlag();
            counter = 0;
        }
        if (Setbit == 1)
        {
            EEADR =0x0002;
            EEDATA =0x01;
            Nop();
            Nop();
            EECON1bits.EEPGD = 0;
            EECON1bits.CFGS = 0;
            EECON1bits.WREN = 1;
            INTCONbits.GIE = 0;
            EECON2 = 0x55;
            EECON2 = 0xAA;
            EECON1bits.WR = 1;
            while(EECON1bits.WR);
            INTCONbits.GIE = 1;
            EECON1bits.WREN = 0;
            Setbit = 0;
        }
   }//end while
}//end main

/******************************************************************************
 * Function:        static void InitializeSystem(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        InitializeSystem is a centralize initialization routine.
 *                  All required USB initialization routines are called from
 *                  here.
 *
 *                  User application initialization routine should also be
 *                  called from here.                  
 *
 * Note:            None
 *****************************************************************************/
static void InitializeSystem(void)
{
    //On the PIC18F87J50 Family of USB microcontrollers, the PLL will not power up and be enabled
    //by default, even if a PLL enabled oscillator configuration is selected (such as HS+PLL).
    //This allows the device to power up at a lower initial operating frequency, which can be
    //advantageous when powered from a source which is not gauranteed to be adequate for 48MHz
    //operation.  On these devices, user firmware needs to manually set the OSCTUNE<PLLEN> bit to
    //power up the PLL.
    //The USB specifications require that USB peripheral devices must never source
    //current onto the Vbus pin.  Additionally, USB peripherals should not source
    //current on D+ or D- when the host/hub is not actively powering the Vbus line.
    //When designing a self powered (as opposed to bus powered) USB peripheral
    //device, the firmware should make sure not to turn on the USB module and D+
    //or D- pull up resistor unless Vbus is actively powered.  Therefore, the
    //firmware needs some means to detect when Vbus is being powered by the host.
    //A 5V tolerant I/O pin can be connected to Vbus (through a resistor), and
    //can be used to detect when Vbus is high (host actively powering), or low
    //(host is shut down or otherwise not supplying power).  The USB firmware
    //can then periodically poll this I/O pin to know when it is okay to turn on
    //the USB module/D+/D- pull up resistor.  When designing a purely bus powered
    //peripheral device, it is not possible to source current on D+ or D- when the
    //host is not actively providing power on Vbus. Therefore, implementing this
    //bus sense feature is optional.  This firmware can be made to use this bus
    //sense feature by making sure "USE_USB_BUS_SENSE_IO" has been defined in the
    //usbcfg.h file.
    #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = INPUT_PIN; // See io_cfg.h
    #endif
    //If the host PC sends a GetStatus (device) request, the firmware must respond
    //and let the host know if the USB peripheral device is currently bus powered
    //or self powered.  See chapter 9 in the official USB specifications for details
    //regarding this request.  If the peripheral device is capable of being both
    //self and bus powered, it should not return a hard coded value for this request.
    //Instead, firmware should check if it is currently self or bus powered, and
    //respond accordingly.  If the hardware has been configured like demonstrated
    //on the PICDEM FS USB Demo Board, an I/O pin can be polled to determine the
    //currently selected power source.  On the PICDEM FS USB Demo Board, "RA2"
    //is used for this purpose.  If using this feature, make sure "USE_SELF_POWER_SENSE_IO"
    //has been defined in usbcfg.h, and that an appropriate I/O pin has been mapped
    //to it in io_cfg.h.
    #if defined(USE_SELF_POWER_SENSE_IO)
    tris_self_power = INPUT_PIN;
    #endif
    mInitializeUSBDriver();         // See usbdrv.h
    UserInit();                     // See user.c & .h
}//end InitializeSystem

/******************************************************************************
 * Function:        void USBTasks(void)
 * PreCondition:    InitializeSystem has been called.
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        Service loop for USB tasks.
 * Note:            None
 *****************************************************************************/
void USBTasks(void)                         //Servicing Hardware
{
    USBCheckBusStatus();                    // Must use polling method
    USBDriverService();              	    // Interrupt or polling method
}// end USBTasks

/******************************************************************************
 * Function:        void BlinkUSBStatus(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        BlinkUSBStatus turns on and off LEDs corresponding to
 *                  the USB device state.
 * Note:            mLED macros can be found in io_cfg.h
 *                  usb_device_state is declared in usbmmap.c and is modified
 *                  in usbdrv.c, usbctrltrf.c, and usb9.c
 *****************************************************************************/
//#if !defined(__18F14K50) && !defined(__18F13K50) && !defined(__18LF14K50) && !defined(__18LF13K50)
#if !defined(__18F13K50) && !defined(__18LF13K50)
void BlinkUSBStatus(void)
{
    static word led_count=0;
    if(led_count == 0)led_count = 10000U;
    led_count--;
    //#define mLED_Both_Off()         {mLED_1_Off();mLED_2_Off();}
   //#define mLED_Both_On()          {mLED_1_On();mLED_2_On();}
   // #define mLED_Only_1_On()        {mLED_1_On();mLED_2_Off();}
   // #define mLED_Only_2_On()        {mLED_1_Off();mLED_2_On();}
    if(usb_device_state < CONFIGURED_STATE)
    {
        //mLED_Only_1_On();
	//mLED_1_Toggle();
            //mLED_Both_Off()
	//mLED_Only_1_On()
    }
     else
    {
        if(led_count==0)
        {
            mLED_1_Toggle();
            //mLED_2 = !mLED_1;       // Alternate blink
        }//end if
    }//end if(...)
}//end BlinkUSBStatus
#endif

/*
//#if !defined(__18F14K50) && !defined(__18F13K50) && !defined(__18LF14K50) && !defined(__18LF13K50)
#if !defined(__18F13K50) && !defined(__18LF13K50)
void BlinkUSBStatus(void)
{
//    static word led_count=0;

//    if(led_count == 0)led_count = 10000U;
//    led_count--;

//    #define mLED_Both_Off()         {mLED_1_Off();mLED_2_Off();}
      #define mLED_Both_On()          {mLED_1_On();mLED_2_On();}
//    #define mLED_Only_1_On()        {mLED_1_On();mLED_2_Off();}
//    #define mLED_Only_2_On()        {mLED_1_Off();mLED_2_On();}

mLED_Both_On();


//	 if(usb_device_state < CONFIGURED_STATE)
//	 {
//		//mLED_Only_1_On();
//		//mLED_1_Toggle();		
//		mLED_Both_Off()
//		//mLED_Only_1_On()
//	 } 
//	 else
//   {
//         if(led_count==0)
//         {
//             mLED_1_Toggle();
//             mLED_2 = !mLED_1;       // Alternate blink
//         }//end if
//    }//end if(...)
}//end BlinkUSBStatus
#endif
*/
/** EOF main.c ***************************************************************/
