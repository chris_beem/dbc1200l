When the Microprocessor is working @5V then switch ON CONFIG bit: 

Main.c
        #pragma config VREGEN   = ON        //USB Voltage Regulator

When the Microprocessor is working @3.3V then switch OFF CONFIG bit: 

        #pragma config VREGEN   = OFF        //USB Voltage Regulator