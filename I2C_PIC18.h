//*************************************************************************
//
//Description:	Include-file for PIC18F4550 Microchip
//		Contains all I2CI-declarations I2C_PIC18.h
//
//Filename	: I2C_PIC18.h
//Version	: 1.0
//Date		: 11-12-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
//*************************************************************************
void I2CDelay(unsigned int j);
void SetSCL(void);
void ClrSCL(void);
void SetSDA(void);
void ClrSDA(void);
char PollSDA(void);
void SDAisInput(void);
void SDAisOutput(void);
void I2CStart(void);
void I2CStop(void);
char ReadI2CAck(void);  
void SendI2CNoAck(void);
void WriteI2CByte(char Byte);
unsigned char ReadI2CByte(void);
void SendI2CAck(void);
int ReadI2CMID(int Address, int Register);
void WriteI2CMID(int Address, int Register, int Senddata);
int ReadI2CReg(unsigned char *Data, int readLen);
int WriteI2CReg(unsigned char *Data, int writeLen);     



