#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../ATC.c ../EEPROM_Function.c ../Switch_Eq_Attn_Conversion.c ../c018i.c ../I2C_PIC18.c ../LoCon_Commands.c ../LoCon_Interface_PIC18.c ../Serial_Data_V01.c ../MCP9808.c ../Timers.c ../USB_Commands.c ../LoCon_USB-LoCon.c ../USB/usb_device.c "../USB/HID Device Driver/usb_function_hid.c" ../usb_descriptors.c ../main.c ../variables.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1472/ATC.o ${OBJECTDIR}/_ext/1472/EEPROM_Function.o ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o ${OBJECTDIR}/_ext/1472/c018i.o ${OBJECTDIR}/_ext/1472/I2C_PIC18.o ${OBJECTDIR}/_ext/1472/LoCon_Commands.o ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o ${OBJECTDIR}/_ext/1472/MCP9808.o ${OBJECTDIR}/_ext/1472/Timers.o ${OBJECTDIR}/_ext/1472/USB_Commands.o ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o ${OBJECTDIR}/_ext/1360907413/usb_device.o ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o ${OBJECTDIR}/_ext/1472/usb_descriptors.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/variables.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1472/ATC.o.d ${OBJECTDIR}/_ext/1472/EEPROM_Function.o.d ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o.d ${OBJECTDIR}/_ext/1472/c018i.o.d ${OBJECTDIR}/_ext/1472/I2C_PIC18.o.d ${OBJECTDIR}/_ext/1472/LoCon_Commands.o.d ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o.d ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o.d ${OBJECTDIR}/_ext/1472/MCP9808.o.d ${OBJECTDIR}/_ext/1472/Timers.o.d ${OBJECTDIR}/_ext/1472/USB_Commands.o.d ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o.d ${OBJECTDIR}/_ext/1360907413/usb_device.o.d ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o.d ${OBJECTDIR}/_ext/1472/usb_descriptors.o.d ${OBJECTDIR}/_ext/1472/main.o.d ${OBJECTDIR}/_ext/1472/variables.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1472/ATC.o ${OBJECTDIR}/_ext/1472/EEPROM_Function.o ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o ${OBJECTDIR}/_ext/1472/c018i.o ${OBJECTDIR}/_ext/1472/I2C_PIC18.o ${OBJECTDIR}/_ext/1472/LoCon_Commands.o ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o ${OBJECTDIR}/_ext/1472/MCP9808.o ${OBJECTDIR}/_ext/1472/Timers.o ${OBJECTDIR}/_ext/1472/USB_Commands.o ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o ${OBJECTDIR}/_ext/1360907413/usb_device.o ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o ${OBJECTDIR}/_ext/1472/usb_descriptors.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/variables.o

# Source Files
SOURCEFILES=../ATC.c ../EEPROM_Function.c ../Switch_Eq_Attn_Conversion.c ../c018i.c ../I2C_PIC18.c ../LoCon_Commands.c ../LoCon_Interface_PIC18.c ../Serial_Data_V01.c ../MCP9808.c ../Timers.c ../USB_Commands.c ../LoCon_USB-LoCon.c ../USB/usb_device.c ../USB/HID Device Driver/usb_function_hid.c ../usb_descriptors.c ../main.c ../variables.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F45K50
MP_PROCESSOR_OPTION_LD=18f45k50
MP_LINKER_DEBUG_OPTION=  -u_DEBUGSTACK
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1472/ATC.o: ../ATC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/ATC.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ATC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/ATC.o   ../ATC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/ATC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ATC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/EEPROM_Function.o: ../EEPROM_Function.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/EEPROM_Function.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/EEPROM_Function.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/EEPROM_Function.o   ../EEPROM_Function.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/EEPROM_Function.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/EEPROM_Function.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o: ../Switch_Eq_Attn_Conversion.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o   ../Switch_Eq_Attn_Conversion.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/c018i.o: ../c018i.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/c018i.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/c018i.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/c018i.o   ../c018i.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/c018i.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/c018i.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/I2C_PIC18.o: ../I2C_PIC18.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_PIC18.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_PIC18.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/I2C_PIC18.o   ../I2C_PIC18.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/I2C_PIC18.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/I2C_PIC18.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/LoCon_Commands.o: ../LoCon_Commands.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Commands.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Commands.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/LoCon_Commands.o   ../LoCon_Commands.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/LoCon_Commands.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/LoCon_Commands.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o: ../LoCon_Interface_PIC18.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o   ../LoCon_Interface_PIC18.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Serial_Data_V01.o: ../Serial_Data_V01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o   ../Serial_Data_V01.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Serial_Data_V01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MCP9808.o: ../MCP9808.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/MCP9808.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/MCP9808.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/MCP9808.o   ../MCP9808.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/MCP9808.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MCP9808.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Timers.o: ../Timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Timers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/Timers.o   ../Timers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/USB_Commands.o: ../USB_Commands.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/USB_Commands.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/USB_Commands.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/USB_Commands.o   ../USB_Commands.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/USB_Commands.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/USB_Commands.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o: ../LoCon_USB-LoCon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o   ../LoCon_USB-LoCon.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1360907413/usb_device.o: ../USB/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360907413" 
	@${RM} ${OBJECTDIR}/_ext/1360907413/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360907413/usb_device.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1360907413/usb_device.o   ../USB/usb_device.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1360907413/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360907413/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1914349627/usb_function_hid.o: ../USB/HID\ Device\ Driver/usb_function_hid.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1914349627" 
	@${RM} ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o.d 
	@${RM} ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o   "../USB/HID Device Driver/usb_function_hid.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1914349627/usb_function_hid.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/usb_descriptors.o: ../usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_descriptors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/usb_descriptors.o   ../usb_descriptors.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/usb_descriptors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/usb_descriptors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/main.o   ../main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/variables.o: ../variables.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/variables.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/variables.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1 -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/variables.o   ../variables.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/variables.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/variables.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/1472/ATC.o: ../ATC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/ATC.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ATC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/ATC.o   ../ATC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/ATC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ATC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/EEPROM_Function.o: ../EEPROM_Function.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/EEPROM_Function.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/EEPROM_Function.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/EEPROM_Function.o   ../EEPROM_Function.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/EEPROM_Function.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/EEPROM_Function.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o: ../Switch_Eq_Attn_Conversion.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o   ../Switch_Eq_Attn_Conversion.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Switch_Eq_Attn_Conversion.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/c018i.o: ../c018i.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/c018i.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/c018i.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/c018i.o   ../c018i.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/c018i.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/c018i.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/I2C_PIC18.o: ../I2C_PIC18.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_PIC18.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/I2C_PIC18.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/I2C_PIC18.o   ../I2C_PIC18.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/I2C_PIC18.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/I2C_PIC18.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/LoCon_Commands.o: ../LoCon_Commands.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Commands.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Commands.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/LoCon_Commands.o   ../LoCon_Commands.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/LoCon_Commands.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/LoCon_Commands.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o: ../LoCon_Interface_PIC18.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o   ../LoCon_Interface_PIC18.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/LoCon_Interface_PIC18.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Serial_Data_V01.o: ../Serial_Data_V01.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o   ../Serial_Data_V01.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Serial_Data_V01.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Serial_Data_V01.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MCP9808.o: ../MCP9808.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/MCP9808.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/MCP9808.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/MCP9808.o   ../MCP9808.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/MCP9808.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MCP9808.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Timers.o: ../Timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Timers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/Timers.o   ../Timers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/USB_Commands.o: ../USB_Commands.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/USB_Commands.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/USB_Commands.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/USB_Commands.o   ../USB_Commands.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/USB_Commands.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/USB_Commands.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o: ../LoCon_USB-LoCon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o   ../LoCon_USB-LoCon.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/LoCon_USB-LoCon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1360907413/usb_device.o: ../USB/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360907413" 
	@${RM} ${OBJECTDIR}/_ext/1360907413/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360907413/usb_device.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1360907413/usb_device.o   ../USB/usb_device.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1360907413/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360907413/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1914349627/usb_function_hid.o: ../USB/HID\ Device\ Driver/usb_function_hid.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1914349627" 
	@${RM} ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o.d 
	@${RM} ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o   "../USB/HID Device Driver/usb_function_hid.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1914349627/usb_function_hid.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1914349627/usb_function_hid.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/usb_descriptors.o: ../usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_descriptors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/usb_descriptors.o   ../usb_descriptors.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/usb_descriptors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/usb_descriptors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/main.o   ../main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/variables.o: ../variables.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/variables.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/variables.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I".." -I"../USB/Include/Usb" -I"../USB/Include" -ms -oa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/variables.o   ../variables.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/variables.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/variables.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../rm18f45K50\ -\ HID\ Bootload.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\rm18f45K50 - HID Bootload.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"$(BINDIR_)$(TARGETBASE).map" -w -l"../../MCC18/lib"  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_REAL_ICE=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../rm18f45K50\ -\ HID\ Bootload.lkr ../Loadable/PIC18F45K50_UNIVERSAL_BOOTLOADER_PIC18F45K50_V1_30.X_UNIFIED.hex
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\rm18f45K50 - HID Bootload.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"$(BINDIR_)$(TARGETBASE).map" -w -l"../../MCC18/lib"  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
	@echo "Creating unified hex file"
	@"C:/Program Files (x86)/Microchip/MPLABX/v5.10/mplab_platform/platform/../mplab_ide/modules/../../bin/hexmate" --edf="C:/Program Files (x86)/Microchip/MPLABX/v5.10/mplab_platform/platform/../mplab_ide/modules/../../dat/en_msgs.txt" dist/${CND_CONF}/${IMAGE_TYPE}/DBC_1200_LITE_Amplifier_Application.X.${IMAGE_TYPE}.hex ../Loadable/PIC18F45K50_UNIVERSAL_BOOTLOADER_PIC18F45K50_V1_30.X_UNIFIED.hex -odist/${CND_CONF}/production/DBC_1200_LITE_Amplifier_Application.X.production.unified.hex

endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
