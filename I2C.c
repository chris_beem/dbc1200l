/********************************************************************
FileName: I2C.c
Processor: PIC18 Microcontrollers
Complier: Microchip C18 (for PIC18) or C30 (for PIC24)
Company: Microchip Technology, Inc.
#include <p18cxxx.h> // This code is developed for PIC18F2550
//It can be modified to be used with any PICmicro with MSSP module
/** PRIVATE PROTOTYPES *********************************************/
void i2c_init(void);
void i2c_start(void);
void i2c_repStart(void);
void i2c_stop(void);
unsigned char i2c_write( unsigned char i2cWriteData );
unsigned char i2c_read( unsigned char ack );
/********************************************************************
* Function Name: i2c_init
* Return Value: void
* Parameters: Enable SSP
* Description: This function sets up the SSP1 module on a
* PIC18CXXX device for use with a Microchip I2C
********************************************************************/
#include "library.h"
#include <p18f45k50.h>


void i2c_init(void) {
TRISBbits.TRISB0 = 1; // Digital Output (make it input only when reading data)
TRISBbits.TRISB1 = 1; // Digital Output
SSP1CON1 = 0x28; // enable I2C Master mode
SSPCON2 = 0x00; // clear control bits
SSPSTAT = 0x80; // disable slew rate control; disable SMBus
SSPADD = 19; // set baud rate to 100 kHz (Fosc = 48 MHz)
PIR1bits.SSPIF = 0;
PIR2bits.BCLIF = 0;
SSPCON2bits.SEN = 0; // force idle condition
}


/********************************************************************
* Function Name: i2c_start
* Return Value: void
* Parameters: void
* Description: Send I2C Start Command
********************************************************************/
void i2c_start(void) {
PIR1bits.SSPIF = 0; //clear flag
while (SSPSTATbits.BF ); // wait for idle condition
SSPCON2bits.SEN = 1; // initiate START condition
while (!PIR1bits.SSPIF) ; // wait for a flag to be set
PIR1bits.SSPIF = 0; // clear flag
}
/********************************************************************
* Function Name: i2c_repStart
* Return Value: void
* Parameters: void
* Description: Resend I2C Start Command
*
********************************************************************/
void i2c_repStart(void) {
PIR1bits.SSPIF = 0; // clear flag
while ( SSPSTATbits.BF ) ; // wait for idle condition
SSPCON2bits.RSEN = 1; // initiate Repeated START condition
while (!PIR1bits.SSPIF) ; // wait for a flag to be set
PIR1bits.SSPIF = 0; // clear flag
}
/********************************************************************
* Function Name: i2c_stop
* Return Value: void
* Parameters: void
* Description: Send I2C Stop command
*
********************************************************************/
void i2c_stop(void) {
PIR1bits.SSPIF = 0; // clear flag
while ( SSPSTATbits.BF ) ; // wait for idle condition
SSPCON2bits.PEN = 1; // Initiate STOP condition
while (!PIR1bits.SSPIF) ; // wait for a flag to be set
PIR1bits.SSPIF = 0; // clear flag
}

/********************************************************************
* Function Name: i2c_write
* Return Value: Status byte for WCOL detection.
* Parameters: Single data byte for I2C2 bus.
* Description: This routine writes a single byte to the
* I2C2 bus.
********************************************************************/
unsigned char i2c_write( unsigned char i2cWriteData ) 
{
	PIR1bits.SSPIF = 0; // clear interrupt
	while ( SSPSTATbits.BF ) ; // wait for idle condition
	SSPBUF = i2cWriteData; // Load SSPBUF with i2cWriteData (the value to be transmitted)
	while (!PIR1bits.SSPIF) ; // wait for a flag to be set
	PIR1bits.SSPIF = 0; // clear flag
	return ( !SSPCON2bits.ACKSTAT ); // function returns '1' if transmission is acknowledged
}

/********************************************************************
* Function Name: i2c_read
* Return Value: contents of SSP2BUF register
* Parameters: ack = 1 and nak = 0
* Description: Read a byte from I2C bus and ACK/NAK device
********************************************************************/
unsigned char i2c_read( unsigned char ack ) 
{
	unsigned char i2cReadData;
	PIR1bits.SSPIF = 0;// clear interrupt
	
	while ( SSPSTATbits.BF ) ; // wait for idle condition
	SSPCON2bits.RCEN = 1; // enable receive mode
	
	while (!PIR1bits.SSPIF) ; // wait for a flag to be set
	PIR1bits.SSPIF = 0;// clear flag
	i2cReadData = SSPBUF; // Read SSPBUF and put it in i2cReadData
	
	if ( ack ) { // if ack=1
	SSPCON2bits.ACKDT = 0; // then transmit an Acknowledge
	} else {
	SSPCON2bits.ACKDT = 1; // otherwise transmit a Not Acknowledge
	}
	SSPCON2bits.ACKEN = 1; // send acknowledge sequence
	
	while (!PIR1bits.SSPIF) ; // wait for a flag to be set
	PIR1bits.SSPIF = 0;// clear flag
	return( i2cReadData ); // return the value read from SSPBUF
}