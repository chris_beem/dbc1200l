//*************************************************************************
//
//Description:	Source-file for Timer Functions
//
//Filename	: Timers.c
//Version	: 0.1
//Date		: 15-10-2014
//Copyright	: Technetix B.V.
//Author	: ing. C.Beem, 
// Code based on ATC.C from DBX platform
//
//*************************************************************************

#include "library.h"

un_ATCSwOffsets OF;                                                          //EEPROM. Contains te offset from all the switches
un_ATCModSwFlags SW;                                                            //RAM only. Contains the present switches for ASLC and setting index
st_ATC ATC;
st_TC TC;
st_ES ES;

//========================== Module Functions ================================//
void ATC_DeInit(void)
{
    ES.Required = 0;                                                            //Reset Temporary required action
    TC.LastTemp = 0;                                                            //Reset Last temperature on which the offset is determined
    TC.Require = 0;                                                        //Reset ATC_RequiOffM1 in 0.5dB steps. Depends on temperature
    TC.Perform = 0;                                                        //Clr ATC_PerfoOffM1 before the ATC action. :TOLOOKAT    
    //ATC_ClearOffsets();
    OF.Sw[INT_ATT] = 0;                                                //Clear offsets to do a recalculation for a module write
    OF.Sw[PRE_ATT] = 0;
}

void ATC_Init(void)
{
    ATC_DeInit();
    ATC_UpdateAllOffsets(DS_MOD_NEVER_WRITE, DS_OFF_ALWAYS_UPDATE, DS_ATC_NO_STEPPING);             //For updating the A3C.State.All in the set A3C mode reply
}

void StoreOldSetStepping(void)                                                           //Delay needed for stepping
{
    Old_Info.SET.att_int = Info.SET.att_int;
    Old_Info.SET.att_pre = Info.SET.att_pre;
    Old_Info.SET.att_us = Info.SET.att_us;
    Old_Info.SET.eq_int = Info.SET.eq_int;
    Old_Info.SET.eq_pre = Info.SET.eq_pre;
    Old_Info.SET.eq_us = Info.SET.eq_us;
    Old_Info.SET.pwr_sw = Info.SET.pwr_sw;    
    Old_Info.SET.att_int_corrected = Info.SET.att_int_corrected;
    Old_Info.SET.att_pre_corrected = Info.SET.att_pre_corrected;
}

void ATC_CheckOffset(int WriteMod, int UseStepping)
{
    int Difference = 0;                                                         //Contains difference between required and performed
    int ReadID = 0;
    int Changed = 0;
    ES.Required = 0;                                                            //Contains the required Att change value before ATC_EditAttenuationMx

        do
        {
            TC.Perform =  (OF.Sw[INT_ATT] + OF.Sw[PRE_ATT]);                       //Calculate performed offset M1

            Difference = (TC.Require - TC.Perform);                   //Calculate the difference

            if(Difference > 0  )                                                //Positive action. More attenuation needed.
            {
                ES.Required = 1;
            }
            else if(Difference < 0  )                                           //Negative action. Less attenuation needed.
            {
                ES.Required = -1;
            }

            if(Difference != 0)                                                 //only if required is -1 or +1
            {
                if(Changed == 0)
                {
                    if(UseStepping == DS_ATC_STEPPING)                          //Only reload old Settings if ATC stepping is needed
                    {
                        //StoreOldSetStepping();             //Store old offsets from this module  #UNUSED NOW
                    }
                    Changed = 1;                    
                }
                                            
                ES.TypeSw = ATTENUATOR;                                         //Load the switchType that you want to edit. Attenuation of Equalizing

                ES.Priority = INT_FIRST;//PRE_FIRST;

                ES_EditSwitch();                                                //Edit the switch according to the values in the ES (EditSwitch) struc
            
//                if(UseStepping == DS_ATC_STEPPING)                          //Only reload old Settings if ATC stepping is needed
//                    {
//                        Delay_ATC_stepping();
//                    }
            
            }
            else
            {
                ES.Required = 0;                                                //No difference. No action needed.
            }
            
        
            
         }while((ATC.State == ATC_OK) && (Difference != 0));             //Do these actions as long as there is a difference and the status is ok
}



int ATC_RoundOffAndConvertIn0_5dB(void)
{
    float Tempr = TC.LastTemp;
    float CorrValue  = 0;
    
    CorrValue = (POL_DEG_0 + (Tempr * POL_DEG_1) + (Tempr * Tempr * POL_DEG_2)); //polynomial         
    
    CorrValue *= -2;                                                            //Multiply for 0.5db steps and invert for make attenuation positive 

    if(CorrValue > 0 )                                                          //Add or subtract 0.5 for round up
    {
        CorrValue += 0.5;
    }
    else if(CorrValue < 0 )
    {
        CorrValue -= 0.5; 
    }
    return CorrValue;
}

void ATC_UpdateAllOffsets(int WriteMod, int UpdateOff, int UseStepping)
{
    TEMP_CalculateTemperature();
    
    if((ABSOLUTE(HARDWARE.Temperature - TC.LastTemp) >= 2 ) || (UpdateOff == DS_OFF_ALWAYS_UPDATE))  //Temperature changed more than 2 degrees or Force ATC is set
    {  

        TC.LastTemp = HARDWARE.Temperature;                                            //Store Last Temperature

        TC.Require = ATC_RoundOffAndConvertIn0_5dB();//ATC_DetermineOffset();                             //Determine required Offset for M1
        ATC_CheckOffset(WriteMod, UseStepping);
    
        if(ATC.State == ATC_CANNOT_REACH_VALUE) 
        {
           ATC_DeInit();
        }
    }
}


void ES_EditSwitch(void)
{
    int PreValue;                                                               //Contains the desired PreSw value
    int IntValue;                                                               //Contains the desired PreSw value
    int PreSw = 0;                                                              //Contains the Prestage Sw offset index number
    int IntSw = 0;                                                              //Contains the Interstage Sw offset index number

    ATC.State = ATC_OK;

    PreSw = PRE_ATT;
    IntSw = INT_ATT;
 
    PreValue = Info.SET.att_pre + OF.Sw[PRE_ATT] + ES.Required;                                     //Calculate the desired setting for the Prestage switch
    IntValue = Info.SET.att_int + OF.Sw[INT_ATT] + ES.Required;                                     //Calculate the desired setting for the Interstage switch

    if(ES.Priority == PRE_FIRST)
    { 
        if(((OF.Sw[IntSw] > 0) && (ES.Required == -1)) ||
           ((OF.Sw[IntSw] < 0) && (ES.Required ==  1)))
        {
            if((IntValue >= 0) && (IntValue <= 63))                 //Required Setting valid?
            {
                 OF.Sw[IntSw] += ES.Required;                       //Update Offset IntSwitch to RAM
            }
            else if((PreValue >= 0) && (PreValue <= 63))                      //Required Setting valid?
            {
                OF.Sw[PreSw] += ES.Required;                        //Update Offset PreSwitch to RAM
            }
            else
            {
                ATC.State = ATC_CANNOT_REACH_VALUE;
            }
        }
        else
        {
            if((PreValue >= 0) && (PreValue <= 63))                           //Required Setting valid?
            {
                OF.Sw[PreSw] += ES.Required;                        //Update Offset PreSwitch to RAM
            }
            else if((IntValue >= 0) && (IntValue <= 63))                      //Required Setting valid?
            {
                OF.Sw[IntSw] += ES.Required;                        //Update Offset IntSwitch to RAM
            }
            else
            {
                ATC.State = ATC_CANNOT_REACH_VALUE;
            }
        }
    }
    else if(ES.Priority == INT_FIRST)
    {
        if(((OF.Sw[PreSw] > 0) && (ES.Required == -1)) ||           //Check if switch back is needed
           ((OF.Sw[PreSw] < 0) && (ES.Required ==  1)))
        {
            if((PreValue >= 0) && (PreValue <= 63))                           
            {
                OF.Sw[PreSw] += ES.Required;                        
            }
            else if((IntValue >= 0) && (IntValue <= 63))                      
            {
                OF.Sw[IntSw] += ES.Required;                 
            }
            else
            {
                ATC.State = ATC_CANNOT_REACH_VALUE;
            }
        }
        else
        { 
            if((IntValue >= 0) && (IntValue <= 63))                        
            {
                OF.Sw[IntSw] += ES.Required;                 
            }
            else if((PreValue >= 0) && (PreValue <= 63))                      
            {
                OF.Sw[PreSw] += ES.Required;                 
            }
            else
            {
                ATC.State = ATC_CANNOT_REACH_VALUE;
            }
        }
    }
}