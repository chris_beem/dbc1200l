//*************************************************************************
//
//Description:	Global Definitions for Complete DBC-1200 LITE-Amplifier project (Dashboard)
//
//Filename	: Definitions.h
//Version	: 0.1
//Date		: 20-02-2019
//Copyright	: Technetix B.V.
//Author	: Ing. G. Bronkhorst
//
// 04-12-2012 Modification: Start with first initial version of the Controller
// 05-06-2013 Modification: New LoCon commands (0x9A..0x9F) added
// 05-06-2013 Modification: New Factory Default locations added
// 21-10-2013 Modification: New function to switch off IDS added
// 07-01-2014 Modification: "Amplifier Type Limits" replaced by "Aux 1 Value" and commando swap for Amplifier Type ID and Amplifier Type Limits
// 14-12-2018 Modification: Implemented ATC support and readout of Temperature via RD2 and RD3 PIC18 Pins
//*************************************************************************

// =======================================================================
//Basic Firmware functions to enable and disable (Dashboard)
// =======================================================================

//#define chris_debug_on 
//#define USE_DEBUG_PINS
#define USB_FIRMWARE_UPGRADE                                                    //NEEDS TO BE ENABLED BASIC FEATURE OF EACH PLATFORM!
#define USB_INTERNAL_FUNCTIONS                                                  //Needed to use USB to communicate to product only (on Locon format)
#define USE_SERIAL_DATA_FUNCTIONS                                               //Selection for function to activate communication with all Attenuator/Equalizer and Switches
#define USE_INTERRUPT                                                           //Enable USB Attach line on Interrupt base
#define USE_I2C_FUNCTIONS                                                       //Enable this to activate I2C Functions
#define USE_READOUT_INT16_TEMPERATURE                                           //Readout the Temperature of the I2C MCP9808
//#define USB_LOCON_FUNCTIONS                                                   //For all USB-LoCon functions. Also internal Read/Write LoCon functions as well USB-LoCon Gateway functions
//#define USE_LOCON_FUNCTIONS                                                   //Activate for Standard LoCon RX-TX communication, without communication USB-LoCon.
//#define USE_FIXED_LOCON_ADDRESS                                               //Activate Default LoCon Address. Only activate if it is a special request from a client. Check "definitions.h" for actual fixed address!
//#define USE_ADC_FUNCTIONS                                                     //Need to be enabled to use the ADC functions
//#define USE_INTERNAL_16MHZ_OSCILLATOR                                         //Use the internal 16MHz Oscillator instead of the external oscillator (16MHz)

// =======================================================================
//THESE DEFINITIONS SHOULD BE UPDATED WHEN FIRMWARE IS MODIFIED:
// =======================================================================
#define FIRMWAREVERSIONMSB                                              0x01    //MSB of firmware-version of DBC-1200 LITE-Amplifier interface
#define FIRMWAREVERSIONLSB                                              0x01    //LSB of firmware-version of DBC-1200 LITE-Amplifier interface

#define PRODUCTIONDATEyearMSB                                           0x20    //Most significant byte of production-year
#define PRODUCTIONDATEyearLSB                                           0x19    //Least significant byte of production-year
#define PRODUCTIONDATEmonth                                             0x08    //Byte for production-month
#define PRODUCTIONDATEday                                               0x01    //Byte for production-day

#define DEFAULT_FIXED_LOCON_ADDRESS                                     0xAC    //Fixed address (on special request activated)
#define OWN_LOCON_DEVICE_ID                                             0x5D	//LoCon Product ID DBC-1200 LITE -Amplifier

// =======================================================================
//   I2C Read / Write and Address Commands MCP23008 Microchip
// =======================================================================
#define MID_I2C_WRITE_COMMAND                                           0x00	//Send data to I2C
#define MID_I2C_READ_COMMAND                                            0x01    //Receive data from I2C

#define I2C_READ_COMMAND                                                0x01    //Set bit for I2C read command
// =======================================================================
//   Motherboard version
// =======================================================================
#define DBC                                                             0x00    //DBC Amplifier
#define DBD                                                             0x01    //DBD Amplifier
#define UNKNOWN                                                         0x02    //Not implemented version

#define DBC_1200_LITE_AMPLIFIER                                         0xF1    //In FIRMWARE Fixed Motherboard ID 241 decimal to indicate this is a DBC-1200 LITE

// =======================================================================
//   LoCon Specific General Commands
// =======================================================================
#if defined(USE_LOCON_FUNCTIONS)
                                                                                //General Command Read functions
#define LOCON_CMD_READ_FIRMWAREVERSION                                  0xC0	//Read firmware version of DBC-1200 LITE-Amplifier
#define LOCON_CMD_READ_MODIFIED_BIT                                     0xEF	//Read modified bit
#define LOCON_CMD_READ_STATUS                                           0xF4	//Read status bytes
#define LOCON_CMD_READ_ADDRESS                                          0xF9    //Read Address
#define LOCON_CMD_REQUEST_ID                                            0xFD	//Read ID information (LoCon ID, production date)
#define LOCON_CMD_READ_ERRORS                                           0xFF	//Read Errors (Alarms) in 1 byte

                                                                                //General Command Write functions
#define LOCON_CMD_WRITE_ADDRESS_0                                       0x08	//Write Address 0 (0xA0)
#define LOCON_CMD_WRITE_ADDRESS_1                                       0x18	//Write Address 1 (0xA1)
#define LOCON_CMD_WRITE_ADDRESS_2                                       0x28	//Write Address 2 (0xA2)
#define LOCON_CMD_WRITE_ADDRESS_3                                       0x38	//Write Address 3 (0xA3)
#define LOCON_CMD_WRITE_ADDRESS_4                                       0x48	//Write Address 4 (0xA4)
#define LOCON_CMD_WRITE_ADDRESS_5                                       0x58	//Write Address 5 (0xA5)
#define LOCON_CMD_WRITE_ADDRESS_6                                       0x68	//Write Address 6 (0xA6)
#define LOCON_CMD_WRITE_ADDRESS_7                                       0x78	//Write Address 7 (0xA7)
#define LOCON_CMD_WRITE_ADDRESS_8                                       0x88	//Write Address 8 (0xA8)

#define LOCON_CMD_RESET_MODIFIED_BIT                                    0xEC	//Write to Reset modified bit
#define LOCON_CMD_SET_MODIFIED_BIT                                      0xED	//Write to Set modified bit
#endif

// =======================================================================
//   LoCon Device Specific Commands
// =======================================================================
#if defined(USE_LOCON_FUNCTIONS)

#define LOCON_CMD_READ_SETTING_EQ_HINGE_DS1                             0x8A    //Read EQ Hinge for Downstream Module 1
#define LOCON_CMD_WRITE_SETTING_EQ_HINGE_DS1                            0x8B    //Write EQ Hinge for Downstream Module 1

#define LOCON_CMD_READ_AUX_1_VALUE_BLL                                  0x9A    //Read software only to provide a future function Aux 1
#define LOCON_CMD_WRITE_AUX_1_VALUE_BLL                                 0x9B    //Write software only to provide a future function Aux 1
#define LOCON_CMD_READ_FACTORY_DEFAULT_SETTING                          0x9C    //Read Factory Default Setting
#define LOCON_CMD_WRITE_FACTORY_DEFAULT_SETTING                         0x9D    //Write Factory Default Setting

#define LOCON_CMD_READ_SETTING_0_5_STEPS_ENABLED                        0x9E    //Read if Setting for 0.5dB Steps is enabled or disabled
#define LOCON_CMD_WRITE_SETTING_0_5_STEPS_ENABLED                       0x9F    //Write Setting for 0.5dB Steps is enabled (0x01) or disabled (DEFAULT) (0x00)

#define LOCON_CMD_READ_UPSTREAM_ATTN                                    0xA0    //Read Upstream Attenuator
#define LOCON_CMD_READ_PRE_ATTN_MOD_1                                   0xA1    //Read Pre Attenuator Module 1
#define LOCON_CMD_READ_INTERSTAGE_ATTN_MOD_1                            0xA3    //Read Interstage Attenuator Module 1
#define LOCON_CMD_READ_UPSTREAM_EQU                                     0xA5    //Read Upstream Equalizer
#define LOCON_CMD_READ_PRE_EQU_MOD_1                                    0xA6    //Read Pre Equalizer Module 1
#define LOCON_CMD_READ_INTERSTAGE_EQU_MOD_1                             0xA8    //Read Interstage Equalizer Module 1

#define LOCON_CMD_READ_ATC_TEMPERATURE                                  0xAA    //Read ATC Temperature
#define LOCON_CMD_READ_ATC_ENABLED_DISABLED                             0xAB    //Read ATC Enabled or Disabled
#define LOCON_CMD_READ_DOWNSTREAM_POWER_SW_MOD_1                        0xAC    //Read Downstream Power Mode Switch Module 1
#define LOCON_CMD_READ_PRODUCT_MAINBOARD_IDENTIFIER                     0xE2    //Read Mainboard Identifier (DBD/DBC mainboard or DBC-1200 LITE Amplifier)
#define LOCON_CMD_READ_TYPE_ID_AMPLIFIER_BLL                            0xE5    //Read Type ID value of the digital Module (BLL only)

#define LOCON_CMD_WRITE_UPSTREAM_ATTN                                   0xB0    //Write Upstream Attenuator
#define LOCON_CMD_WRITE_PRE_ATTN_MOD_1                                  0xB1    //Write Pre Attenuator Module 1
#define LOCON_CMD_WRITE_INTERSTAGE_ATTN_MOD_1                           0xB3    //Write Interstage Attenuator Module 1
#define LOCON_CMD_WRITE_UPSTREAM_EQU                                    0xB5    //Write Upstream Equalizer
#define LOCON_CMD_WRITE_PRE_EQU_MOD_1                                   0xB6    //Write Pre Equalizer Module 1
#define LOCON_CMD_WRITE_INTERSTAGE_EQU_MOD_1                            0xB8    //Write Interstage Equalizer Module 1

#define LOCON_CMD_WRITE_ATC_ENABLED_DISABLED                            0xBB    //Write ATC Enabled or Disabled
#define LOCON_CMD_WRITE_DOWNSTREAM_POWER_SW_MOD_1                       0xBC    //Write Downstream Power Mode Switch Module 1
#define LOCON_CMD_WRITE_TYPE_ID_AMPLIFIER_BLL                           0xF5    //Write Type ID value of the digital Module (BLL only)

#define LOCON_CMD_READ_ALL_ATTN_AT_ONCE                                 0xC1    //Read all all Attenuators in one command (5 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Pre Attenuator Mod 2, Interstage Attenuator Mod 1, Interstage Attenuator Mod 2)
#define LOCON_CMD_READ_ALL_EQU_AT_ONCE                                  0xC2    //Read all all Equalizers in one command (5 bytes, Upstream Equalizer, Pre Equalizer Mod 1, Pre Equalizer Mod 2, Interstage Equalizer Mod 1, Interstage Equalizer Mod 2)
#define LOCON_CMD_READ_ALL_SWITCHES_AT_ONCE                             0xC3    //Read all Switches in one command (6 bytes, Upstream Slope Point, Upstream On/Off Switch, Downstream Power Switch Mod 1, Downstream Power Switch Mod 2, IDS Switch Output 1, IDS Switch Output 2)
#define LOCON_CMD_READ_ALL_SETTINGS_AT_ONCE                             0xC4    //Read all settings in one command (16 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Pre Attenuator Mod 2, Interstage Attenuator Mod 1, Interstage Attenuator Mod 2, Upstream Equalizer, Pre Equalizer Mod 1, Pre Equalizer Mod 2, Interstage Equalizer Mod 1, Interstage Equalizer Mod 2, Upstream Slope Point, Upstream On/Off Switch, Downstream Power Switch Mod 1, Downstream Power Switch Mod 2, IDS Switch Output 1, IDS Switch Output 2)

#define LOCON_CMD_READ_STEP_0_5_STEPS_TIME_DELAY_MS                     0xC7    //Read the actual delay time for the Step Attenuator
#define LOCON_CMD_WRITE_STEP_0_5_STEPS_TIME_DELAY_MS                    0xC9    //Write the actual delay time for the Step Attenuator

#define LOCON_CMD_WRITE_ALL_ATTN_AT_ONCE                                0xD1    //Write all all Attenuators in one command (5 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Pre Attenuator Mod 2, Interstage Attenuator Mod 1, Interstage Attenuator Mod 2)
#define LOCON_CMD_WRITE_ALL_EQU_AT_ONCE                                 0xD2    //Write all all Equalizers in one command (5 bytes, Upstream Equalizer, Pre Equalizer Mod 1, Pre Equalizer Mod 2, Interstage Equalizer Mod 1, Interstage Equalizer Mod 2)
#define LOCON_CMD_WRITE_ALL_SWITCHES_AT_ONCE                            0xD3    //Write all Switches in one command (6 bytes, Upstream Slope Point, Upstream On/Off Switch, Downstream Power Switch Mod 1, Downstream Power Switch Mod 2, IDS Switch Output 1, IDS Switch Output 2)
#define LOCON_CMD_WRITE_ALL_SETTINGS_AT_ONCE                            0xD4    //Write all settings in one command (16 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Pre Attenuator Mod 2, Interstage Attenuator Mod 1, Interstage Attenuator Mod 2, Upstream Equalizer, Pre Equalizer Mod 1, Pre Equalizer Mod 2, Interstage Equalizer Mod 1, Interstage Equalizer Mod 2, Upstream Slope Point, Upstream On/Off Switch, Downstream Power Switch Mod 1, Downstream Power Switch Mod 2, IDS Switch Output 1, IDS Switch Output 2)

#define LOCON_CMD_SET_RESET                                             0xDE    //LoCon command to reset Product
#define LOCON_CMD_SET_FACTORY_DEFAULT                                   0xDF    //LoCon command for setting the Product to factory default
#endif

// =======================================================================
//   Reply Port address destinations USB functions
// =======================================================================
#define NO_MODULE_SELECTED                                                 0    //Destination None
#define PRE_ATTENUATOR_MODULE_1                                            1    //Destination Pre Attenuator Module 1
#define INTERSTAGE_ATTENUATOR_MODULE_1                                     2    //Destination Interstage Attenuator Module 1
#define PRE_EQUALIZER_MODULE_1                                             3    //Destination Pre Equaliser Module 1
#define INTERSTAGE_EQUALIZER_MODULE_1                                      4    //Destination Interstage Equaliser Module 1
#define UPSTREAM_ATTENUATOR                                                5    //Destination Upstream Attenuator
#define UPSTREAM_EQUALIZER                                                 6    //Destination Upstream Equaliser
#define DEFAULT_ATTENUATION_NO_MODULE                                   0x0C    //Set Dummy condition to 6dB
#define PRE_ATTENUATOR_STARTUP_MODULE_1                                 0x00    //Set condition at startup to 3dB to give room for ATC correction 
#define INTERSTAGE_ATTENUATOR_STARTUP_MODULE_1                          0x06    //Set condition at startup to 0dB
#define UPSTREAM_ATTENUATOR_STARTUP                                     0x00    //Set condition at startup to .5dB to give room for ATC

// =======================================================================
//   LoCon Product Information
// =======================================================================
#define LOCON_BUFFERLENGTH                                                20    //Size of LoCon receive- and transmitbuffers

// =======================================================================
//   USB Handlers
// =======================================================================
#define USB_HANDLE_ERROR                                                0xEA    //WRONG HANDLER DETECTED, is always there. Also when only USB Firmware Upgrade is selected!

#if defined(USB_LOCON_FUNCTIONS)
#define USB_LOCON_NO_REPLY_REQUIRED_FIRST                               0xE0    //LoCon No reply asked First Transmission
#define USB_LOCON_NO_REPLY_REQUIRED_REPEAT                              0xE1    //Repeated Transmission No reply asked
#define USB_COMMAND_NOT_SUPPORTED                                       0xE5    //Command not supported in LoConGateway Module (For future purposes when Gateway contains internal functions..)
#define USB_LOCON_RECEIVE_PARITY_ERROR                                  0xE6    //Parity error in received LoCon-data
#define USB_LOCON_TIMEOUT_E2                                            0xE7    //LoCon Time out error by command 0xE2
#define USB_LOCON_TIMEOUT_E3                                            0xE7    //LoCon Time out error by command 0xE3
#define USB_FRAMING_ERROR                                               0xE5    //Unknown command Framing. (no Master framing (not E0 to E3 as Framing)
#define USB_MESSAGE_LENGTH_ERROR                                        0xEB    //Wrong USB MESSAGE LENGTH (>20 bytes, DBC-1200 LITE-Amplifier)
#endif

// =======================================================================
//   LoCon Specific General Commands
// =======================================================================
#if defined(USB_INTERNAL_FUNCTIONS)
//General Command Read functions
#define USB_LOCON_CMD_READ_FIRMWAREVERSION                              0xC0    //Read firmware version of DBC-1200 LITE-Amplifier
#define USB_LOCON_CMD_READ_MODIFIED_BIT                                 0xEF    //Read modified bit
#define USB_LOCON_CMD_READ_STATUS                                       0xF4    //Read status bytes
#define USB_LOCON_CMD_READ_ADDRESS                                      0xF9    //Read Address
#define USB_LOCON_CMD_REQUEST_ID                                        0xFD    //Read ID information (LoCon ID, production date)
#define USB_LOCON_CMD_READ_ERRORS                                       0xFF    //Read Errors (Alarms) in 1 byte

//General Command Write functions
#define USB_LOCON_CMD_WRITE_ADDRESS_0                                   0x08    //Write Address 0 (0xA0)
#define USB_LOCON_CMD_WRITE_ADDRESS_1                                   0x18    //Write Address 1 (0xA1)
#define USB_LOCON_CMD_WRITE_ADDRESS_2                                   0x28    //Write Address 2 (0xA2)
#define USB_LOCON_CMD_WRITE_ADDRESS_3                                   0x38    //Write Address 3 (0xA3)
#define USB_LOCON_CMD_WRITE_ADDRESS_4                                   0x48    //Write Address 4 (0xA4)
#define USB_LOCON_CMD_WRITE_ADDRESS_5                                   0x58    //Write Address 5 (0xA5)
#define USB_LOCON_CMD_WRITE_ADDRESS_6                                   0x68    //Write Address 6 (0xA6)
#define USB_LOCON_CMD_WRITE_ADDRESS_7                                   0x78    //Write Address 7 (0xA7)
#define USB_LOCON_CMD_WRITE_ADDRESS_8                                   0x88    //Write Address 8 (0xA8)

#define USB_LOCON_CMD_RESET_MODIFIED_BIT                                0xEC    //Write to Reset modified bit
#define USB_LOCON_CMD_SET_MODIFIED_BIT                                  0xED    //Write to Set modified bit
#endif

// =======================================================================
//   USB LOCON Device Specific Commands
// =======================================================================
#if defined(USB_INTERNAL_FUNCTIONS)

#define USB_LOCON_CMD_READ_SETTING_EQ_HINGE_DS1                         0x8A    //Read EQ Hinge for Downstream Module 1
#define USB_LOCON_CMD_WRITE_SETTING_EQ_HINGE_DS1                        0x8B    //Write EQ Hinge for Downstream Module 1

#define USB_LOCON_CMD_READ_AUX_1_VALUE_BLL                              0x9A    //Read software only to provide a future function Aux 1
#define USB_LOCON_CMD_WRITE_AUX_1_VALUE_BLL                             0x9B    //Write software only to provide a future function Aux 1
#define USB_LOCON_CMD_READ_FACTORY_DEFAULT_SETTING                      0x9C    //Read Factory Default Setting
#define USB_LOCON_CMD_WRITE_FACTORY_DEFAULT_SETTING                     0x9D    //Write Factory Default Setting

#define USB_LOCON_CMD_READ_SETTING_0_5_STEPS_ENABLED                    0x9E    //Read if Setting for 0.5dB Steps is enabled or disabled
#define USB_LOCON_CMD_WRITE_SETTING_0_5_STEPS_ENABLED                   0x9F    //Write Setting for 0.5dB Steps is enabled (0x01) or disabled (DEFAULT) (0x00)

#define USB_LOCON_CMD_READ_UPSTREAM_ATTN                                0xA0    //Read Upstream Attenuator
#define USB_LOCON_CMD_READ_PRE_ATTN_MOD_1                               0xA1    //Read Pre Attenuator Module 1
#define USB_LOCON_CMD_READ_INTERSTAGE_ATTN_MOD_1                        0xA3    //Read Interstage Attenuator Module 1
#define USB_LOCON_CMD_READ_UPSTREAM_EQU                                 0xA5    //Read Upstream Equalizer
#define USB_LOCON_CMD_READ_PRE_EQU_MOD_1                                0xA6    //Read Pre Equalizer Module 1
#define USB_LOCON_CMD_READ_INTERSTAGE_EQU_MOD_1                         0xA8    //Read Interstage Equalizer Module 1

#define USB_LOCON_CMD_READ_ATC_TEMPERATURE                              0xAA    //Read ATC Temperature
#define USB_LOCON_CMD_READ_ATC_ENABLED_DISABLED                         0xAB    //Read ATC Enabled or Disabled
#define USB_LOCON_CMD_READ_DOWNSTREAM_POWER_SW_MOD_1                    0xAC    //Read Downstream Power Mode Switch Module 1
#define USB_LOCON_CMD_READ_PRODUCT_MAINBOARD_IDENTIFIER                 0xE2    //Read Mainboard Identifier (DBD/DBC mainboard or DBC-1200 LITE Amplifier)
#define USB_LOCON_CMD_READ_TYPE_ID_AMPLIFIER_BLL                        0xE5    //Read Type ID value of the digital Module (BLL only)


#define USB_LOCON_CMD_WRITE_UPSTREAM_ATTN                               0xB0    //Write Upstream Attenuator
#define USB_LOCON_CMD_WRITE_PRE_ATTN_MOD_1                              0xB1    //Write Pre Attenuator Module 1
#define USB_LOCON_CMD_WRITE_INTERSTAGE_ATTN_MOD_1                       0xB3    //Write Interstage Attenuator Module 1
#define USB_LOCON_CMD_WRITE_UPSTREAM_EQU                                0xB5    //Write Upstream Equalizer
#define USB_LOCON_CMD_WRITE_PRE_EQU_MOD_1                               0xB6    //Write Pre Equalizer Module 1
#define USB_LOCON_CMD_WRITE_INTERSTAGE_EQU_MOD_1                        0xB8    //Write Interstage Equalizer Module 1

#define USB_LOCON_CMD_WRITE_ATC_ENABLED_DISABLED                        0xBB    //Write ATC Enabled or Disabled
#define USB_LOCON_CMD_WRITE_DOWNSTREAM_POWER_SW_MOD_1                   0xBC    //Write Downstream Power Mode Switch Module 1
#define USB_LOCON_CMD_WRITE_TYPE_ID_AMPLIFIER_BLL                       0xF5    //Write Type ID value of the digital Module (BLL only)

#define USB_LOCON_CMD_READ_ALL_ATTN_AT_ONCE                             0xC1    //Read all all Attenuators in one command (3 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Interstage Attenuator Mod 1)
#define USB_LOCON_CMD_READ_ALL_EQU_AT_ONCE                              0xC2    //Read all all Equalizers in one command (3 bytes, Upstream Equalizer, Pre Equalizer Mod 1, Interstage Equalizer Mod 1)
#define USB_LOCON_CMD_READ_ALL_SWITCHES_AT_ONCE                         0xC3    //Read all Switches in one command (1 byte, Downstream Power Switch Mod 1)
#define USB_LOCON_CMD_READ_ALL_SETTINGS_AT_ONCE                         0xC4    //Read all settings in one command (7 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Interstage Attenuator Mod 1, Upstream Equalizer, Pre Equalizer Mod 1, Interstage Equalizer Mod 1, Downstream Power Switch Mod 1)

#define USB_LOCON_CMD_READ_STEP_0_5_STEPS_TIME_DELAY_MS                 0xC7    //Read the actual delay time for the Step Attenuator
#define USB_LOCON_CMD_WRITE_STEP_0_5_STEPS_TIME_DELAY_MS                0xC9    //Write the actual delay time for the Step Attenuator

#define USB_LOCON_CMD_WRITE_ALL_ATTN_AT_ONCE                            0xD1    //Write all all Attenuators in one command (3 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Interstage Attenuator Mod 1)
#define USB_LOCON_CMD_WRITE_ALL_EQU_AT_ONCE                             0xD2    //Write all all Equalizers in one command (3 bytes, Upstream Equalizer, Pre Equalizer Mod 1, Interstage Equalizer Mod 1)
#define USB_LOCON_CMD_WRITE_ALL_SWITCHES_AT_ONCE                        0xD3    //Write all Switches in one command (1 byte, Downstream Power Switch Mod 1)
#define USB_LOCON_CMD_WRITE_ALL_SETTINGS_AT_ONCE                        0xD4    //Write all settings in one command (7 bytes, Upstream Attenuator, Pre Attenuator Mod 1, Interstage Attenuator Mod 1, Upstream Equalizer, Pre Equalizer Mod 1, Interstage Equalizer Mod 1, Downstream Power Switch Mod 1)

#define USB_LOCON_CMD_SET_RESET                                         0xDE    //LoCon command to reset Product
#define USB_LOCON_CMD_SET_FACTORY_DEFAULT                               0xDF    //LoCon command for setting the Product to factory default
#endif

#if defined(USB_FIRMWARE_UPGRADE)
//Specific USB Commands
#define USB_COMMAND_ERROR                                               0xE5    //Wrong USB Command detected
#define USB_RESET_FIRMWARE                                              0xF0    //Firmware in Reset for entering Bootloader
#define USB_RESET_BOOTLOADER                                            0xE0    //Firmware in Reset for entering Bootloader
#endif

// =======================================================================
//   Reply destinations
// =======================================================================
#define DESTINATION_LOCON                                                  1    //Reply Destination to LoCon RX/TX port
#define DESTINATION_USB                                                    2    //Reply Destination to USB port
#define DESTINATION_UART                                                   3    //Reply Destination to UART port (future feature?)

// =======================================================================
//   Reply Options for LoCon and Bootup
// =======================================================================
#define DEFAULT                                                            0    //Do standard LoCon functionality, also reply the master by LoCon TX communication
#define NON_DEFAULT                                                        1    //Do the calculations for Attenuators/Equalizers, but do not reply to the LoCon Bus
#define BOOT_UP                                                            2    //Device is just rebooted. All settings of the Attenuators and Equalizes are with this define Read from EEPROM and set to the real device!

// =======================================================================
//   Timer Setting
// =======================================================================
#define TIMER0_VAL 0xFFFF-12000  //Preset for timer0 set to 1 second using 256 PS,16Mhz clock,3xPLL,using Fosc/4 source 
#define TIMER0H_VAL                                                     (TIMER0_VAL&0xFF00)>>8;
#define TIMER0L_VAL                                                     (TIMER0_VAL&0x00FF);

// =======================================================================
//   Window selection for Current Monitor
// =======================================================================
#if defined(USE_ADC_FUNCTIONS)
    #define UNDER_BOUNDERY_RF_LEVEL     0                                       //Window for Current measuring. If value measured is lower than this value, then unit is in Alarm
    #define UPPER_BOUNDERY_RF_LEVEL     100                                     //Window for Current measuring. If value measured is higher than this value, then unit is in Alarm
#endif

// =======================================================================
//   EEPROM Address Locations
// =======================================================================
//LoCon Storage functions

#define EE_ADR_UNIFIED_HEX_FLAG                                       0x0000    //Bootloader Flag to indicate if unified HEX programming is used
#define EE_ADR_BOOTLOADER_FLAG                                        0x0001    //Bootloader Flag to indicate if bootloader is requested
#define EE_ADR_VALID_FIRMWARE_PRESENT                                 0x0002    //Check if there's correct Application firmware installed
#define EE_ADR_OWNLOCONADDRESS                                        0x0003    //Address to store LoConAddress of the Product
#define EE_ADR_MODIFIED_BIT                                           0x0004    //Address to store value of modified-bit

//Product specific storage functions
#define EE_ADR_UPSTREAM_ATTN_SETTING                                  0x0005    //Address to store the Upstream Attenuator Setting
#define EE_ADR_PRE_ATTN_MOD_1_SETTING                                 0x0006    //Address to store the Pre Attenuator Module 1 Setting
#define EE_ADR_INTERSTAGE_ATTN_MOD_1_SETTING                          0x0007    //Address to store the Interstage Attenuator Module 1 Setting

#define EE_ADR_UPSTREAM_EQU_SETTING                                   0x0008    //Address to store the Upstream Equaliser Setting
#define EE_ADR_PRE_EQU_MOD_1_SETTING                                  0x0009    //Address to store the Pre Equaliser Module 1 Setting
#define EE_ADR_INTERSTAGE_EQU_MOD_1_SETTING                           0x000A    //Address to store the Interstage Equaliser Module 1 Setting

#define EE_ADR_DOWNSTREAM_POWER_SW_MOD_1_SETTING                      0x000B    //Address to store the Downstream Power Switch Module 1 setting

//SPECIAL FOR BLL
#define EE_ADR_AUX_1_VALUE                                            0x000C    //Address to store the Aux 1 Value
//NEXT is also used in Factory Default
#define EE_ADR_TYPE_ID_AMPLIFIER                                      0x000D    //Address to store the type ID of the amplifier (Type 1,2 3 or 4) FREE BYTE
#define EE_ADR_SETTING_0_5_STEPS_ENABLED                              0x000E    //Address to store the setting if the steps are enabled or disabled
#define EE_ADR_SETTING_0_5_STEPS_TIME_DELAY_MS                        0x000F    //Address to store the value for the setting of the Step Attenuator delay time
#define EE_ADR_SETTING_EQ_HINGE_DS1                                   0x0010    //Address to store the value of the EQ Hinge for Downstream Module 1

//Product specific storage functions
#define EE_ADR_FACTORY_RESTORE_UPSTREAM_ATTN_SETTING                  0x0011    //Address to store the Upstream Attenuator Setting
#define EE_ADR_FACTORY_RESTORE_PRE_ATTN_MOD_1_SETTING                 0x0012    //Address to store the Pre Attenuator Module 1 Setting
#define EE_ADR_FACTORY_RESTORE_INTERSTAGE_ATTN_MOD_1_SETTING          0x0013    //Address to store the Interstage Attenuator Module 1 Setting

#define EE_ADR_FACTORY_RESTORE_UPSTREAM_EQU_SETTING                   0x0014    //Address to store the Upstream Equaliser Setting
#define EE_ADR_FACTORY_RESTORE_PRE_EQU_MOD_1_SETTING                  0x0015    //Address to store the Pre Equaliser Module 1 Setting
#define EE_ADR_FACTORY_RESTORE_INTERSTAGE_EQU_MOD_1_SETTING           0x0016    //Address to store the Interstage Equaliser Module 1 Setting

#define EE_ADR_FACTORY_RESTORE_DOWNSTREAM_POWER_SW_MOD_1_SETTING      0x0017    //Address to store the Downstream Power Switch Module 1 setting

#define EE_ADR_FACTORY_RESTORE_TYPE_ID_AMPLIFIER                      0x0018    //Address to store the value Amplifier TYPE ID used for Factory Default
#define EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_ENABLED              0x0019    //Address to store the value for the setting if 0.5dB is enabled or disabled
#define EE_ADR_FACTORY_RESTORE_SETTING_0_5_STEPS_TIME_DELAY_MS        0x001A    //Address to store the value for the setting of the Step Attenuator delay time
#define EE_ADR_FACTORY_RESTORE_SETTING_EQ_HINGE_DS1                   0x001B    //Address to store the value of the EQ Hinge for Downstream Module 1
#define EE_ADR_SETTING_ATC_ENABLED_DISABLED                           0x001C    //Address to store if ATC Function is enabled or disabled