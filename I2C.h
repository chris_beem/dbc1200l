/********************************************************************
FileName: I2C.h
Processor: PIC18 Microcontrollers
Complier: Microchip C18 (for PIC18) or C30 (for PIC24)
Company: Microchip Technology, Inc.
#include <p18cxxx.h> // This code is developed for PIC18F2550
//It can be modified to be used with any PICmicro with MSSP module


/** PRIVATE PROTOTYPES *********************************************/
void i2c_init(void);
void i2c_start(void);
void i2c_repStart(void);
void i2c_stop(void);
unsigned char i2c_write( unsigned char i2cWriteData );
unsigned char i2c_read( unsigned char ack );
