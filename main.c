/********************************************************************
 FileName:              main.c
 Designer:		Ing. G. Bronkhorst
 Processor:		PIC18F4550
 Hardware:		HardwareProfile.h file.
 Complier:              Microchip C18 (for PIC18)
 Company:		Technetix B.V.
 ********************************************************************
 File Description:
 Change History:
  Rev   Date         Description
  1.00   07/12/2012   Initial release
  1.01   05/02/2013   Modification for Startup delay, support DBC/DBD functions and change Control functions sequence (now I/O |_ |- first)
  1.09   16/10/2014   Upstream Problem covered in code (see release notes document
  1.00   04/12/2018   Special Firmware for DBC-1200 LITE Amplifier, based on V1.09 of the DBDCM-C-1 Firmware (Fixed Hardware, Motherboard and Modules). Module detection is switched off!!)
 *       04/06/2019   Fix USB buffer issue in SendToUSB() (USB_Commands.c) to stay connected  
 ********************************************************************/

//TODO, difference between int and unsigned char dec. for info struct

#ifndef USB_LoCon
#define USB_LoCon


#include <p18f45k50.h>

#include "library.h"

//DECLARATIONS
#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
extern int OwnLoConAddress;                                                     //Register to store own LoCon Address
extern int LoConAddress;                                                        //Register to store received LoCon address from LoCon bus or USB bus to compare with OwnLoCon Address
extern int ModifiedBit;                                                         //Register to store Modified Bit
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined (USB_LOCON_FUNCTIONS)
    extern int NrOfReceivedLoConBytes;                                          //Register array for Receive LoCon commands from Master
    extern int LoConTransmitString[19];                                         //Register array for Send LoCon commands to Master
    extern int ReplyDestination;                                                //Register used for ReceiveLoCon and ExecuteLoCon in Main routine
#endif
    
//Switch states declaration registers
int _OLD_Upstream_Att;
int _OLD_Pre_Att_Mod_1;
int _OLD_Interstage_Att_Mod_1;
int _OLD_Upstream_Equ;
int _OLD_Pre_Equ_Mod_1;
int _OLD_Interstage_Equ_Mod_1;
int _OLD_Downstream_Power_Sw_Mod_1;

int count;


int MainboardVersion;
int Aux_1_Value;
int Amplifier_ID_BLL;
int Setting_0_5dB_Steps;
int Setting_0_5dB_Steps_Time_Delay;
int EQ_Hinge_Downstream_1;
int Upstream_Attenuator_6dB = 0x00;                                             //Global declaration for DBC to indicate an extra 6dB attenuator is needed to simulate IDS function
int ATC_Enabled_Disabled;
// end of declarations


#if defined(USE_ADC_FUNCTIONS)
    int AmplifierCurrentAlarm;
#endif

/** CONFIGURATION **************************************************/
#if !defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER) && !defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
#if defined(PIC18F4550_MICRO)                                                   //Configuration bits for PIC18F4550
        #pragma config PLLDIV   = 5                                             //(20 MHz crystal)
        #pragma config CPUDIV   = OSC1_PLL2   
        #pragma config USBDIV   = 2                                             //Clock source from 96MHz PLL/2
        #pragma config FOSC     = HSPLL_HS
        #pragma config FCMEN    = OFF
        #pragma config IESO     = OFF
        #pragma config PWRT     = OFF
        #pragma config BOR      = OFF
        #pragma config BORV     = 3
        #pragma config VREGEN   = ON                                            //USB Voltage Regulator 
                                                                                //When the Microprocessor is working @3.3V then switch OFF CONFIG bit: #pragma config VREGEN   = OFF       //USB Voltage Regulator
                                                                                //When the Microprocessor is working @5V then switch ON CONFIG bit:    #pragma config VREGEN   = ON        //USB Voltage Regulator
        #pragma config WDTPS    = 32768
        #pragma config MCLRE    = ON
        #pragma config LPT1OSC  = OFF
        #pragma config PBADEN   = OFF
        //#pragma config CCP2MX = ON
        #pragma config STVREN   = ON
        #pragma config LVP      = OFF
        //#pragma config ICPRT  = OFF                                           //Dedicated In-Circuit Debug/Programming
        #pragma config XINST    = OFF                                           //Extended Instruction Set
        #pragma config CP0      = OFF
        #pragma config CP1      = OFF
        //#pragma config CP2    = OFF
        //#pragma config CP3    = OFF
        #pragma config CPB      = OFF
        //#pragma config CPD    = OFF
        #pragma config WRT0     = OFF
        #pragma config WRT1     = OFF
        //#pragma config WRT2   = OFF
        //#pragma config WRT3   = OFF

        #pragma config WRTB     = ON                                            //Boot Block Write Protection 18-02-2009 MODIFIED!!
        //#pragma config WRTB   = OFF
        //#pragma config WDT    = OFF
        #pragma config WDT      = ON

        /*/
        #ifdef __DEBUG
        //#pragma config BKBUG  = ON
        #endif
        #ifndef __DEBUG
            #pragma config BKBUG = OFF
        #endif
         * */

        #pragma config WRTC     = OFF
        //#pragma config WRTD   = OFF
        #pragma config EBTR0    = OFF
        #pragma config EBTR1    = OFF
        //#pragma config EBTR2  = OFF
        //#pragma config EBTR3  = OFF
        #pragma config EBTRB    = OFF

#elif defined(PIC18F14K50_MICRO)

        #pragma config CPUDIV = NOCLKDIV, USBDIV = OFF                          //CONFIG1L
        #pragma config FOSC = HS, PLLEN=ON, FCMEN = OFF, IESO = OFF             //CONFIG1H
		#pragma config PWRTEN = OFF , BOREN = ON, BORV = 30 //, VREGEN = ON     //CONFIG2L
        #pragma config WDTEN = ON, WDTPS = 32768                                //CONFIG2H
        #pragma config MCLRE = OFF, HFOFST = OFF                                //CONFIG3H
        #pragma config STVREN = ON, LVP = OFF, XINST = OFF, BBSIZ=OFF           //CONFIG4L
        #pragma config CP0 = OFF, CP1 = OFF                                     //CONFIG5L
        #pragma config CPB = OFF                                    		    //CONFIG5H
        #pragma config WRT0 = OFF, WRT1 = OFF                                   //CONFIG6L
        //Disabled WRTB for debugging.  Reenable for real.
 		#pragma config WRTB = ON, WRTC = OFF                                    //CONFIG6H
		//#pragma config WRTB = OFF, WRTC = OFF                                 //CONFIG6H
        #pragma config EBTR0 = OFF, EBTR1 = OFF                                 //CONFIG7L
        #pragma config EBTRB = OFF                                              //CONFIG7H
                
        #ifdef __DEBUG
        //#pragma config BKBUG = ON 
        #endif
        #ifndef __DEBUG
            //#pragma config BKBUG = OFF
        #endif   
#elif defined(PIC18F45K50_MICRO)                                                //Configuration bits for PIC18F45K50
    //CONFIG1L 
    #pragma config PLLSEL   = PLL3X                                             //PLL Selection (4x clock multiplier) 
    #pragma config CFGPLLEN = ON                                                //PLL Enable Configuration bit (PLL Disabled (firmware controlled)) 
    #pragma config CPUDIV   = NOCLKDIV                                          //CPU System Clock Postscaler (CPU uses system clock (no divide)) 
    #pragma config LS48MHZ  = SYS48X8                                           //Low Speed USB mode with 48 MHz system clock (System clock at 24 MHz, USB clock divider is set to 4) 
    //CONFIG1H 
    #pragma config FOSC     = HSH                                               //Oscillator Selection (USE_INTERNAL_16MHZ_OSCILLATOR for Internal oscillator FOSC = INTOSCIO)
    #pragma config PCLKEN   = OFF                                               //Primary Oscillator Shutdown (Primary oscillator shutdown firmware controlled) 
    #pragma config FCMEN    = OFF                                               //Fail-Safe Clock Monitor (Fail-Safe Clock Monitor disabled) 
    #pragma config IESO     = OFF                                               //Internal/External Oscillator Switchover (Oscillator Switchover mode disabled) 
    //CONFIG2L 
    #pragma config nPWRTEN  = OFF                                               //Power-up Timer Enable (Power up timer disabled) 
    #pragma config BOREN    = OFF                                               //Brown-out Reset Enable (BOR disabled in hardware (SBOREN is ignored)) 
    #pragma config BORV     = 190                                               //Brown-out Reset Voltage (BOR set to 1.9V nominal) 
    #pragma config nLPBOR   = OFF                                               //Low-Power Brown-out Reset (Low-Power Brown-out Reset disabled) 
    // CONFIG2H 
    #pragma config WDTEN    = ON                                                //Watchdog Timer Enable bits (WDT disabled in hardware (SWDTEN ignored)) 
    #pragma config WDTPS    = 32768                                             //Watchdog Timer Postscaler (1:32768) 
    // CONFIG3H 
    #pragma config CCP2MX   = RC1                                               //CCP2 MUX bit (CCP2 input/output is multiplexed with RC1) 
    #pragma config PBADEN   = OFF                                               //PORTB A/D Enable bit (PORTB<5:0> pins are configured as analog input channels on Reset) 
    #pragma config T3CMX    = RC0                                               //Timer3 Clock Input MUX bit (T3CKI function is on RC0) 
    #pragma config SDOMX    = RB3                                               //SDO Output MUX bit (SDO function is on RB3) 
    #pragma config MCLRE    = ON                                                //Master Clear Reset Pin Enable (MCLR pin enabled; RE3 input disabled) 

    // CONFIG4L 
    #pragma config STVREN   = ON                                                //Stack Full/Underflow Reset (Stack full/underflow will cause Reset) 
    #pragma config LVP      = OFF                                               //Single-Supply ICSP Enable bit (Single-Supply ICSP enabled if MCLRE is also 1) 
    #pragma config ICPRT    = OFF                                               //Dedicated In-Circuit Debug/Programming Port Enable (ICPORT disabled) 
    #pragma config XINST    = OFF                                               //Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled) 
    // CONFIG5L 
    #pragma config CP0      = ON                                                //Block 0 Code Protect (Block 0 is not code-protected) 
    #pragma config CP1      = ON                                                //Block 1 Code Protect (Block 1 is not code-protected) 
    #pragma config CP2      = ON                                                //Block 2 Code Protect (Block 2 is not code-protected) 
    #pragma config CP3      = ON                                                //Block 3 Code Protect (Block 3 is not code-protected) 
    // CONFIG5H 
    #pragma config CPB      = ON                                                //Boot Block Code Protect (Boot block is not code-protected) 
    #pragma config CPD      = OFF                                               //Data EEPROM Code Protect (Data EEPROM is not code-protected) 
    // CONFIG6L 
    #pragma config WRT0     = OFF                                               //Block 0 Write Protect (Block 0 (0800-1FFFh) is not write-protected) 
    #pragma config WRT1     = OFF                                               //Block 1 Write Protect (Block 1 (2000-3FFFh) is not write-protected) 
    #pragma config WRT2     = OFF                                               //Block 2 Write Protect (Block 2 (04000-5FFFh) is not write-protected) 
    #pragma config WRT3     = OFF                                               //Block 3 Write Protect (Block 3 (06000-7FFFh) is not write-protected) 
    // CONFIG6H 
    #pragma config WRTC     = OFF                                               //Configuration Registers Write Protect (Configuration registers (300000-3000FFh) are not write-protected) 
    #pragma config WRTB     = OFF                                               //Boot Block Write Protect (Boot block (0000-7FFh) is not write-protected) 
    #pragma config WRTD     = OFF                                               //Data EEPROM Write Protect (Data EEPROM is not write-protected) 
    // CONFIG7L 
    #pragma config EBTR0    = OFF                                               //Block 0 Table Read Protect (Block 0 is not protected from table reads executed in other blocks) 
    #pragma config EBTR1    = OFF                                               //Block 1 Table Read Protect (Block 1 is not protected from table reads executed in other blocks) 
    #pragma config EBTR2    = OFF                                               //Block 2 Table Read Protect (Block 2 is not protected from table reads executed in other blocks) 
    #pragma config EBTR3    = OFF                                               //Block 3 Table Read Protect (Block 3 is not protected from table reads executed in other blocks) 
    // CONFIG7H 
    #pragma config EBTRB    = OFF                                               //Boot Block Table Read Protect (Boot block is not protected from table reads executed in other blocks) 
#else
    #error No hardware board defined, see "Microprocessor_Selector.h" and __FILE__
#endif

#endif

/** VARIABLES ******************************************************/
#pragma udata
BYTE old_Bootloader_sw,old_sw3;
BOOL emulate_mode;
BYTE movement_length;
BYTE vector = 0;

#if defined(__18F14K50) || defined(__18F13K50) || defined(__18LF14K50) || defined(__18LF13K50) 
    #pragma udata usbram2
#elif defined(__18F2455) || defined(__18F2550) || defined(__18F4455) || defined(__18F4550)\
    || defined(__18F4450) || defined(__18F2450)\
    || defined(__18F2458) || defined(__18F2453) || defined(__18F4558) || defined(__18F4553)
    #pragma udata USB_VARIABLES=0x500
#else
    #pragma udata
#endif

unsigned char ReceivedDataBuffer[64];                                           //Always keep this declarations here on this position!
unsigned char ToSendDataBuffer[64];                                             //Always keep this declarations here on this position!
#pragma udata
USB_HANDLE USBOutHandle = 0;                                                    //Always keep this declarations here on this position!
USB_HANDLE USBInHandle = 0;                                                     //Always keep this declarations here on this position!
BOOL blinkStatusValid = TRUE;                                                   //Always keep this declarations here on this position!
BOOL ReplyUSB = FALSE;                                                          //Always keep this declarations here on this position!
BOOL NoReplyRequired = FALSE;                                                   //Always keep this declarations here on this position!

/** PRIVATE PROTOTYPES *********************************************/
// BlinkUSBStatus(void);
BOOL Bootloader_sw_IsPressed(void);                                             //Always keep this declarations here on this position!
BOOL Switch3IsPressed(void);                                                    //Always keep this declarations here on this position!
static void InitializeSystem(void);                                             //Always keep this declarations here on this position!

/** VECTOR REMAPPING ***********************************************/
#if defined(__18CXX)
	//On PIC18 devices, addresses 0x00, 0x08, and 0x18 are used for
	//the reset, high priority interrupt, and low priority interrupt
	//vectors.  However, the current Microchip USB bootloader 
	//examples are intended to occupy addresses 0x00-0x7FF or
	//0x00-0xFFF depending on which bootloader is used.  Therefore,
	//the bootloader code remap these vectors to new locations
	//as indicated below.  This remapping is only necessary if you
	//wish to program the hex file generated from this project with
	//the USB bootloader.  If no bootloader is used, edit the
	//usb_config.h file and comment out the following defines:
	
	//#define PROGRAMMABLE_WITH_USB_HID_BOOTLOADER
	//#define PROGRAMMABLE_WITH_USB_LEGACY_CUSTOM_CLASS_BOOTLOADER
	
	#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)
		#define REMAPPED_RESET_VECTOR_ADDRESS                           0x1400
		#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS                  0x1408
		#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS                   0x1418
	#elif defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)	
		#define REMAPPED_RESET_VECTOR_ADDRESS                           0x800
		#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS                  0x808
		#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS                   0x818
	#else	
		#define REMAPPED_RESET_VECTOR_ADDRESS                           0x00
		#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS                  0x08
		#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS                   0x18
	#endif
	
	#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)||defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
	extern void _startup (void);                                                //See c018i.c in C18 compiler dir
	#pragma code REMAPPED_RESET_VECTOR = REMAPPED_RESET_VECTOR_ADDRESS
	void _reset (void)
	{
	    _asm goto _startup _endasm
	}
	#endif
	#pragma code REMAPPED_HIGH_INTERRUPT_VECTOR = REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS
	void Remapped_High_ISR (void)
	{
	     _asm goto YourHighPriorityISRCode _endasm
	}
	#pragma code REMAPPED_LOW_INTERRUPT_VECTOR = REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS
	void Remapped_Low_ISR (void)
	{
	     _asm goto YourLowPriorityISRCode _endasm
	}
	
	#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)||defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
	//Note: If this project is built while one of the bootloaders has
	//been defined, but then the output hex file is not programmed with
	//the bootloader, addresses 0x08 and 0x18 would end up programmed with 0xFFFF.
	//As a result, if an actual interrupt was enabled and occured, the PC would jump
	//to 0x08 (or 0x18) and would begin executing "0xFFFF" (unprogrammed space).  This
	//executes as nop instructions, but the PC would eventually reach the REMAPPED_RESET_VECTOR_ADDRESS
	//(0x1000 or 0x800, depending upon bootloader), and would execute the "goto _startup".  This
	//would effective reset the application.
	
	//To fix this situation, we should always deliberately place a 
	//"goto REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS" at address 0x08, and a
	//"goto REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS" at address 0x18.  When the output
	//hex file of this project is programmed with the bootloader, these sections do not
	//get bootloaded (as they overlap the bootloader space).  If the output hex file is not
	//programmed using the bootloader, then the below goto instructions do get programmed,
	//and the hex file still works like normal.  The below section is only required to fix this
	//scenario.
	#pragma code HIGH_INTERRUPT_VECTOR = 0x08
	void High_ISR (void)
	{
	     _asm goto REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS _endasm
	}
	#pragma code LOW_INTERRUPT_VECTOR = 0x18
	void Low_ISR (void)
	{
	     _asm goto REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS _endasm
	}
	#endif

	#pragma code
		
	//These are your actual interrupt handling routines.
	#pragma interrupt YourHighPriorityISRCode

    
    void YourHighPriorityISRCode()
    {
  
  
        while((INTCON3bits.INT2IF == 1) || (U1IR))
        {
            //mLED_Alarm_Toggle();  
            if((USBDeviceState != CONFIGURED_STATE) && (PORTBbits.RB2 == 1))
            {
                while((USBDeviceState != CONFIGURED_STATE) && (PORTBbits.RB2 == 1))
                    {
                        //Do this while enumeration is not complete yet
                        USB_task();
                        //mLED_Alarm_On();
                        
                    }
            }
            else
            {
                USB_task();
                //mLED_Alarm_Toggle();         
            }
            U1IR = 0;
            INTCON3bits.INT2IF = 0;
           
        }
        
        /*TODO .. maybe reset?*/
           return;
           
    
    }
//	void YourHighPriorityISRCode_BULKY()
//	{
//                                                                                //GIE is automatically cleared to disable interrupts.
//        //if ((INTCON3bits.INT2IF == 1)  &&  (INTCON3bits.INT2IE == 1))                                         //If External INT2 bit is set
//        {
//            INTCON3bits.INT2IF = 0;
//            //Flag.USB_CON = 1;
//            //mLED_Alarm_Toggle();
//            //T0CONbits.TMR0ON = 0;                                               //Disable Timer 0 to prevent having interrupts on Timer 0 during USB communication
//            //return;
//            
//            Delay_Boot();
//            
//                                                              //Front LED is (and stays) GREEN
//            //while(PORTBbits.INT2 == 1)                                          //While USB is attached
//            {
//                ClRWdT_reset();
//                if(U1IR){
//                    //U1IR=0;
//                    //mLED_Alarm_Toggle(); 
//                }
//                //OSCCONbits.SCS0 = 0;                                          //Switch on External (Primary) Oscillator (1 of 2)
//                //OSCCONbits.SCS1 = 0;                                          //Switch on External (Primary) Oscillator (2 of 2)
//
//                #if defined(USB_FIRMWARE_UPGRADE)
//                    USBDeviceTasks();                                           //Interrupt or polling method.  This function needs to be called every <100ms to prevend "Unknown Device" to the PC!
//                #if defined(USB_FIRMWARE_UPGRADE) || defined (USB_LOCON_FUNCTIONS)
//                    ProcessIO();                                                //Check the USB request from Master (LoCon action or Firmware upgrade mode
//                #endif
//                #if defined(USB_LOCON_FUNCTIONS)
//                    mLED_Alarm_Toggle();
//                    ExecuteLoConDataToUSB();                                    //Proceed in USB-LoCon functions such as control by USB an LoCon device and reply to USB
//                #endif
//
//            }
//        }
//        //else
//        {
//            Nop();                                                              //Wrong interrupt has occurred
//        }
//        INTCON3bits.INT2IF = 0;                                                 //Clear External INT2 Flag (before re enabling interrupts)
//        return;                                                                      //GIE is automatically Re-enabled
//        USBModuleDisable();
//        USBDeviceState = DETACHED_STATE;
//        Reset();
//}                                                                               //This return will be a "retfie fast", since this is in a #pragma interrupt section
	#pragma interruptlow YourLowPriorityISRCode
	void YourLowPriorityISRCode()
	{
            if ((INTCONbits.TMR0IF == 1) && (INTCONbits.TMR0IE == 1))                                        //Check if Timer 0 Expired
            {
                  handleTMR0Interrupt();                                        //Handle the action when Timer 0 expired
            }
            INTCONbits.TMR0IF = 0;  //abc clear the timer overflow interrupt
	}                                                                           //This return will be a "retfie", since this is in a #pragma interrupt low section 
#endif                                                                          //of "#if defined(__18CXX)"

/** DECLARATIONS ***************************************************/
#pragma code

/********************************************************************
 * Function:        void main(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        Main program entry point.
 * Note:            None
 *******************************************************************/
    
//#pragma config FOSC=HS          //CONFIG1H
    

unsigned int cnt,cnt2;
    
void main(void)
{
    #if defined(USE_INTERNAL_16MHZ_OSCILLATOR)
        #if defined(__18F45K50)
            OSCTUNE = 0x80;                                                     //3X PLL ratio mode selected
            OSCCON = 0x04;//0x70;                                               //Switch to 16MHz HFINTOSC
            OSCCON2 = 0x14;//0x10;                                              //Enable PLL, SOSC, PRI OSC drivers turned off
            while(OSCCON2bits.PLLRDY != 1);                                     //Wait for PLL lock
            ACTCON = 0x00;//0x90;                                               //Enable active clock tuning for USB operation
        #endif    
    #endif
    InitializeSystem();

    #if defined(USE_LOCON_FUNCTIONS)
        NrOfReceivedLoConBytes = -1;
        ClearLoConTransmitBuffers();                                            //Clear all LoCon transmit buffers to prevent wrong data later on.
        ClearLoConReceiveBuffers();
    #endif
    if (PORTBbits.RB2 == 1)                                                     //Check I/O RB2 if USB Attached, if so, activate the interrupt!
    {
        INTCON3bits.INT2IF = 1;                                                  //Set External INT2 Flag. RETRIGGER USB because Bootloader is still USB connected so retrigger is needed.
    }
 
        
    #if defined(USB_INTERRUPT)
        USBDeviceAttach();
    #endif
/*  OSCCONbits.IDLEN = 1;                                                       //Device enters Idle mode on sleep instruction
    OSCCONbits.IRCF0 = 1;                                                       //Set clock frequency to 8MHz (1 of 3)
    OSCCONbits.IRCF1 = 1;                                                       //Set clock frequency to 8MHz (2 of 3)
    OSCCONbits.IRCF2 = 1;                                                       //Set clock frequency to 8MHz (3 of 3)
*/
       
    while(1)                                                                    //Infinite loop to check LoCon and USB buffers
    { 

                
        ClRWdT_reset();

        //Sleep();                                                              //Disabled because of consciousness mode is now activated

        #if defined(USE_ADC_FUNCTIONS)
        RFLevelAlarm = CheckRFLevel();                                          //Check if the RF alarm is given or not given
        if (RFLevelAlarm == 1)                                                  //Only switch alarm LED to Red. If RED the RF Alarm LED can only be reset using the LoCon Command to check for Errors (0xFF). If Alarm gone, LED is green again!
        {
            mLED_Alarm_On();
        }
        if (RFLevelAlarm == 0)
        {
            mLED_Alarm_Off();
        }
        #if defined(USE_LOCON_FUNCTIONS)                                        //WARNING: LoCon is now on Polling base, if there are issues in missing data, then change to Low Priority interrupt!
        if (LoConRX == 1)                                                       //Check if there is LoCon data received on the LoCon RX port of the DBC-1200 LITE-Amplifier
        {
            ReplyDestination = DESTINATION_LOCON;
            ReceiveLoConFunction();                                             //If there is data detected, then proceed in receiving the LoCon Data
        }
        #endif
        #endif
        #if defined(USE_LOCON_FUNCTIONS)                                        //WARNING: LoCon is now on Polling base, if there are issues in missing data, then change to Low Priority interrupt!
        if (LoConRX == 1)                                                       //Check if there is LoCon data received on the LoCon RX port of the DBC-1200 LITE-Amplifier
        {
            ReplyDestination = DESTINATION_LOCON;
            ReceiveLoConFunction();                                             //If there is data detected, then proceed in receiving the LoCon Data
        }
        #endif
        #if (defined(USB_FIRMWARE_UPGRADE) || defined(USB_LOCON_FUNCTIONS)) && !defined(USE_INTERRUPT)
            USBDeviceTasks();                                                   //Interrupt or polling method.  This function needs to be called every <100ms to prevend "Unknown Device" to the PC!
        #endif
        #if (defined(USB_FIRMWARE_UPGRADE) || defined(USB_LOCON_FUNCTIONS)) && !defined(USE_INTERRUPT)
            ProcessIO();                                                        //Check the USB request from Master (LoCon action or Firmware upgrade mode
        #endif
        #if defined(USB_LOCON_FUNCTIONS)
            ExecuteLoConDataToUSB();                                            //Proceed in USB-LoCon functions such as control by USB an LoCon device and reply to USB
        #endif
        T0CONbits.TMR0ON = 1;                                                   //This bit is set to high (1) to enable the Timer0 and set to low (0) to stop it.  

        if(Flag.ATC_TIMING == 1)
        {
            Flag.ATC_TIMING = 0;
            ATC_task();
        }
        
        USB_Valid_Check();
        
    }//end while
}//end main

void ATC_task( void )
{

    if (ATC_Enabled_Disabled == 1)
    {     
        //TEMP_CalculateTemperature(); 
         mDebug_Toggle();
         ATC_UpdateAllOffsets(DS_MOD_CHANGE_WRITE, DS_OFF_TEMP_UPDATE, DS_ATC_STEPPING);  
    }
    ControlAllSwitchesPeriodically();
}


void USB_task( void )
{

    ClRWdT_reset();
    USBDeviceTasks();                                           //Interrupt or polling method.  This function needs to be called every <100ms to prevend "Unknown Device" to the PC!
    ProcessIO();                                                //Check the USB request from Master (LoCon action or Firmware upgrade mode

}

void USB_Valid_Check() /*To check if USB has been disconnected, if so? Reset please!*/
{
    USB_PHY.current = PORTBbits.RB2;
    
        if(USB_PHY.current != USB_PHY.last)
        {
            if(count >= 2500) // Create some filtering, has to be connected/disconnected for a while, to prevent spikes being seen as connected/sicsconnected
            {
                USB_PHY.last = USB_PHY.current;
                if(USB_PHY.current == 1)
                {
                    set_ATC_update_fast();
                }
                if(USB_PHY.current == 0)
                {
                    //set_ATC_update_slow();
                    Reset();
                }
            }
            else
            {
                count++;
            }
        }
        else
        {
            count = 0;
        }
}
/********************************************************************
 * Function:        static void InitializeSystem(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        InitializeSystem is a centralize initialization
 *                  routine. All required USB initialization routines
 *                  are called from here.
 *                  User application initialization routine should
 *                  also be called from here.                  
 * Note:            None
 *******************************************************************/
static void InitializeSystem(void)
{
    WDTCONbits.SWDTEN = 1;                                                      //Enable Watchdog Timer
    #if (defined(__18CXX) & !defined(PIC18F87J50_PIM))
        //ADCON1 |= 0x0F;                                                       //Default all pins to digital
    #endif
//	The USB specifications require that USB peripheral devices must never source
//	current onto the Vbus pin.  Additionally, USB peripherals should not source
//	current on D+ or D- when the host/hub is not actively powering the Vbus line.
//	When designing a self powered (as opposed to bus powered) USB peripheral
//	device, the firmware should make sure not to turn on the USB module and D+
//	or D- pull up resistor unless Vbus is actively powered.  Therefore, the
//	firmware needs some means to detect when Vbus is being powered by the host.
//	A 5V tolerant I/O pin can be connected to Vbus (through a resistor), and
// 	can be used to detect when Vbus is high (host actively powering), or low
//	(host is shut down or otherwise not supplying power).  The USB firmware
// 	can then periodically poll this I/O pin to know when it is okay to turn on
//	the USB module/D+/D- pull up resistor.  When designing a purely bus powered
//	peripheral device, it is not possible to source current on D+ or D- when the
//	host is not actively providing power on Vbus. Therefore, implementing this
//	bus sense feature is optional.  This firmware can be made to use this bus
//	sense feature by making sure "USE_USB_BUS_SENSE_IO" has been defined in the
//	HardwareProfile.h file.    
    #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = TRISAbits.TRISA1;                                      //See Pin description PIC18F14K50.h
    #endif
//	If the host PC sends a GetStatus (device) request, the firmware must respond
//	and let the host know if the USB peripheral device is currently bus powered
//	or self powered.  See chapter 9 in the official USB specifications for details
//	regarding this request.  If the peripheral device is capable of being both
//	self and bus powered, it should not return a hard coded value for this request.
//	Instead, firmware should check if it is currently self or bus powered, and
//	respond accordingly.  If the hardware has been configured like demonstrated
//	on the PICDEM FS USB Demo Board, an I/O pin can be polled to determine the
//	currently selected power source.  On the PICDEM FS USB Demo Board, "RA2" 
//	is used for	this purpose.  If using this feature, make sure "USE_SELF_POWER_SENSE_IO"
//	has been defined in HardwareProfile.h, and that an appropriate I/O pin has been mapped
//	to it in HardwareProfile.h.
    #if defined(USE_SELF_POWER_SENSE_IO)
    #define tris_self_power = TRISAbits.TRISA2;                                 //See Pin description PIC18F14K50.h
    #endif
    BoardInit();
    USBDelay(10000);                                                            //Delay to prevent unknown situation outside the Microprocessor (communication settling)
    UserInit();                                                                 //EEPROM initialization, Setting up attenuators and Initialize Interrupts, ADC, LoCon RX/TX etc

                                                                                //Initialize the variable holding the handle for the last
    //transmission
    USBOutHandle = 0;
    USBInHandle = 0;
    blinkStatusValid = TRUE;
    
    USBDeviceInit();                                                            //usb_device.c.  Initializes USB module SFRs and firmware variables to known states.    

    InterruptInit();                                                             //Activate Interrupts
    #if !defined(USE_INTERRUPT)
        mInitUSBAttach();
    #endif
}//end InitializeSystem

/********************************************************************
 * Function:        void ReadEEPROMSettings(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        InitializeSystem EEPROM from factory default setting
 *                  to read all settings at power up state. This is always
 *                  a function to process an initialization.
 * Note:            None
 *******************************************************************/
void ReadEEPROMSettings(void)
{
   int BootloaderFlag;

    BootloaderFlag = ReadEEPROM(EE_ADR_BOOTLOADER_FLAG);
    if (BootloaderFlag == 0xFF)
    {
        BootloaderFlag = 0x00;                                                  //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_BOOTLOADER_FLAG, BootloaderFlag);
    }
    #if !defined(USE_FIXED_LOCON_ADDRESS)
        OwnLoConAddress = ReadEEPROM(EE_ADR_OWNLOCONADDRESS);
        if (OwnLoConAddress == 0xFF)
        {
            OwnLoConAddress = 0xA0;                                             //Setting to 0 default, EEPROM is not set yet
            WriteEEPROM(EE_ADR_OWNLOCONADDRESS, OwnLoConAddress);            
        }
    #endif
    #if defined(USE_FIXED_LOCON_ADDRESS)
    {
        OwnLoConAddress = DEFAULT_FIXED_LOCON_ADDRESS;
    }
    #endif
    ModifiedBit = ReadEEPROM(EE_ADR_MODIFIED_BIT);
    if (ModifiedBit == 0xFF)
    {
        ModifiedBit = 0x00;                                                     //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_MODIFIED_BIT, ModifiedBit);            
    }
    
    
    Info.SET.att_us = ReadEEPROM(EE_ADR_UPSTREAM_ATTN_SETTING);
    if (Info.SET.att_us == 0xFF)
    {
        Info.SET.att_us = 0x00;                                                    //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_UPSTREAM_ATTN_SETTING , Info.SET.att_us);
    }
    
    
    Info.SET.att_pre = ReadEEPROM(EE_ADR_PRE_ATTN_MOD_1_SETTING);
    if (Info.SET.att_pre == 0xFF)
    {
        Info.SET.att_pre = PRE_ATTENUATOR_STARTUP_MODULE_1;                                                   //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_PRE_ATTN_MOD_1_SETTING , Info.SET.att_pre); 
    } 
    
    
    Info.SET.att_int = ReadEEPROM(EE_ADR_INTERSTAGE_ATTN_MOD_1_SETTING);
    if (Info.SET.att_int == 0xFF)
    {
        Info.SET.att_int = INTERSTAGE_ATTENUATOR_STARTUP_MODULE_1;                                            //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_INTERSTAGE_ATTN_MOD_1_SETTING , Info.SET.att_int);
    }
    
    
    Info.SET.eq_us= ReadEEPROM(EE_ADR_UPSTREAM_EQU_SETTING);
    if (Info.SET.eq_us == 0xFF)
    {
        Info.SET.eq_us = 0x00;                                                    //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_UPSTREAM_EQU_SETTING , Info.SET.eq_us);
    }
        
        
    Info.SET.eq_pre = ReadEEPROM(EE_ADR_PRE_EQU_MOD_1_SETTING);
    if (Info.SET.eq_pre == 0xFF)
    { 
        Info.SET.eq_pre = 0x00;                                                   //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_PRE_EQU_MOD_1_SETTING , Info.SET.eq_pre);
    }
    
    
    Info.SET.eq_int = ReadEEPROM(EE_ADR_INTERSTAGE_EQU_MOD_1_SETTING);
    if (Info.SET.eq_int == 0xFF)
    {
        Info.SET.eq_int = 0x00;                                            //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_INTERSTAGE_EQU_MOD_1_SETTING , Info.SET.eq_int);
    }
    
   
    Info.SET.pwr_sw = ReadEEPROM(EE_ADR_DOWNSTREAM_POWER_SW_MOD_1_SETTING );
    if (Info.SET.pwr_sw == 0xFF)
    {
        Info.SET.pwr_sw = 0x00;                                       //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_DOWNSTREAM_POWER_SW_MOD_1_SETTING  , Info.SET.pwr_sw);
    }
    
    
    Aux_1_Value = ReadEEPROM(EE_ADR_AUX_1_VALUE);
    if (Aux_1_Value == 0xFF)
    {
        Aux_1_Value = 0x00;                                                     //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_AUX_1_VALUE  , Aux_1_Value);
    }
    Amplifier_ID_BLL = ReadEEPROM(EE_ADR_TYPE_ID_AMPLIFIER);
    if (Amplifier_ID_BLL == 0xFF)
    {
        Amplifier_ID_BLL = 0x00;                                                //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_TYPE_ID_AMPLIFIER  , Amplifier_ID_BLL);
    }
    Setting_0_5dB_Steps = ReadEEPROM(EE_ADR_SETTING_0_5_STEPS_ENABLED);
    if (Setting_0_5dB_Steps == 0xFF)
    {
        Setting_0_5dB_Steps = 0x00;                                             //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_SETTING_0_5_STEPS_ENABLED  , Setting_0_5dB_Steps);
    }
    Setting_0_5dB_Steps_Time_Delay = ReadEEPROM(EE_ADR_SETTING_0_5_STEPS_TIME_DELAY_MS);
    if (Setting_0_5dB_Steps_Time_Delay == 0xFF)
    {
        Setting_0_5dB_Steps_Time_Delay = 0x0F;                                  //Setting to 15ms default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_SETTING_0_5_STEPS_TIME_DELAY_MS   , Setting_0_5dB_Steps_Time_Delay);
    }

    EQ_Hinge_Downstream_1 = ReadEEPROM(EE_ADR_SETTING_EQ_HINGE_DS1);
    if (EQ_Hinge_Downstream_1 == 0xFF)
    {
        EQ_Hinge_Downstream_1 = 0x00;                                           //Setting to 0 default, EEPROM is not set yet
        WriteEEPROM(EE_ADR_SETTING_EQ_HINGE_DS1  , EQ_Hinge_Downstream_1);
    }
    ATC_Enabled_Disabled = ReadEEPROM(EE_ADR_SETTING_ATC_ENABLED_DISABLED);
    if (ATC_Enabled_Disabled == 0xFF)
    {
        ATC_Enabled_Disabled = 0x01;                                            //Setting to 1 default, ATC is default Enabled
        WriteEEPROM(EE_ADR_SETTING_ATC_ENABLED_DISABLED  , ATC_Enabled_Disabled);
    }
}

void Delay_Boot(void)                                                           //Delay needed in Boot-up
{
    int j = 500;
    int	k = 10;
    
    while(j>0 && k != 0)
    {
    	j--;
    	if(j == 1)
        {
            k--;
            j = 1000;
    	}
    }
}

void Delay_Boot_Short(void)                                                     //Delay needed in Boot-up
{
    int j = 100;
    int	k = 3;
    
    while(j>0 && k != 0)
    {
    	j--;
    	if(j == 1)
    	{
            k--;
            j = 100;
    	}
    }
}

void ControlAllSwitchesPeriodically(void)
{

    
    int tmp_att_pre = 0;
    int tmp_att_int = 0;

     
    if((ATC_Enabled_Disabled == 1)&&(ATC.State  == ATC_OK))
    {
    tmp_att_pre = DecimalToSameHex((HEXToSameDecimal(Info.SET.att_pre)+OF.Sw[PRE_ATT])); //Workaround BCD to HEX (eeprom is BCD, offset is not BCD)
    tmp_att_int = DecimalToSameHex((HEXToSameDecimal(Info.SET.att_int)+OF.Sw[INT_ATT])); //Workaround BCD to HEX (eeprom is BCD, offset is not BCD)
    }   
    else
    {
    tmp_att_pre = Info.SET.att_pre;
    tmp_att_int = Info.SET.att_int; 
    }
    
    
    HandleCmdWriteDownstreamPowerSwMod1(Info.SET.pwr_sw, BOOT_UP);
    HandleCmdWritePreAttnMod1(tmp_att_pre, BOOT_UP);
    HandleCmdWriteInterstageAttnMod1(tmp_att_int, BOOT_UP);
    HandleCmdWriteUpstreamAttn(Info.SET.att_us, BOOT_UP);
    HandleCmdWritePreEquMod1(Info.SET.eq_pre, BOOT_UP);
    HandleCmdWriteInterstageEquMod1(Info.SET.eq_int, BOOT_UP);
    HandleCmdWriteUpstreamEqu(Info.SET.eq_us, BOOT_UP);
    
    StoreOldSetStepping();

    //WriteSerialData(DEFAULT_ATTENUATION_NO_MODULE,NO_MODULE_SELECTED);        //Flush Serial register to 6dB all Peregrine settings (0x0C)
}

void ControlAllSwitches(void)                                                   //Set all switches to the EEPROM setting
{
    int tmp_att_pre = Info.SET.att_pre;
    int tmp_att_int = Info.SET.att_int;
    
    Delay_Boot();
    Delay_Boot();    
     
    if((ATC_Enabled_Disabled == 1) && (ATC.State == ATC_OK)) // apply immediate ATC correction if needed.
    {
        tmp_att_pre = DecimalToSameHex((HEXToSameDecimal(tmp_att_pre)+OF.Sw[PRE_ATT]));     //Workaround BCD to HEX (eeprom is BCD, offset is not BCD)
        tmp_att_int = DecimalToSameHex((HEXToSameDecimal(tmp_att_int)+OF.Sw[INT_ATT]));     //Workaround BCD to HEX (eeprom is BCD, offset is not BCD)
    }

    HandleCmdWriteDownstreamPowerSwMod1(Info.SET.pwr_sw, BOOT_UP); 
    
    if (Info.SET.att_pre < PRE_ATTENUATOR_STARTUP_MODULE_1)
    {
        WriteSerialData(PRE_ATTENUATOR_STARTUP_MODULE_1,PRE_ATTENUATOR_MODULE_1);                         
        Info.SET.att_pre = PRE_ATTENUATOR_STARTUP_MODULE_1;
        HandleCmdWritePreAttnMod1(Info.SET.att_pre, BOOT_UP);                     
    }
    else  
    {
        HandleCmdWritePreAttnMod1(tmp_att_pre, BOOT_UP);
    }

    if (Info.SET.att_int < INTERSTAGE_ATTENUATOR_STARTUP_MODULE_1)
    {
        WriteSerialData(INTERSTAGE_ATTENUATOR_STARTUP_MODULE_1,INTERSTAGE_ATTENUATOR_MODULE_1);                  
        Info.SET.att_int = INTERSTAGE_ATTENUATOR_STARTUP_MODULE_1;
        HandleCmdWriteInterstageAttnMod1(Info.SET.att_int, BOOT_UP);
    }
    else
    {
        HandleCmdWriteInterstageAttnMod1(tmp_att_int, BOOT_UP);
    }
    

    if (Info.SET.att_us < UPSTREAM_ATTENUATOR_STARTUP)
    {
        WriteSerialData(UPSTREAM_ATTENUATOR_STARTUP,UPSTREAM_ATTENUATOR);                           
        Info.SET.att_us = UPSTREAM_ATTENUATOR_STARTUP;
        HandleCmdWriteUpstreamAttn(Info.SET.att_us, BOOT_UP);
    }
    else
    {
        HandleCmdWriteUpstreamAttn(Info.SET.att_us, BOOT_UP);
    }    
    
    
    if (Info.SET.eq_us == 0x00)
    {
        WriteSerialData(0x01,UPSTREAM_EQUALIZER);                               //Switch to 0.5dB
        Delay_Boot_Short();
        HandleCmdWriteUpstreamEqu(Info.SET.eq_us, BOOT_UP);
    }
    else
    {
        HandleCmdWriteUpstreamEqu(Info.SET.eq_us, BOOT_UP);
    }

    if (Info.SET.eq_pre == 0x00)
    {
        WriteSerialData(0x01,PRE_EQUALIZER_MODULE_1);                           //Switch to 0.5dB
        HandleCmdWritePreEquMod1(Info.SET.eq_pre, BOOT_UP);
    }
    else
    {
        HandleCmdWritePreEquMod1(Info.SET.eq_pre, BOOT_UP);
    }

    if (Info.SET.eq_int == 0x00)
    {
        WriteSerialData(0x01,INTERSTAGE_EQUALIZER_MODULE_1);                    //Switch to 0.5dB
        HandleCmdWriteInterstageEquMod1(Info.SET.eq_int, BOOT_UP);
    }
    else
    {
        HandleCmdWriteInterstageEquMod1(Info.SET.eq_int, BOOT_UP);
    }
    
    
    WriteSerialData(DEFAULT_ATTENUATION_NO_MODULE,NO_MODULE_SELECTED);          //Flush Serial register to 6dB all Peregrine settings (0x0C)
}

void MainboardIdentifier(void)
{
    /*
    if ((MBD_IDN2 == 0) && (MBD_IDN1 == 0))
    {
        MainboardVersion = DBC;                                                 //DBC Motherboard
    }
    else if ((MBD_IDN2 == 0) && (MBD_IDN1 == 1))
    {
        MainboardVersion = DBD;                                                 //DBD Motherboard
    }
    else if ((MBD_IDN2 == 1) && (MBD_IDN1 == 0))
    {
        MainboardVersion = UNKNOWN;                                             //UNKNOWN Motherboard
    }
    else if ((MBD_IDN2 == 1) && (MBD_IDN1 == 1))
    {
        MainboardVersion = UNKNOWN;                                             //UNKNOWN Motherboard
    }
    */
    //Hardware selection is disabled for DBC-1200 LITE Amplifier.
     MainboardVersion = DBC_1200_LITE_AMPLIFIER;
}


/******************************************************************************
 * Function:        void InitializeInterrupt(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        This routine should take care of all Interrupt requests by USB
 * Note:
 *****************************************************************************/
void InitializeInterrupt(void)
{
    Flag.ATC_TIMING = 0;
    Flag.USB_CON = 0;
    Flag.USB_TIMING = 0;
    INTCONbits.GIE = 0;                                                         //Disable all interrupts

    //USB Interrupt initialization
    INTCON2 = 0xF1;                                                             //Clear complete register to ensure all bits are zero.
    INTCON3 = 0x00;                                                             //Clear complete register to ensure all bits are zero.
    TRISBbits.TRISB2 = 1;                                                       //Set RB2(INT2) as input
    INTCON2bits.INTEDG2 = 1;                                                    //External Interrupt 2 on rising edge
    INTCON3bits.INT2IP = 1;                                                     //External INT2 High priority
    INTCON3bits.INT2IE = 1;                                                     //Enable External INT2
    INTCON3bits.INT2IF = 0;                                                     //Clear External INT2 Flag (before re enabling interrupts)
    
    INTCONbits.INT0IF = 0;                                                       //Clear INT0IF flag
    INTCON3bits.INT1IF = 0;                                                      //Clear INT1IF flag
    
    INTCONbits.INT0IE = 0;                                                       //Disable INT0 external interrupt
    INTCON3bits.INT1IE = 0;                                                      //Disable INT1 external interrupt
    
    RCONbits.IPEN = 0;          //Disable priority levels on interrupts

    //Timer interrupt initialization
    TimerInit();                                                                //Initialize Timer0 on Interrupt Base for RC consciousness mode 8MHz
}

/******************************************************************************
 * Function:        void InterruptInit(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        This routine should take care of all of the demo code
 *                  initialization that is required.
 * Note:
 *****************************************************************************/
void InterruptInit(void)
{
    #if defined(USE_INTERRUPT)
    InitializeInterrupt();                                                      //Initialize External Interrupt for USB Attach (needs to be called BEFORE Timer0 Init!)
    #endif
}

/******************************************************************************
 * Function:        void BoardInit(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        This routine should take care of all of Board init
 *                  initialization that is required.
 * Note:
 *****************************************************************************/
void BoardInit(void)
{
    //Initialize all of the LED pins
    mInitAllLEDs();
    mInitDebugPin();
    #if defined(USE_LOCON_FUNCTIONS)
    //Initialize LoCon TX
    mInitLoConTX();
    //Initialize LoCon RX
    mInitLoConRX();
    #endif
    //Initialize push button
    mInitBootloader_sw();
    //Initialize all Attenuators and Equalizers
    old_Bootloader_sw = Bootloader_sw;
	//old_sw3 = sw3;
    #if defined(USE_SERIAL_DATA_FUNCTIONS)
        Initialize_Serial_Interface();
    #endif
    //Initialize Downstream Power Mode Switch for Module 1
    mInitDownstreamPowerSwitchMod1();
    //Initialization of the Motherboard
    //mInitMotherboardIdentifier();                                             //Motherboard ID is set in Firmware, not on Motherboard

    //Initialize the I2C Bit bang bus
    #if defined (USE_I2C_FUNCTIONS)
    mInitI2CSCLOutput();                                                        //Initialize I2C clock as output and set high. (Idle bus)
    mInitI2CSDAOutput();                                                        //Initialize I2C data as output and set high.  (Idle bus)
    #endif
    //Initialize ADC control for reading Current status value
    #if defined (USE_ADC_FUNCTIONS)
        mInitCurrentMonitor();
    #endif
    //Set (to be sure) Alarm LED to green
    mLED_Alarm_Off();
}

/******************************************************************************
 * Function:        void UserInit(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        This routine should take care of all of the User Init
 *                  initialization that is required.
 * Note:            
 *****************************************************************************/
void UserInit(void)
{
    //Initialize Amplifier board first (needed in ControllAllSwitches)
    MainboardIdentifier();
    //Initialize registers for actual EEPROM settings
    ReadEEPROMSettings();
    //Set Register settings to all attached switches
    
    //Initialize ATC Functions
    ATC_Init();     
    
    #if defined(USE_SERIAL_DATA_FUNCTIONS)
        ControlAllSwitches();
    #endif

    USB_PHY.last = 0;
    USB_PHY.count = 0;
}//end UserInit


/******************************************************************************
 * Function:        BOOL Bootloader_sw_IsPressed(void)
 * PreCondition:    None
 * Input:           None
 * Output:          TRUE - pressed, FALSE - not pressed
 * Side Effects:    None
 * Overview:        Indicates if the switch is pressed.  
 * Note:            
 *****************************************************************************/
BOOL Bootloader_sw_IsPressed(void)
{
    if(Bootloader_sw != old_Bootloader_sw)
    {
        old_Bootloader_sw = Bootloader_sw;                                      //Save new value
        if(Bootloader_sw == 0)                                                  //If pressed
            return TRUE;                                                        //Was pressed
    }//end if
	 return FALSE;                                                              //Was not pressed
   
}//end Bootloader_sw_IsPressed

/******************************************************************************
 * Function:        BOOL Switch3IsPressed(void)
 * PreCondition:    None
 * Input:           None
 * Output:          TRUE - pressed, FALSE - not pressed
 * Side Effects:    None
 * Overview:        Indicates if the switch is pressed.  
 * Note:            
 *****************************************************************************/
/*BOOL Switch3IsPressed(void)
{
    if(sw3 != old_sw3)
    {
        old_sw3 = sw3;                                                          //Save new value
        if(sw3 == 0)                                                            //If pressed
            return TRUE;                                                        //Was pressed
    }//end if
    return FALSE;                                                               //Was not pressed
}//end Switch3IsPressed
*/ 


/******************************************************************************
 * Function:        int ReadCurrentMonitorADC(void)
 * PreCondition:    None
 * Input:           None
 * Output:          WORD_VAL - the 10-bit right justified Current Monitor value
 * Side Effects:    ADC buffer value updated
 * Overview:        This function reads the CurrentMonitorADC Value and leaves the value in the
 *                  ADC buffer register
 * Note:            None
 *****************************************************************************/
#if defined(USE_ADC_FUNCTIONS)
int ReadCurrentMonitorADC(void)
{
    WORD_VAL w;
    int ReturnValue;
    //mInitCurrentMonitor();

    ADCON0bits.GO = 1;                                                          //Start AD conversion
    while(ADCON0bits.NOT_DONE);                                                 //Wait for conversion
    w.v[0] = ADRESL;
    w.v[1] = ADRESH;

    ReturnValue = (w.v[1] * 256) + w.v[0];                                      //Max value w.v[1] = 0x03 w.v[0] = 0xFF
    return ReturnValue;                                                         //Value between 0..1023
}//end ReadCurrentMonitorADC
#endif

/********************************************************************
 * Function:        void BlinkUSBStatus(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        BlinkUSBStatus turns on and off LEDs 
 *                  corresponding to the USB device state.
 * Note:            mLED macros can be found in HardwareProfile.h
 *                  USBDeviceState is declared and updated in
 *                  usb_device.c.
 *******************************************************************/
/*
void BlinkUSBStatus(void)
{
    static WORD led_count=0;
    if(led_count == 0)led_count = 10000U;
    led_count--;
    #define mLED_Both_Off()         {mLED_1_Off();mLED_2_Off();}
    #define mLED_Both_On()          {mLED_1_On();mLED_2_On();}
    #define mLED_Only_1_On()        {mLED_1_On();mLED_2_Off();}
    #define mLED_Only_2_On()        {mLED_1_Off();mLED_2_On();}

    if(USBSuspendControl == 1)
    {
        if(led_count==0)
        {
            mLED_1_Toggle();
            if(mGetLED_1())
            {
                mLED_2_On();
            }
            else
            {
                mLED_2_Off();
            }
        }//end if
    }
    else
    {
        if(USBDeviceState == DETACHED_STATE)
        {
            mLED_Both_Off();
        }
        else if(USBDeviceState == ATTACHED_STATE)
        {
            mLED_Both_On();
        }
        else if(USBDeviceState == POWERED_STATE)
        {
            mLED_Only_1_On();
        }
        else if(USBDeviceState == DEFAULT_STATE)
        {
            mLED_Only_2_On();
        }
        else if(USBDeviceState == ADDRESS_STATE)
        {
            if(led_count == 0)
            {
                mLED_1_Toggle();
                mLED_2_Off();
            }//end if
        }
        else if(USBDeviceState == CONFIGURED_STATE)
        {
            if(led_count==0)
            {
                mLED_1_Toggle();
                if(mGetLED_1())
                {
                    mLED_2_Off();
                }
                else
                {
                    mLED_2_On();
                }
            }//end if
        }//end if(...)
    }//end if(UCONbits.SUSPND...)
}//end BlinkUSBStatus
*/

// ******************************************************************************************************
// ************** USB Callback Functions ****************************************************************
// ******************************************************************************************************
// The USB firmware stack will call the callback functions USBCBxxx() in response to certain USB related
// events.  For example, if the host PC is powering down, it will stop sending out Start of Frame (SOF)
// packets to your device.  In response to this, all USB devices are supposed to decrease their power
// consumption from the USB Vbus to <2.5mA each.  The USB module detects this condition (which according
// to the USB specifications is 3+ms of no bus activity/SOF packets) and then calls the USBCBSuspend()
// function.  You should modify these callback functions to take appropriate actions for each of these
// conditions.  For example, in the USBCBSuspend(), you may wish to add code that will decrease power
// consumption from Vbus to <2.5mA (such as by clock switching, turning off LEDs, putting the
// microcontroller to sleep, etc.).  Then, in the USBCBWakeFromSuspend() function, you may then wish to
// add code that undoes the power saving things done in the USBCBSuspend() function.

// The USBCBSendResume() function is special, in that the USB stack will not automatically call this
// function.  This function is meant to be called from the application firmware instead.  See the
// additional comments near the function.

/******************************************************************************
 * Function:        void USBCBSuspend(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        Call back that is invoked when a USB suspend is detected
 * Note:            None
 *****************************************************************************/
void USBCBSuspend(void)
{
	//Example power saving code.  Insert appropriate code here for the desired
	//application behavior.  If the microcontroller will be put to sleep, a
	//process similar to that shown below may be used:

	//ConfigureIOPinsForLowPower();
	//SaveStateOfAllInterruptEnableBits();
	//DisableAllInterruptEnableBits();
	//EnableOnlyTheInterruptsWhichWillBeUsedToWakeTheMicro();                   //Should enable at least USBActivityIF as a wake source
	//Sleep();
	//RestoreStateOfAllPreviouslySavedInterruptEnableBits();                    //preferably, this should be done in the USBCBWakeFromSuspend() function instead.
	//RestoreIOPinsToNormal();                                                  //preferably, this should be done in the USBCBWakeFromSuspend() function instead.

	//IMPORTANT NOTE: Do not clear the USBActivityIF (ACTVIF) bit here.  This bit is 
	//cleared inside the usb_device.c file.  Clearing USBActivityIF here will cause 
	//things to not work as intended.	
}

/******************************************************************************
 * Function:        void _USB1Interrupt(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        This function is called when the USB interrupt bit is set
 *                  In this example the interrupt is only used when the device
 *                  goes to sleep when it receives a USB suspend command
 * Note:            None
 *****************************************************************************/
#if 0
void __attribute__ ((interrupt)) _USB1Interrupt(void)
{
    #if !defined(self_powered)
        if(U1OTGIRbits.ACTVIF)
        {
            IEC5bits.USB1IE = 0;
            U1OTGIEbits.ACTVIE = 0;
            IFS5bits.USB1IF = 0;
        
            //USBClearInterruptFlag(USBActivityIFReg,USBActivityIFBitNum);
            USBClearInterruptFlag(USBIdleIFReg,USBIdleIFBitNum);
            //USBSuspendControl = 0;
        }
    #endif
}
#endif

/******************************************************************************
 * Function:        void USBCBWakeFromSuspend(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        The host may put USB peripheral devices in low power
 *                  suspend mode (by "sending" 3+ms of idle).  Once in suspend
 *                  mode, the host may wake the device back up by sending non-
 *                  idle state signalling.
 *                  This call back is invoked when a wakeup from USB suspend
 *                  is detected.
 * Note:            None
 *****************************************************************************/
void USBCBWakeFromSuspend(void)
{
	// If clock switching or other power savings measures were taken when
	// executing the USBCBSuspend() function, now would be a good time to
	// switch back to normal full power run mode conditions.  The host allows
	// a few milliseconds of wakeup time, after which the device must be 
	// fully back to normal, and capable of receiving and processing USB
	// packets.  In order to do this, the USB module must receive proper
	// clocking (IE: 48MHz clock must be available to SIE for full speed USB
	// operation).
}

/********************************************************************
 * Function:        void USBCB_SOF_Handler(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        The USB host sends out a SOF packet to full-speed
 *                  devices every 1 ms. This interrupt may be useful
 *                  for isochronous pipes. End designers should
 *                  implement callback routine as necessary.
 * Note:            None
 *******************************************************************/
void USBCB_SOF_Handler(void)
{
    // No need to clear UIRbits.SOFIF to 0 here.
    // Callback caller is already doing that.
}

/*******************************************************************
 * Function:        void USBCBErrorHandler(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        The purpose of this callback is mainly for
 *                  debugging during development. Check UEIR to see
 *                  which error causes the interrupt.
 * Note:            None
 *******************************************************************/
void USBCBErrorHandler(void)
{
    // No need to clear UEIR to 0 here.
    // Callback caller is already doing that.

	// Typically, user firmware does not need to do anything special
	// if a USB error occurs.  For example, if the host sends an OUT
	// packet to your device, but the packet gets corrupted (ex:
	// because of a bad connection, or the user unplugs the
	// USB cable during the transmission) this will typically set
	// one or more USB error interrupt flags.  Nothing specific
	// needs to be done however, since the SIE will automatically
	// send a "NAK" packet to the host.  In response to this, the
	// host will normally retry to send the packet again, and no
	// data loss occurs.  The system will typically recover
	// automatically, without the need for application firmware
	// intervention.
	
	// Nevertheless, this callback function is provided, such as
	// for debugging purposes.
}

/*******************************************************************
 * Function:        void USBCBCheckOtherReq(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        When SETUP packets arrive from the host, some
 * 					firmware must process the request and respond
 *					appropriately to fulfill the request.  Some of
 *					the SETUP packets will be for standard
 *					USB "chapter 9" (as in, fulfilling chapter 9 of
 *					the official USB specifications) requests, while
 *					others may be specific to the USB device class
 *					that is being implemented.  For example, a HID
 *					class device needs to be able to respond to
 *					"GET REPORT" type of requests.  This
 *					is not a standard USB chapter 9 request, and 
 *					therefore not handled by usb_device.c.  Instead
 *					this request should be handled by class specific 
 *					firmware, such as that contained in usb_function_hid.c.
 * Note:            None
 *******************************************************************/
void USBCBCheckOtherReq(void)
{
    USBCheckHIDRequest();
}//end

/*******************************************************************
 * Function:        void USBCBStdSetDscHandler(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        The USBCBStdSetDscHandler() callback function is
 *					called when a SETUP, bRequest: SET_DESCRIPTOR request
 *					arrives.  Typically SET_DESCRIPTOR requests are
 *					not used in most applications, and it is
 *					optional to support this type of request.
 * Note:            None
 *******************************************************************/
void USBCBStdSetDscHandler(void)
{
    // Must claim session ownership if supporting this request
}//end

/*******************************************************************
 * Function:        void USBCBInitEP(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        This function is called when the device becomes
 *                  initialized, which occurs after the host sends a
 * 					SET_CONFIGURATION (wValue not = 0) request.  This 
 *					callback function should initialize the endpoints 
 *					for the device's usage according to the current 
 *					configuration.
 * Note:            None
 *******************************************************************/
void USBCBInitEP(void)
{
    //enable the HID endpoint
    USBEnableEndpoint(HID_EP,USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
    //Re-arm the OUT endpoint for the next packet
    USBOutHandle = HIDRxPacket(HID_EP,(BYTE*)&ReceivedDataBuffer,64);
}

/********************************************************************
 * Function:        void USBCBSendResume(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        The USB specifications allow some types of USB
 * 					peripheral devices to wake up a host PC (such
 *					as if it is in a low power suspend to RAM state).
 *					This can be a very useful feature in some
 *					USB applications, such as an Infrared remote
 *					control	receiver.  If a user presses the "power"
 *					button on a remote control, it is nice that the
 *					IR receiver can detect this signalling, and then
 *					send a USB "command" to the PC to wake up.
 *					
 *					The USBCBSendResume() "callback" function is used
 *					to send this special USB signalling which wakes 
 *					up the PC.  This function may be called by
 *					application firmware to wake up the PC.  This
 *					function should only be called when:
 *					
 *					1.  The USB driver used on the host PC supports
 *						the remote wakeup capability.
 *					2.  The USB configuration descriptor indicates
 *						the device is remote wakeup capable in the
 *						bmAttributes field.
 *					3.  The USB host PC is currently sleeping,
 *						and has previously sent your device a SET 
 *						FEATURE setup packet which "armed" the
 *						remote wakeup capability.   
 *
 *					This callback should send a RESUME signal that
 *                  has the period of 1-15ms.
 *
 * Note:            Interrupt vs. Polling
 *                  -Primary clock
 *                  -Secondary clock ***** MAKE NOTES ABOUT THIS *******
 *                   > Can switch to primary first by calling USBCBWakeFromSuspend()
 
 *                  The modifiable section in this routine should be changed
 *                  to meet the application needs. Current implementation
 *                  temporary blocks other functions from executing for a
 *                  period of 1-13 ms depending on the core frequency.
 *
 *                  According to USB 2.0 specification section 7.1.7.7,
 *                  "The remote wakeup device must hold the resume signaling
 *                  for at lest 1 ms but for no more than 15 ms."
 *                  The idea here is to use a delay counter loop, using a
 *                  common value that would work over a wide range of core
 *                  frequencies.
 *                  That value selected is 1800. See table below:
 *                  ==========================================================
 *                  Core Freq(MHz)      MIP         RESUME Signal Period (ms)
 *                  ==========================================================
 *                      48              12          1.05
 *                       4              1           12.6
 *                  ==========================================================
 *                  * These timing could be incorrect when using code
 *                    optimization or extended instruction mode,
 *                    or when having other interrupts enabled.
 *                    Make sure to verify using the MPLAB SIM's Stopwatch
 *                    and verify the actual signal on an oscilloscope.
 *******************************************************************/
void USBCBSendResume(void)
{
    static WORD delay_count;
    
    USBResumeControl = 1;                                                       //Start RESUME signaling
    
    delay_count = 1800U;                                                        //Set RESUME line for 1-13ms
    do
    {
        delay_count--;
    }while(delay_count);
    USBResumeControl = 0;
}

#endif
/** EOF main.c *************************************************/