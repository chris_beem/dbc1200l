//*************************************************************************
//
//Description:	Source-file for PIC18F4550-firmware LoCon interface
//				Contains all LoCon-communication routines
//
//Filename	: LoCon_Interface_PIC18
//Version	: 0.1
//Date		: 27-09-2012
//Copyright	: Technetix B.V.
//Author	: ing. G. Bronkhorst
//
//
// 27-09-2012 Modification: First Definition list finished. All LoCon Interface functions are implemented. Ready for Beta Release.
//*************************************************************************
//#include "/USB/Include/Compiler.h"
//#include "Microprocessor_Selector.h"
//#include "Definitions.h"

#include "library.h"

/*
#include "/USB/Include/GenericTypeDefs.h"
//#include "/USB/Include/Compiler.h"

#include "Microprocessor_Selector.h"
#include "Definitions.h"
#include "main.h"
//#include "usb_config.h"
//#include "/USB/Include/Usb/usb_device.h"
//#include "/USB/Include/Usb/usb.h"
//#include "/USB/Include/Usb/usb_function_hid.h"
*/

#if defined(USE_LOCON_FUNCTIONS)
extern int LoConTransmitString[19];
extern int LoConReceiveString[19];
extern int LoConFraming;
extern int LoConAddress;
extern int LoConCommand;
extern int ReplyDestination;
#endif

#if defined(USE_LOCON_FUNCTIONS)
void LoConDelay(unsigned int j)                                                 //Function to implement a time delay to receive LoCon
{
  while(j>0)
  {
  	j--;
  }
} 
#endif

#if defined(USE_LOCON_FUNCTIONS)
//----------------------- This routine checks parity of array of bytes ------	
//---------------------------- Return values: 0=OK ; -1=NOT OK --------------
int CheckParity(int NrOfBytes, int *LoConByte)
{ 	
    int i, j, NrOfOnes, ParityBit, ParityError;
    ParityError=0;
    
    for(i=0; i<NrOfBytes; i++)                                                  //For each byte in array
    {
   	ParityBit=LoConByte[i] & 0x0001;                                            //LSB of byte is parity bit
   	NrOfOnes=0;    
   	for(j=1; j<9; j++)                                                          //9 bits per byte: 8data, 1 parity
        {
            if((LoConByte[i]>>j) & 0x0001)                                      //Check LSB (skip parity bit)
            {
                NrOfOnes++;                                                     //LSB=1
            }
        }
        if(NrOfOnes % 2)                                                        //If odd number of ones
        {
            if(ParityBit!=0)
            {
                ParityError=-1;                                                 //Parity error
            }
        }
        else                                                                    //If even number of ones
        {
            if(ParityBit!=1)
            {
                ParityError=-1;                                                 //Parity error
            }
        }
    }
    return(ParityError);
}        
#endif

#if defined(USE_LOCON_FUNCTIONS)
//---------------- //This routine removes the parity bits of an array of bytes ----
void RemoveParity(int NrOfBytes, int *LoConByte)
{	
    int i;
    
    for(i=0; i<NrOfBytes; i++)
    {
        LoConByte[i]>>=1;                                                       //Shift right one bit
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
int ValidLoConMasterFraming(int FramingCode)
{
    if((FramingCode & 0xFC) == 0xE0)
    {
    	return(1);                                                              //FramingCode OK
    }
    else
    {
    	return(0);                                                              //Invalid FramingCode
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
int ValidLoConSlaveFraming(int FramingCode)
{
    if((FramingCode & 0xFC) == 0xE4)
    {
    	return(1);                                                              //FramingCode OK
    }
    else
    {
    	return(0);                                                              //Invalid FramingCode
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS) || defined(USB_INTERNAL_FUNCTIONS)
int ValidLoConAddress(int OwnAddress, int Address)
{
    if((Address == OwnAddress) || (Address == 0xAF))
    {
    	return(1);                                                              //Address OK
    }
    else
    {
    	return(0);                                                              //Invalid address
    }
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
//LoCon routines for LoConI/O port:
int ReceiveLoConData(int *ReceiveLoConByte, int time_out)
{
    static int j,k,l,bits,bytes;
    static unsigned i=0;
    
    for(bytes=0;bytes<=19;bytes++)                                              //Maximum of 19 bytes per transaction
    {
        //mLED_1_On(); //This is the TX PIN
    	for(bits=0;bits<9;bits++)                                               //9bits per byte (8 + Parity)
     	{
            j=0;
            k=0;
            l=0;
            while (LoConRX == 0)                                                //While no data (I/O-pin is low)
            {
                l++;
            	if (l == 233333)                                                //Timeout Loop for waiting valid LoCondata is >1000ms
                {
                    return (-1);                                                //Return amount of bytes received
                }
                if(bytes!=0)                                                        //Inner Byte time for inner byte timeout after 5.47ms:
                {
                    i++;
                    if (i == 2800)                                              //Time-out counter = 750 -> Maximum time between LoCon bits is 20%  so timeout for 1 is 1.2ms and 0 is 0.6ms.
                    {
                        if(time_out==0)                                         //If max. number of timeouts reached this is 0
                        {
                            return(bytes);                                      //Return amount of bytes received
                        }
                        time_out--;                                             //Decrease max. number of timeouts
                        i=0;                                                    //Reset timeout counter
                    }
                }
            }
            while(1)                                                            //While always
            {
                j++;
                k=0;
                while ((LoConRX) != 0)                                              //While I/O-pin is high
                {
                    k++;
                    if (k == 250)                                               // This indicates it's about 0,44ms time delay before generate timeout on receive Disecq
                    {
                        return (-1);                                                        //Return error: bit too long
                    }
                } 
                k=0;
                while ((LoConRX == 0) && (k<21))                                    //While I/O-pin is low and counter<7          while ((LoConRX == 0) && (k<9))
                {
                    k++;
                }
                if (k == 21)
                {
                    break;                                                      //Break while-loop when I/O-pin low long enough
                }
            }
            ReceiveLoConByte[bytes] = (ReceiveLoConByte[bytes] << 1);           //Shift Receive byte left one bit
            if ((j == 10) || (j == 11) || (j == 12))                            //Number of bits for '1' is 11
            {
                //mLED_1_On(); // This is the TX PIN
                ReceiveLoConByte[bytes] = (ReceiveLoConByte[bytes] | 0x01);
                //mLED_1_Off(); // This is the TX PIN
            }
            if ((j == 21) || (j == 22) || (j == 23))                            //Number of bits for '0' is 22
            {
                //mLED_1_On(); //This is the TX PIN. ACK Sign on LED for receiving a correct 0 bit
                ReceiveLoConByte[bytes] = (ReceiveLoConByte[bytes] & 0xFFFE);
                //mLED_1_Off(); // This is the TX PIN
            }
            if ((j != 10) && (j != 11) && (j != 12) && (j != 21) && (j != 22) && (j != 23))
            {
                return(-1);                                                     //Return error: wrong number of bits
            }
            if (bits <8)                                                        //If not all 9 bits received
            {
                i=0;
                while ((LoConRX == 0) && (i<40000))                             //Inner bit time > 200us: Wait for I/O-pin to get high again We use 17ms : 2800/1.2ms * 40000 = 17.143ms
                {
                        i++;
                }
                if (i == 40000)
                {
                    return(-1) ;                                                //Return error: Incomplete byte
                }
            }
            //mLED_1_On();
     	}
    }
    return(-1);
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
int ReceiveLoCon(int *LoConString)                                              //Function to receive LoCon string
{
    int NrReceivedBytes;
    char ReturnValue;
    
    NrReceivedBytes = ReceiveLoConData(LoConString, 0);                         //Wait for data on LoCon-I/O
    if(NrReceivedBytes>=1)                                                      //If data received correct and at least one byte
    {
        if(CheckParity(NrReceivedBytes, LoConString)!=-1)
        {
            RemoveParity(NrReceivedBytes, LoConString);                         //Remove parity bits from data
            ReturnValue=NrReceivedBytes;
        }
        else
        {
            ReturnValue=-2;                                                     //Parity error
        }
    }
    else
    {
        ReturnValue=-1;                                                         //Nothing received
    }
    return(ReturnValue);
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
void SendLoConBit(int i)
{
	int j, dummy=0;
  	for (j=0;j<33;j++)
  	{
   		if(j<i)
   		{
	 		LoConTX_On();
			//mLED_1_On();
			for(dummy=0; dummy<12; dummy++);                                    //Send pulses in row with delay time as given
	 		//dummy++;
   		}
   		else
   		{
                        LoConTX_Off();
			//mLED_1_Off();
			for(dummy=0; dummy<10; dummy++);                                    //Send pulses in row with delay time as given
	 		dummy++;
   		}
        LoConTX_Off();
		//mLED_1_Off();
		for(dummy=0; dummy<10; dummy++);                                        //Send pulses in row with delay time as given
	}
}
#endif

#if defined(USE_LOCON_FUNCTIONS)
void SendLoCon(int NbBytes, int *SendLoConByte)
{
	int i,j,NbOnes;
    
	for(i=0;i<NbBytes;i++)
 	{ 
   		LoConDelay(620);                                                        //Time between LoCon Bytes is ~2ms
		NbOnes=0;         
		for(j=0;j<8;j++)
   		{
			if((SendLoConByte[i] >> (7-j)) % 2)
     		{
       			SendLoConBit(11);                                               //Send '1' if LSB=1
       			NbOnes = NbOnes + 1;                                            //Number of '1's transmitted+=1
     		}
     		else
     		{
       			SendLoConBit(22);                                               //Send '0' is LSB=0
     		}         
   		} 
   		if(NbOnes % 2)
   		{                     
       		SendLoConBit(22);                                                   //Parity=0 if odd number of 1's transmitted
   		}
   		else
   		{
       		SendLoConBit(11);                                                   //Parity=1 if even number of 1's transmitted
   		}                 
 	}                       
}                                               
#endif
